printf "************************************************ \n"
printf "************************************************ \n"
printf "**  CREANDO SEED DE TERMINALES PARTICULARES   ** \n"
printf "************************************************ \n"
printf "************************************************ \n"
def buscador_ciudades(like)
  a = ActiveRecord::Base.connection
  r = a.exec_query( sql_ciudades(like))
  if r.count != 0
    begin
      returner = r.first["id"].to_i
      return returner
    rescue ActiveRecord::RecordNotFound
      return nil
    rescue ActiveRecord::ActiveRecordError
      return nil
    rescue Exception
      #Only comes in here if nothing else catches the error
      return nil
    end
  end
end
def sql_ciudades(like)
  <<SQL
  SELECT
    *
  FROM
    public.ciudads
  WHERE
    ciudads.nombre LIKE '%#{like}%';
SQL
end

def buscador_parroquias(like)
  a = ActiveRecord::Base.connection
  r = a.exec_query( sql_parroquias(like))
  if r.count != 0
    begin
      returner = r.first["id"].to_i
      return returner
    rescue ActiveRecord::RecordNotFound
      return nil
    rescue ActiveRecord::ActiveRecordError
      return nil
    rescue Exception
      #Only comes in here if nothing else catches the error
      return nil
    end
  end
end
def sql_parroquias(like)
  <<SQL
  SELECT
    *
  FROM
    public.parroquia_vs
  WHERE
    parroquia_vs.nombre LIKE '%#{like}%';
SQL
end

def buscador_estados(like)
  a = ActiveRecord::Base.connection
  r = a.exec_query( sql_estados(like))
  if r.count != 0
    begin
      returner = r.first["id"].to_i
      return returner
    rescue ActiveRecord::RecordNotFound
      return nil
    rescue ActiveRecord::ActiveRecordError
      return nil
    rescue Exception
      #Only comes in here if nothing else catches the error
      return nil
    end
  end
end
def sql_estados(like)
  <<SQL
  SELECT
    *
  FROM
    public.estados
  WHERE
    estados.nombre LIKE '%#{like}%';
SQL
end


paradas_array = ([{nombre: "Terminal de Pasajeros Melicio Pérez", coordenada_utm: "POINT(-71.27230748474100608 8.96901049815710039)", avenida: "Perimetral", calle: "", sector: "Urb. Los Lirios", tipo_parada: "Terminal Publico", parroquia_v_id: "PLATANILLAL", ciudad_id: "PUERTO AYACUCHO", estado_id: "AMAZONAS"},
    {nombre: "Terminal Privado Expresos la Prosperidad (Inscata)", coordenada_utm: "", avenida: "Principal", calle: "Sucre N° 5", sector: "Urb. Alto Parima", tipo_parada: "Terminal Privado", parroquia_v_id: "PLATANILLAL", ciudad_id: "PUERTO AYACUCHO", estado_id: "AMAZONAS"},
    {nombre: "Terminal Expresos Occidente", coordenada_utm: "POINT(-71.11995995001799997 9.95590138353539977)", avenida: "", calle: "", sector: "Entrada Terminal Expresos Occidente", tipo_parada: "Terminal Privado", parroquia_v_id: "POZUELOS", ciudad_id: "POZUELOS", estado_id: "ANZOATEGUI"},
    {nombre: "Terminal de Autobuses Peliexpress", coordenada_utm: "POINT(-72.54354095288799442 10.06042049033399977)", avenida: "Intercomunal Jorge Rodriguez", calle: "", sector: "Las garzas, Lecheria", tipo_parada: "Terminal Privado", parroquia_v_id: "EL MORRO", ciudad_id: "LECHERIAS", estado_id: "ANZOATEGUI"},
    {nombre: "Terminal de Pasajeros Cleto Quijada", coordenada_utm: "POINT(-71.47696030881900242 10.39345401617899967)", avenida: "", calle: "", sector: "Barrio Meneven", tipo_parada: "Terminal Publico", parroquia_v_id: "MIGUEL OTERO SILVA", ciudad_id: "EL TIGRE", estado_id: "ANZOATEGUI"},
    {nombre: "Terminal de Pasajeros Puerto La Cruz", coordenada_utm: "POINT(-67.4503422314059975 10.17844858426200005)", avenida: "", calle: "Juncal", sector: "Barrio El pensil", tipo_parada: "Terminal Publico", parroquia_v_id: "CAPITAL PUERTO LA CRUZ", ciudad_id: "PUERTO LA CRUZ", estado_id: "ANZOATEGUI"},
    {nombre: "Terminal de Pasajeros de Anaco", coordenada_utm: "POINT(-66.87731002251899781 10.13701625468399925)", avenida: "", calle: "Calle Marti", sector: "Las parcelas", tipo_parada: "Terminal Publico", parroquia_v_id: "CAPITAL ANACO", ciudad_id: "ANACO", estado_id: "ANZOATEGUI"},
    {nombre: "Terminal de Pasajeros La Aduana Barcelona", coordenada_utm: "POINT(-62.04875366105999746 9.05584747305199933)", avenida: "", calle: "San Carlos", sector: "Barrio Aduana", tipo_parada: "Terminal Publico", parroquia_v_id: "EL CARMEN", ciudad_id: "BARCELONA", estado_id: "ANZOATEGUI"},
    {nombre: "Terminal de Pasajeros Metropolitano de Piritu", coordenada_utm: "POINT(-66.71398680697200234 10.26904323780600059)", avenida: "El tejar", calle: "", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "CAPITAL PIRITU", ciudad_id: "PIRITU", estado_id: "ANZOATEGUI"},
    {nombre: "Terminal de Pasajeros Bertho Ovidio Flores", coordenada_utm: "", avenida: "", calle: "", sector: "Colombia Pariaguan", tipo_parada: "Terminal Publico", parroquia_v_id: "CAPITAL FRANCISCO DE MIRANDA", ciudad_id: "PARIAGUAN", estado_id: "ANZOATEGUI"},
    {nombre: "Terminal de Pasajeros Pedro María Freites", coordenada_utm: "POINT(-66.96864732804399978 10.47754881821400019)", avenida: "Bolivar", calle: "", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "CAPITAL PEDRO MARIA FREITES", ciudad_id: "CANTAURA", estado_id: "ANZOATEGUI"},
    {nombre: "Terminal de Pasajeros de Humberto Hernandez", coordenada_utm: "POINT(-66.89057672571800595 10.50037987667900019)", avenida: "España ", calle: "Piar", sector: "Barrio La Playa", tipo_parada: "Terminal Publico", parroquia_v_id: "SAN FERNANDO", ciudad_id: "SAN FERNANDO", estado_id: "APURE"},
    {nombre: "Expresos Los Llanos San Fernando", coordenada_utm: "", avenida: "", calle: "España", sector: "", tipo_parada: "Terminal Privado", parroquia_v_id: "SAN FERNANDO", ciudad_id: "SAN FERNANDO", estado_id: "APURE"},
    {nombre: "Segunto Duarte", coordenada_utm: "", avenida: "", calle: "", sector: "Entrada de Achaguas", tipo_parada: "Terminal Publico", parroquia_v_id: "ACHAGUAS", ciudad_id: "ACHAGUAS", estado_id: "APURE"},
    {nombre: "Expresos Los Llanos Guasdualito", coordenada_utm: "", avenida: "", calle: "Cedeño", sector: "", tipo_parada: "Terminal Privado", parroquia_v_id: "GUASDUALITO", ciudad_id: "GUASDUALITO", estado_id: "APURE"},
    {nombre: "A.C Romulo Gallegos", coordenada_utm: "", avenida: "Francisco Montoya Elorza", calle: "", sector: "", tipo_parada: "Terminal Privado", parroquia_v_id: "ELORZA", ciudad_id: "ELORZA", estado_id: "APURE"},
    {nombre: "Teminal Privado Unión de Conductores San Antonio", coordenada_utm: "POINT(-64.63869116998399988 10.19366751360199963)", avenida: "Carretera nacional cagua", calle: "", sector: "", tipo_parada: "Terminal Privado", parroquia_v_id: "CAPITAL SUCRE", ciudad_id: "CAGUA", estado_id: "ARAGUA"},
    {nombre: "Terminal de Maracay", coordenada_utm: "POINT(-69.73633516176599301 9.02628636154219954)", avenida: "", calle: "", sector: "Urb. Santa Ana", tipo_parada: "Terminal Publico", parroquia_v_id: "JOSE CASANOVA GODOY", ciudad_id: "MARACAY", estado_id: "ARAGUA"},
    {nombre: "Terminal de Pasajeros José Félix Ribas", coordenada_utm: "POINT(-66.60243813745499608 10.47134386900600056)", avenida: "Intercomunal La Mora", calle: "", sector: "Hacienda La Mora", tipo_parada: "Terminal Publico", parroquia_v_id: "CASTOR NIEVES RIOS", ciudad_id: "LA VICTORIA", estado_id: "ARAGUA"},
    {nombre: "Terminal de Pasajeros Ocumare de La Costa", coordenada_utm: "POINT(-67.86273890133399789 10.21618444130199954)", avenida: "", calle: "Pumita", sector: "Parcelamiento las Animas", tipo_parada: "", parroquia_v_id: "OCUMARE DE LA COSTA DE ORO", ciudad_id: "OCUMARE DE LA COSTA", estado_id: "ARAGUA"},
    {nombre: "Terminal de Pasajeros de Choroní", coordenada_utm: "", avenida: "", calle: "", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "CHORONI", ciudad_id: "CHORONI", estado_id: "ARAGUA"},
    {nombre: "Aeroexpresos Ejecutivos Maracay", coordenada_utm: "POINT(-71.6198202710170051 10.63540845777800037)", avenida: "99", calle: "13", sector: "Zona Militar Instituto Prevencion Social de las fuerza armadas", tipo_parada: "Terminal Privado", parroquia_v_id: "MADRE MARIA DE SAN JOSE", ciudad_id: "MARACAY", estado_id: "ARAGUA"},
    {nombre: "Terminal de Pasajeros Virgen Nuestra Señora del Pilar", coordenada_utm: "POINT(-66.90980564863799884 10.48401282295700021)", avenida: "Cuatricentenaria", calle: "", sector: "Barrio El Cambio", tipo_parada: "Terminal Publico", parroquia_v_id: "EL CARMEN", ciudad_id: "BARINAS", estado_id: "BARINAS"},
    {nombre: "Terminal de Pasajeros Socopo", coordenada_utm: "", avenida: "", calle: "3", sector: "Barrio Libertador", tipo_parada: "Terminal Publico", parroquia_v_id: "TICOPORO", ciudad_id: "SOCOPO", estado_id: "BARINAS"},
    {nombre: "Terminal de Pasajeros Santa Barbara", coordenada_utm: "", avenida: "", calle: "", sector: "Pedraza paseo cuatricentario", tipo_parada: "Terminal Publico", parroquia_v_id: "SANTA ROSA", ciudad_id: "CIUDAD BOLIVAR", estado_id: "BARINAS"},
    {nombre: "Terminal de Pasajeros José de la Cruz Paredes", coordenada_utm: "", avenida: "", calle: "La Paz", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "BARRANCAS", ciudad_id: "BARRANCAS", estado_id: "BARINAS"},
    {nombre: "Terminal de Pasajeros Antonio Camacho", coordenada_utm: "", avenida: "", calle: "", sector: "Santa Clara", tipo_parada: "Terminal Publico", parroquia_v_id: "BARINITAS", ciudad_id: "BARINITAS", estado_id: "BARINAS"},
    {nombre: "Terminal de Pasajeros Peliexpress", coordenada_utm: "", avenida: "Cuatricentenaria", calle: "", sector: "", tipo_parada: "Terminal Privado", parroquia_v_id: "BARINAS", ciudad_id: "BARINAS", estado_id: "BARINAS"},
    {nombre: "Terminal de Pasajeros Santa Barbara", coordenada_utm: "", avenida: "Quinta Avenida", calle: "", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "SANTA BARBARA", ciudad_id: "SANTA BARBARA", estado_id: "BARINAS"},
    {nombre: "Terminal de Pasajeros de Upata", coordenada_utm: "POINT(-71.72781378223299953 10.96142405839299983)", avenida: "", calle: "", sector: "SANTA BARBARA", tipo_parada: "Terminal Publico", parroquia_v_id: "CAPITAL PIAR", ciudad_id: "UPATA", estado_id: "BOLIVAR"},
    {nombre: "Terminal de Pasajeros Terpricanoni (Rodovías de Venezuela)", coordenada_utm: "POINT(-72.23819273105499406 7.74574603761819969)", avenida: "", calle: "Principal", sector: "Urb. Campo C", tipo_parada: "Terminal Privado", parroquia_v_id: "UNIVERSIDAD", ciudad_id: "PUERTO ORDAZ", estado_id: "BOLIVAR"},
    {nombre: "Terminar de Pasajeros Manuel Piar", coordenada_utm: "POINT(-64.64758181272600268 10.2143810939650006)", avenida: "", calle: "Principal", sector: "Urb. Campo C", tipo_parada: "Terminal Publico", parroquia_v_id: "UNIVERSIDAD", ciudad_id: "PUERTO ORDAZ", estado_id: "BOLIVAR"},
    {nombre: "Terminar de Pasajeros Monseñor Francisco Javier Zabaleta", coordenada_utm: "POINT(-64.66571388757799355 10.18114000386399987)", avenida: "Jose Gumilla", calle: "", sector: "Barrio 5 de Julio", tipo_parada: "Terminal Publico", parroquia_v_id: " CARONI", ciudad_id: "SAN FELIX", estado_id: "BOLIVAR"},
    {nombre: "Terminal de Pasajeros Ciudad Bolívar", coordenada_utm: "POINT(-71.64723797827200258 10.96104271550500009)", avenida: "República", calle: "", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "VISTA HERMOSA", ciudad_id: "CIUDADA BOLIVAR", estado_id: "BOLIVAR"},
    {nombre: "Terminal de Pasajeros El Callao", coordenada_utm: "", avenida: "", calle: "", sector: "Salida del Callao", tipo_parada: "Terminal Publico", parroquia_v_id: "CAPITAL CALLAO", ciudad_id: "CALLAO", estado_id: "BOLIVAR"},
    {nombre: "Terminal de Pasajeros de Guasipati", coordenada_utm: "", avenida: "Orinoco", calle: "", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "CAPITAL ROSCIO", ciudad_id: "GUASIPATI", estado_id: "BOLIVAR"},
    {nombre: "Terminal de Pasajeros Arcadio Castro", coordenada_utm: "", avenida: "Matadero", calle: "", sector: "Urb. Rómulo Gallegos", tipo_parada: "Terminal Publico", parroquia_v_id: "CAPITAL CEDEÑO", ciudad_id: "CAICARA DEL ORINOCO", estado_id: "BOLIVAR"},
    {nombre: "Terminal de Pasajeros Ciudad Piar", coordenada_utm: "", avenida: "Martín Travieso", calle: "", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "CAPITAL PIAR", ciudad_id: "CIUDAD PIAR", estado_id: "BOLIVAR"},
    {nombre: "Terminal de Pasajeros Turistico La Gran Sabana", coordenada_utm: "", avenida: "Troncal Nro 10", calle: "", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "CAPITAL GRAN SABANA", ciudad_id: "SANTA ELENA DE GUAIREN", estado_id: "BOLIVAR"},
    {nombre: "Terminal de Pasajero de San Diego", coordenada_utm: "POINT(-71.16877863127399451 8.58594214128040001)", avenida: "", calle: "Tejeria", sector: "Centro de San diego", tipo_parada: "Terminal Publico", parroquia_v_id: "SAN DIEGO", ciudad_id: "SAN DIEGO", estado_id: "CARABOBO"},
    {nombre: "Terminal de Pasajeros Experos los Llanos", coordenada_utm: "", avenida: "Boulevar Norte", calle: "Norte syr", sector: "", tipo_parada: "Terminal Privado", parroquia_v_id: "SAN DIEGO", ciudad_id: "SAN DIEGO", estado_id: "CARABOBO"},
    {nombre: "Rodovías de Venezuela Valencia", coordenada_utm: "POINT(-67.96386389533100214 10.19246715690399974)", avenida: "68", calle: "101", sector: "", tipo_parada: "Terminal Privado", parroquia_v_id: "SAN DIEGO", ciudad_id: "SAN DIEGO", estado_id: "CARABOBO"},
    {nombre: "Terminal de Pasajeros Big Low Center", coordenada_utm: "POINT(-66.94246050962800609 10.49379955614899984)", avenida: "71", calle: "", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "SAN DIEGO", ciudad_id: "SAN DIEGO", estado_id: "CARABOBO"},
    {nombre: "Terminal de Pasajeros Juan Jose Mora", coordenada_utm: "POINT(-62.66025411148699931 8.37472136620280061)", avenida: "", calle: "", sector: "Urb Palma Sola", tipo_parada: "Terminal Publico", parroquia_v_id: "MORON", ciudad_id: "MORON", estado_id: "CARABOBO"},
    {nombre: "Terminal de Pasajeros Juan José Flores", coordenada_utm: "POINT(-69.33502341952099357 10.06907384853600007)", avenida: "", calle: "La Noria", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "FRATERNIDADN", ciudad_id: "PUERTO CABELLO", estado_id: "CARABOBO"},
    {nombre: "Terminal de Pasajeros de San Carlos", coordenada_utm: "POINT(-72.43623854361000269 7.82606097587049998)", avenida: "", calle: "", sector: "Barrio Campo de Aviación", tipo_parada: "Terminal Publico", parroquia_v_id: "GENERAL EN JEFE JOSE LAURENCIO SILVA", ciudad_id: "SAN CARLOS", estado_id: "COJEDES"},
    {nombre: "Terminal de Pasajeros de Tucupita", coordenada_utm: "POINT(-66.66079893104800647 10.24474394568500024)", avenida: "Guasina", calle: "", sector: "Urb. Delfin Mendoza", tipo_parada: "Terminal Publico", parroquia_v_id: "MACARAO", ciudad_id: "TUCUPITA", estado_id: "DELTA AMACURO"},
    {nombre: "Terminal de Pasajeros Terprivenca (Rodovias de Venezuela)", coordenada_utm: "POINT(-64.63474089149200097 10.21740242420800016)", avenida: "", calle: "Amador Bendayán", sector: "", tipo_parada: "Terminal Privado", parroquia_v_id: "EL RECREO", ciudad_id: "CARACAS", estado_id: "DISTRITO CAPITAL"},
    {nombre: "Terminal de Pasajeros Expresos Occidente ", coordenada_utm: "POINT(-66.66033820008499333 10.24425262995099928)", avenida: "Louis Braile", calle: "", sector: "El cementerio", tipo_parada: "Terminal Privado", parroquia_v_id: "SANTA ROSALIA", ciudad_id: "CARACAS", estado_id: "DISTRITO CAPITAL"},
    {nombre: "Terminal de Pasajeros Ing. José Gonzalez Lander", coordenada_utm: "POINT(-64.48841264296500242 9.43248813957760035)", avenida: "", calle: "", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "EL VALLE", ciudad_id: "CARACAS", estado_id: "DISTRITO CAPITAL"},
    {nombre: "Terminal de Pasajeros Nuevo Circo", coordenada_utm: "POINT(-67.96269740562199502 10.19149119333500053)", avenida: "", calle: "", sector: "Nuevo circo", tipo_parada: "Terminal Publico", parroquia_v_id: "SANTA ROSALIA", ciudad_id: "CARACAS", estado_id: "DISTRITO CAPITAL"},
    {nombre: "Terminal  de Pasajeros Crucero de Oriente Sur", coordenada_utm: "POINT(-70.78060222819600256 9.43882938174330022)", avenida: "", calle: "", sector: "", tipo_parada: "Terminal Privado", parroquia_v_id: "EL PARAISO", ciudad_id: "CARACAS", estado_id: "DISTRITO CAPITAL"},
    {nombre: "Terminal de Pasajeros Expresos Camargui", coordenada_utm: "POINT(-63.60425673136499825 9.68715906421189921)", avenida: "", calle: "Sucre", sector: "", tipo_parada: "Terminal Privado", parroquia_v_id: "EL PARAISO", ciudad_id: "CARACAS", estado_id: "DISTRITO CAPITAL"},
    {nombre: "Terminal de Pasajeros Expresos Merida", coordenada_utm: "POINT(-70.92798066546100699 9.81479966881729915)", avenida: "El cortijo", calle: "", sector: "", tipo_parada: "Terminal Privado", parroquia_v_id: "SAN PEDRO", ciudad_id: "CARACAS", estado_id: "DISTRITO CAPITAL"},
    {nombre: "Terminal de Pasajeros Rutas de America", coordenada_utm: "", avenida: "Fuerzas Armandas", calle: "", sector: "", tipo_parada: "Terminal Privado", parroquia_v_id: "SANTA ROSALIA", ciudad_id: "CARACAS", estado_id: "DISTRITO CAPITAL"},
    {nombre: "Terminal de Pasajeros En Construcción", coordenada_utm: "POINT(-64.19122013503900348 10.46965206385699965)", avenida: "", calle: "Larrazábal", sector: "", tipo_parada: "Zona o Parada Terminal", parroquia_v_id: "MACARAO", ciudad_id: "CARACAS", estado_id: "DISTRITO CAPITAL"},
    {nombre: "Terminal de Pasajeros Puerto Cumarebo", coordenada_utm: "POINT(-62.38862789548900167 8.00924905930389919)", avenida: "", calle: "Vargas", sector: "Bella Vista", tipo_parada: "Terminal Publico", parroquia_v_id: "PUERTO CUMAREBO", ciudad_id: "PUERTO CUMAREBO", estado_id: "FALCON"},
    {nombre: "Terminal de Pasajeros Pueblo Nuevo", coordenada_utm: "POINT(-67.77130863447999332 10.46734824612200043)", avenida: "", calle: "", sector: "Buena Vista", tipo_parada: "Terminal Publico", parroquia_v_id: "PUEBLO NUEVO", ciudad_id: "PUEBLO NUEVO", estado_id: "FALCON"},
    {nombre: "Terminal de Pasajeros Churuguara", coordenada_utm: "POINT(-68.69306774335299792 10.37362312921199958)", avenida: "", calle: "", sector: "Urb. Santa Lucia", tipo_parada: "Terminal Publico", parroquia_v_id: "CHURUGUARA", ciudad_id: "CHURUGUARA", estado_id: "FALCON"},
    {nombre: "Terminal de Pasajeros Pariente Marín", coordenada_utm: "", avenida: "", calle: "Wilfredo Rojas", sector: "Nueva aurora norte", tipo_parada: "Terminal Publico", parroquia_v_id: "CAPITAL DABAJURO", ciudad_id: "DABAJURO", estado_id: "FALCON"},
    {nombre: "Terminal de Pasajeros Ali Primera", coordenada_utm: "", avenida: "Intercomunal Alí primera", calle: "", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "CARIRUBANA", ciudad_id: "PUNTO FIJO", estado_id: "FALCON"},
    {nombre: "Terminal de Pasajeros de Tucacas", coordenada_utm: "", avenida: "", calle: "", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "TUCACAS", ciudad_id: "TUCACAS", estado_id: "FALCON"},
    {nombre: "Terminal de Pasajero Polica Salas", coordenada_utm: "POINT(-67.57526319734300557 10.24491656638699943)", avenida: "", calle: "Mapararí", sector: "Barrio Bobare", tipo_parada: "Terminal Publico", parroquia_v_id: "SAN GABRIEL", ciudad_id: "CORO", estado_id: "FALCON"},
    {nombre: "Terminal de Pasajeros de Zaraza", coordenada_utm: "POINT(-66.85799374578600407 10.23673674238600029)", avenida: "", calle: "Bolivar", sector: "Barrio La Romana", tipo_parada: "Terminal Publico", parroquia_v_id: "CAPITAL ZARARA", ciudad_id: "ZARAZA", estado_id: "GUARICO"},
    {nombre: "Terminal de Pasajeros Ángel Custodio Loyola", coordenada_utm: "POINT(-71.60991423469400274 10.60466459762599989)", avenida: "Carretera nacional Via Carzola", calle: "", sector: "Barrio Vicario", tipo_parada: "Terminal Publico", parroquia_v_id: "CAPITAL CALABOZO", ciudad_id: "CALABOZO", estado_id: "GUARICO"},
    {nombre: "Terminal de Pasajeros Juan German Roscio", coordenada_utm: "POINT(-68.73730596589899733 10.33345841805600074)", avenida: "Villa de Cura", calle: "", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "CAPITAL SAN JUAN DE LOS MORROS", ciudad_id: "SAN JUAN DE LOS MORROS", estado_id: "GUARICO"},
    {nombre: "Terminal de Pasajeros Juan Arroyo", coordenada_utm: "POINT(-72.21848559448500282 7.76724483984139979)", avenida: "", calle: "El Roble", sector: "Urb. Los Maestros", tipo_parada: "Terminal Publico", parroquia_v_id: "CAPITAL VALLE DE LA PASCUA", ciudad_id: "VALLE DE LA PASCUA", estado_id: "GUARICO"},
    {nombre: "Terminal de Pasajeros Altagracia de Orituco", coordenada_utm: "", avenida: "", calle: "", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "CAPITAL ALTA GRACIA DE ORITUCO", ciudad_id: "ALTA GRACIA DE ORITUCO", estado_id: "GUARICO"},
    {nombre: "Terminal de Pasajeros Juan Welman", coordenada_utm: "", avenida: "", calle: "", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "CA`PITAL CAMAGUAN`", ciudad_id: "CAMAGUAN", estado_id: "GUARICO"},
    {nombre: "Terminal de Pasajeros de Barquisimeto", coordenada_utm: "POINT(-67.96691179846399677 10.19261127802499978)", avenida: "", calle: "", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "CONCEPCION", ciudad_id: "BARQUISIMETO", estado_id: "LARA"},
    {nombre: "Terminal de  Pasaqjeros de Carora", coordenada_utm: "", avenida: "Francisco de Miranda", calle: "", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "TORRES", ciudad_id: "CARORA", estado_id: "LARA"},
    {nombre: "Terminal de Pasajeros Aeroexpresos Ejecutivo", coordenada_utm: "", avenida: "Hernán Garmendia", calle: "", sector: "", tipo_parada: "Terminal Privado", parroquia_v_id: "CATEDRAL", ciudad_id: "BARQUISIMETO", estado_id: "LARA"},
    {nombre: "Terminal de Pasajeros José Antonio Paredes", coordenada_utm: "POINT(-72.23627693472700173 7.74901762664990024)", avenida: "Las americas", calle: "", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "MARIANO PICON SALAS", ciudad_id: "MERIDA", estado_id: "MERIDA"},
    {nombre: "Nuevo Terminal de Mérida", coordenada_utm: "POINT(-66.91096678448600699 10.50048200492799921)", avenida: "Centenario", calle: "", sector: "", tipo_parada: "", parroquia_v_id: "MATRIZ", ciudad_id: "EJIDO", estado_id: "MERIDA"},
    {nombre: "Terminal de Pasajeros Expresos Mérida", coordenada_utm: "POINT(-64.20351849084299545 10.55449541088600007)", avenida: "15", calle: "", sector: "Barrio La Esperanza", tipo_parada: "Terminal Privado", parroquia_v_id: "PRESIDENTE PAEZ", ciudad_id: "EL VIGIA", estado_id: "MERIDA"},
    {nombre: "Terminal de Pasajeros El Vigía", coordenada_utm: "POINT(-71.2455827383450071 10.12551276804600064)", avenida: "Carretera Panamericana", calle: "", sector: "Los Corrales", tipo_parada: "Terminal Publico", parroquia_v_id: "PRESIDENTE ROMULO GALLEGOS", ciudad_id: "EL VIGIA", estado_id: "MERIDA"},
    {nombre: "Terminal de Pasajeros de Zea", coordenada_utm: "", avenida: "Perimetral", calle: "", sector: "Mocoties", tipo_parada: "Terminal Publico", parroquia_v_id: "EL LLANO", ciudad_id: "ZEA", estado_id: "MERIDA"},
    {nombre: "Terminal de Pasajeros Maria Vivas García", coordenada_utm: "", avenida: "Cristobal Mendoza", calle: "", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "TOVAR", ciudad_id: "TOVAR", estado_id: "MERIDA"},
    {nombre: "Terminal de Pasajeros Antonio José de Sucre", coordenada_utm: "", avenida: "", calle: "Aguas de Urao", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "SAN JUAN", ciudad_id: "LAGUNILLAS", estado_id: "MERIDA"},
    {nombre: "Terminal de Pasajeros de Guarenas", coordenada_utm: "POINT(-69.19836159547899967 9.57478114199069985)", avenida: "Intercomunal Guarenas", calle: "", sector: "Urb. Trapichito", tipo_parada: "Terminal Publico", parroquia_v_id: "GUARENAS", ciudad_id: "GUARENAS", estado_id: "MIRANDA"},
    {nombre: "Terminal de Pasajeros Santa Teresa de Tuy", coordenada_utm: "POINT(-71.34121671323499925 10.19827185107000034)", avenida: "", calle: "Principal", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "SANTA TERESA DEL TUY", ciudad_id: "SANTA TERESA DEL TUY", estado_id: "MIRANDA"},
    {nombre: "Terminal Antonio José de Sucre", coordenada_utm: "POINT(-66.8354579420939956 10.49085595183800024)", avenida: "Carretera Nacional", calle: "", sector: "Barrio Las Margaritas", tipo_parada: "Terminal Publico", parroquia_v_id: "GUATIRE", ciudad_id: "GUATIRE", estado_id: "MIRANDA"},
    {nombre: "Terminal de Pasajero Los Lagos", coordenada_utm: "POINT(-69.92388909017999765 11.94432689628300004)", avenida: "", calle: "", sector: "Barrio Camatagua", tipo_parada: "Terminal Publico", parroquia_v_id: "LOS TEQUES", ciudad_id: "LOS TEQUES", estado_id: "MIRANDA"},
    {nombre: "Terminal de Pasajeros de Charallaves", coordenada_utm: "POINT(-70.2265070561559952 8.63054327369670027)", avenida: "Cristobal Rojas", calle: "6", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "CHARALLAVE", ciudad_id: "CHARALLAVE", estado_id: "MIRANDA"},
    {nombre: "Terminal de Pasajeros Aeroexpresos Ejecutivos ", coordenada_utm: "POINT(-66.15043177616300341 10.40039543185100079)", avenida: "Ávila", calle: "", sector: "Bello Campo", tipo_parada: "Terminal Privado", parroquia_v_id: "CHACAO", ciudad_id: "CARACAS", estado_id: "MIRANDA"},
    {nombre: "Terminal de Pasajeros Peliexpress ", coordenada_utm: "POINT(-68.5841090500669992 9.65290872107019915)", avenida: "", calle: "", sector: "", tipo_parada: "Terminal Privado", parroquia_v_id: "PETARE", ciudad_id: "GUARENAS", estado_id: "MIRANDA"},
    {nombre: "Terminal Tacarigua Mamporal", coordenada_utm: "POINT(-62.74548953281900054 8.28828459980199916)", avenida: "", calle: "El calvario", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "TACARIGUA", ciudad_id: "TACARIGUA DE MAPORAL", estado_id: "MIRANDA"},
    {nombre: "Terminal de Pasajeros San José de Barlovento", coordenada_utm: "", avenida: "", calle: "Flor de Mayo", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "SAN JOSE DE BARLOVENTO", ciudad_id: "SAN JOSE DE BARLOVENTO", estado_id: "MIRANDA"},
    {nombre: "Terminal de Pasajeros Ocumare del Tuy", coordenada_utm: "", avenida: "Tomas Lander", calle: "", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "OCUMARE DEL TUY", ciudad_id: "OCUMARE DEL TUY", estado_id: "MIRANDA"},
    {nombre: "Terminal de La Urbina", coordenada_utm: "POINT(-71.62030190453700129 10.63575407324900013)", avenida: "Principal de La Urbina", calle: "", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "PETARE", ciudad_id: "CARACAS", estado_id: "MIRANDA"},
    {nombre: "Terminal de Pasajeros de Cúa", coordenada_utm: "POINT(-70.20752099472500163 11.69478654269999929)", avenida: "Carretera Nacional Cua", calle: "", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "CUA", ciudad_id: "CUA", estado_id: "MIRANDA"},
    {nombre: "Terminal de Pasajeros Antonio José de Sucre", coordenada_utm: "POINT(-69.67258472083200616 11.40338161208100054)", avenida: "", calle: "", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "PETARE", ciudad_id: "CARACAS", estado_id: "MIRANDA"},
    {nombre: "Terminal de Pasajeros de Caucagua", coordenada_utm: "POINT(-64.25913875451099955 10.57308332917200033)", avenida: "", calle: "Arevalo Gonzalez", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "CAUCAGUA", ciudad_id: "GUARENAS", estado_id: "MIRANDA"},
    {nombre: "Terminal de Pasajeros de Punta de Mata", coordenada_utm: "POINT(-67.47566918557299687 7.89644313914729956)", avenida: "", calle: "", sector: "Sector Punta de Mata", tipo_parada: "Terminal Publico", parroquia_v_id: "CAPITAL EZEQUIEL ZAMORA", ciudad_id: "PUNTA DE MATA", estado_id: "MONAGAS"},
    {nombre: "Terminal de Pasajeros Aeroexpresos Ejecutivos ", coordenada_utm: "POINT(-62.04263907796899957 9.05178418258319972)", avenida: "Libertador", calle: "", sector: "Urb. Alto Los Godos", tipo_parada: "Terminal Privado", parroquia_v_id: "CAPITAL MATURIN", ciudad_id: "MATURIN", estado_id: "MONAGAS"},
    {nombre: "Terminal de Pasajeros Interurbano de Maturín", coordenada_utm: "POINT(-68.89583504264099645 10.16963008174199956)", avenida: "Libertador", calle: "", sector: "Urb. Los Morichales", tipo_parada: "Terminal Publico", parroquia_v_id: "CAPITAL MATURIN", ciudad_id: "MATURIN", estado_id: "MONAGAS"},
    {nombre: "Terminal de Pasajeros de Temblador", coordenada_utm: "", avenida: "Principal de Temblador", calle: "", sector: "Sector Las Brisas 2", tipo_parada: "Terminal Publico", parroquia_v_id: "CAPITAL CEDEÑO", ciudad_id: "TEMBLADOR", estado_id: "MONAGAS"},
    {nombre: "Terminal de Pasajeros Caicara de Maturin", coordenada_utm: "", avenida: "", calle: "Bermudez", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "CAPITAL CEDEÑO", ciudad_id: "CAICARA DE MATURIN", estado_id: "MONAGAS"},
    {nombre: "Terminal de Pasajeros de Juan Griego", coordenada_utm: "POINT(-71.65287851930999352 8.61547934536999982)", avenida: "", calle: "Azahares", sector: "Juan Griego ", tipo_parada: "Terminal Publico", parroquia_v_id: "CAPITAL MARCANO", ciudad_id: "JUAN GRIEGO", estado_id: "NUEVA ESPARTA"},
    {nombre: "Terminal de Pasajeros de Porlamar", coordenada_utm: "", avenida: "Terranova", calle: "26 de Marzo", sector: "Cruz Grande", tipo_parada: "Terminal Publico", parroquia_v_id: "JUAN GRIEGO", ciudad_id: "CAPITAL GARCIA", estado_id: "NUEVA ESPARTA"},
    {nombre: "Terminal de Pasajeros Expresos Ayacucho", coordenada_utm: "", avenida: "", calle: "Libertad", sector: "", tipo_parada: "Terminal Privado", parroquia_v_id: "JUAN GRIEGO", ciudad_id: "CAPITAL MARCANO", estado_id: "NUEVA ESPARTA"},
    {nombre: "Terminal de Pasajeros Union de Conductores Margarita", coordenada_utm: "", avenida: "Terranova", calle: "26 de Marzo", sector: "Cruz Grande", tipo_parada: "Terminal Privado", parroquia_v_id: "CAPITAL GARCIA", ciudad_id: "PORLAMAR", estado_id: "NUEVA ESPARTA"},
    {nombre: "Terminal de Pasajeros de Guanare", coordenada_utm: "POINT(-63.19213456439400289 9.7299526298020993)", avenida: "", calle: "", sector: "urb. Temaca", tipo_parada: "Terminal Publico", parroquia_v_id: "CAPITAL GUANARE", ciudad_id: "GUANARE", estado_id: "PORTUGUESA"},
    {nombre: "Terminal de Pasajeros de Acarigua ", coordenada_utm: "POINT(-67.87111218760699671 10.22769316824800079)", avenida: "Gonzalo Barrios", calle: "", sector: "Barrio La Villa", tipo_parada: "Terminal Publico", parroquia_v_id: "CAPITAL PAEZ", ciudad_id: "ACARIGUA", estado_id: "PORTUGUESA"},
    {nombre: "Terminal de pasajeros de Villa Bruzual", coordenada_utm: "POINT(-66.54409200940099822 10.46982528027099946)", avenida: "9", calle: "", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "CAPITAL TUREN", ciudad_id: "VILLA BRUZUAL", estado_id: "PORTUGUESA"},
    {nombre: "Terminal de Pasajeros Rafael Tacho Linares", coordenada_utm: "", avenida: "Argimiro Gabaldon", calle: "", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "CAPITAL SUCRE", ciudad_id: "BISCUCUY", estado_id: "PORTUGUESA"},
    {nombre: "Terminal de Pasajeros de Guanarito", coordenada_utm: "", avenida: "José Antonio Páez", calle: "", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "CAPITAL GUANARITO", ciudad_id: "GUANARITO", estado_id: "PORTUGUESA"},
    {nombre: "Terminal de Pasajeros de Santa Rosalía", coordenada_utm: "", avenida: "", calle: "", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "CAPITAL SANTA ROSALIA", ciudad_id: "SANTA ROSALIA", estado_id: "PORTUGUESA"},
    {nombre: "Terminal de Pasajeros de Cumanacoa", coordenada_utm: "POINT(-62.74660845252599728 8.28801478070630004)", avenida: "Sucre", calle: "", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "CUMANACOA", ciudad_id: "CUMANACOA", estado_id: "SUCRE"},
    {nombre: "Terminal de Pasajeros de Jesús Freites", coordenada_utm: "POINT(-63.91978159749000099 10.25449293745499979)", avenida: "", calle: "", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "RENDON", ciudad_id: "CARIACO", estado_id: "SUCRE"},
    {nombre: "Terminal de Pasajeros Cumaná", coordenada_utm: "POINT(-64.38306274419599617 9.30929493006940056)", avenida: "Las Palomas", calle: "", sector: "Barrio El Dique", tipo_parada: "Terminal Publico", parroquia_v_id: "CAPITAL BOLIVAR", ciudad_id: "CUMANA", estado_id: "SUCRE"},
    {nombre: "Terminal de Pasajeros de Francisco Bermúdez", coordenada_utm: "POINT(-71.66332226361099345 8.60980673458190005)", avenida: "8 Pérez", calle: "", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "BOLIVAR", ciudad_id: "CARUPANO", estado_id: "SUCRE"},
    {nombre: "Terminal de Pasajeros Gabriel Andaria", coordenada_utm: "", avenida: "", calle: "", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "ROMULO GALLLEGOS", ciudad_id: "CASANAY", estado_id: "SUCRE"},
    {nombre: "Terminal de Pasajeros de Santa Fé", coordenada_utm: "", avenida: "", calle: "", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "CAPITAL BOLIVAR", ciudad_id: "SANTA FE", estado_id: "SUCRE"},
    {nombre: "Terminal de Pasajeros Expresos Occidente ", coordenada_utm: "POINT(-67.62852664618399956 10.28890314792799998)", avenida: "Francisco Javier Garcia de Hevia", calle: "5", sector: "", tipo_parada: "Terminal Privado", parroquia_v_id: "LA CONCORDIA", ciudad_id: "SAN CRISTOBAL", estado_id: "TACHIRA"},
    {nombre: "Terminal de Pasajeros de San Antonio", coordenada_utm: "POINT(-63.54850056666400349 10.49842297608100061)", avenida: "", calle: "", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "BOLIVAR", ciudad_id: "SAN ANTONIO DEL TACHIRA", estado_id: "TACHIRA"},
    {nombre: "Terminal de Pasajeros de La Fría", coordenada_utm: "POINT(-62.66416473699099754 8.35901611973040026)", avenida: "", calle: "2", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "BOCA DE GRITA", ciudad_id: "LA FRIA", estado_id: "TACHIRA"},
    {nombre: "Terminal de Pasajeros Expresos Flamingo ", coordenada_utm: "POINT(-69.12498944593299655 9.32916278374630004)", avenida: "", calle: "9", sector: "Urb. Jose Gregorio Hernandez", tipo_parada: "Terminal Privado", parroquia_v_id: "LA CONCORDIA", ciudad_id: "SAN CRISTOBAL", estado_id: "TACHIRA"},
    {nombre: "Terminal de Pasajeros Genaro Méndez", coordenada_utm: "POINT(-63.20132601298200115 9.73676160225340048)", avenida: "Francisco Javier Garcia de Hevia", calle: "", sector: "Urb. Colina de Torbes", tipo_parada: "Terminal Publico", parroquia_v_id: "LA CONCORDIA", ciudad_id: "SAN CRISTOBAL", estado_id: "TACHIRA"},
    {nombre: "Terminal de Pasajeros Union de Conductores Vargas", coordenada_utm: "POINT(-64.18543149058599795 10.47224266292099948)", avenida: "Los pinos", calle: "", sector: "", tipo_parada: "Terminal Privado", parroquia_v_id: "JAUREGUI", ciudad_id: "LA GRITA", estado_id: "TACHIRA"},
    {nombre: "Terminal de Pasajeros Santa Bárbara de la Yeguera", coordenada_utm: "", avenida: "", calle: "", sector: "Sector El Japón", tipo_parada: "Terminal Publico", parroquia_v_id: "JUNIN", ciudad_id: "SANTA BARBARA DE LA YEGUERA", estado_id: "TACHIRA"},
    {nombre: "Terminal de Pasajeros Expresos Ayacucho", coordenada_utm: "", avenida: "", calle: "4", sector: "Sector Casco Central", tipo_parada: "Terminal Privado", parroquia_v_id: "RIVAS BERTI", ciudad_id: "COLON", estado_id: "TACHIRA"},
    {nombre: "Terminal de Pasajeros Expresos los Llanos", coordenada_utm: "", avenida: "Rotaria ", calle: "", sector: "", tipo_parada: "Terminal Privado", parroquia_v_id: "LA CONCORDIA", ciudad_id: "SAN CRISTOBAL", estado_id: "TACHIRA"},
    {nombre: "Terminal de Pasajeros San Juan de Cólon", coordenada_utm: "", avenida: "", calle: "", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "AYACUCHO", ciudad_id: "COLON", estado_id: "TACHIRA"},
    {nombre: "Terminal de Pasajeros Tatén Díaz", coordenada_utm: "POINT(-71.12714548256700198 9.96685468264679919)", avenida: "", calle: "", sector: "Urb. El Castillo", tipo_parada: "Terminal Publico", parroquia_v_id: "VALMORA RODRIGUEZ", ciudad_id: "VALMORA RODRIGUEZ", estado_id: "TRUJILLO"},
    {nombre: "Terminal de Pasajeros Trujillo", coordenada_utm: "POINT(-67.08493114150499537 10.36542277865500061)", avenida: "La Paz", calle: "", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "MATRIZ", ciudad_id: "TRUJILLO", estado_id: "TRUJILLO"},
    {nombre: "Terminal de Pasajeros de Valera", coordenada_utm: "POINT(-64.20435126498999523 10.55442539111900047)", avenida: "", calle: "8", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "JUAN IGNACIO MONTILLA", ciudad_id: "VALERA", estado_id: "TRUJILLO"},
    {nombre: "Terminal de Pasajeros Francisco de Miranda", coordenada_utm: "", avenida: "Via Panamericana", calle: "", sector: "Sector 12 de Octubre", tipo_parada: "Terminal Publico", parroquia_v_id: "EL DIVIDIVE", ciudad_id: "DIVIDIVE", estado_id: "TRUJILLO"},
    {nombre: "Bocono", coordenada_utm: "", avenida: "Eusebio Baptista ", calle: "", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "BOCONO", ciudad_id: "BOCONO", estado_id: "TRUJILLO"},
    {nombre: "Terminal de Pasajeros Catia la Mar", coordenada_utm: "", avenida: "El Ejercito", calle: "", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "CATIA LA MAR", ciudad_id: "CATIA LA MAR", estado_id: "VARGAS"},
    {nombre: "Terminal de Pasajeros Chivacoa", coordenada_utm: "POINT(-66.97083549121400381 10.37162819025600058)", avenida: "", calle: "", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "CAPITAL BRUZUAL", ciudad_id: "CHIVACOA", estado_id: "YARACUY"},
    {nombre: "Terminal de Pasajeros de Urariche", coordenada_utm: "", avenida: "Romulo Betancourt", calle: "", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "CAPITAL URABICHE", ciudad_id: "URABICHE", estado_id: "YARACUY"},
    {nombre: "Terminal de Pasajeros de Nirgua", coordenada_utm: "", avenida: "Principal de Nirgua", calle: "", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "CAPITAL NIRGUA", ciudad_id: "NIRGUA", estado_id: "YARACUY"},
    {nombre: "Terminal de Pasajeros de Yumare", coordenada_utm: "", avenida: "", calle: "", sector: "Sector Uno", tipo_parada: "Terminal Publico", parroquia_v_id: "CAPITAL SAN FELIPE", ciudad_id: "NIRGUA", estado_id: "YARACUY"},
    {nombre: "Terminal Nuevo de San Felípe", coordenada_utm: "POINT(-71.98006283832999941 8.12994647314999952)", avenida: "5", calle: "", sector: "Zona Industrial", tipo_parada: "Terminal Publico", parroquia_v_id: "CAPITAL INDEPENDENCIA", ciudad_id: "SAN FELIPE", estado_id: "YARACUY"},
    {nombre: "Terminal de Pasajeros de Caja Seca", coordenada_utm: "POINT(-69.66297848348699517 11.4070481044589993)", avenida: "", calle: "Los Maestros", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "Zona En ReclamaciONn Estado Zulia/Estado Merida", ciudad_id: "CAJA SECA", estado_id: "MERIDA"},
    {nombre: "Terminal de Pasajeros de Villa del Rosario", coordenada_utm: "POINT(-63.25101933133999665 10.66996037523399998)", avenida: "23", calle: "", sector: "Barrio Trujillo", tipo_parada: "Terminal Publico", parroquia_v_id: "BOLIVAR", ciudad_id: "VILLA DEL RORARIO", estado_id: "ZULIA"},
    {nombre: "Terminal de Pasajeros de Machiques", coordenada_utm: "POINT(-66.94350414806700655 10.48718110118499958)", avenida: "", calle: "Libertad", sector: "Barrio Servio Tulio Peña", tipo_parada: "Terminal Publico", parroquia_v_id: "BOLIVAR", ciudad_id: "MACHIQUES", estado_id: "ZULIA"},
    {nombre: "Terminal de Pasajeros Cabimas", coordenada_utm: "POINT(-67.42461875636399782 8.92119637061479942)", avenida: "", calle: "", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "BOLIVAR", ciudad_id: "CABIMAS", estado_id: "ZULIA"},
    {nombre: "Terminal de Pasajeros Simón Bolivar", coordenada_utm: "POINT(-71.85729620276499929 10.81834817930499959)", avenida: "", calle: "192", sector: "Barrio Polar", tipo_parada: "Terminal Publico", parroquia_v_id: "BOLIVAR", ciudad_id: "SAN FRANCISCO", estado_id: "ZULIA"},
    {nombre: "Terminal de Pasajeros Maracaibo", coordenada_utm: "POINT(-66.37442703557999835 10.27893874114099937)", avenida: "17", calle: "106", sector: "Barrio El Poniente", tipo_parada: "Terminal Publico", parroquia_v_id: "BOLIVAR", ciudad_id: "MARACAIBO", estado_id: "ZULIA"},
    {nombre: "Terminal de Pasajeros Sinamaica", coordenada_utm: "POINT(-70.78029520259900664 9.43858340298369924)", avenida: "Carretera Paraguaipoa", calle: "", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "BOLIVAR", ciudad_id: "SINAMAICA", estado_id: "ZULIA"},
    {nombre: "Terminal de Pasajeros San Rafael del Mojan", coordenada_utm: "POINT(-64.673457259816999 10.17959945457299931)", avenida: "1", calle: "", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "BOLIVAR", ciudad_id: "SAN RAFAEL DEL MONJAN", estado_id: "ZULIA"},
    {nombre: "Terminal de Pasajeros Mene Grande", coordenada_utm: "POINT(-66.90960665947000052 10.50033460875200042)", avenida: "", calle: "", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "BOLIVAR", ciudad_id: "MENE GRANDE", estado_id: "ZULIA"},
    {nombre: "Terminal de pasajeros de Ciudad Ojeda", coordenada_utm: "POINT(-71.61989520469400361 10.64873150764099918)", avenida: "", calle: "", sector: "Urbanización Senior", tipo_parada: "Terminal Publico", parroquia_v_id: "BOLIVAR", ciudad_id: "CIUDADA OJEDA", estado_id: "ZULIA"},
    {nombre: "Terminal de Pasajeros de la Cañada", coordenada_utm: "POINT(-71.61941471327399711 10.63554624854899977)", avenida: "", calle: "", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "BOLIVAR", ciudad_id: "CONCEOPCION DEL SUR", estado_id: "ZULIA"},
    {nombre: "Terminal de Pasajeros de Puertos de Altagracia", coordenada_utm: "POINT(-64.64851479683899527 10.21293287149400086)", avenida: "Valmore Rodríguez", calle: "", sector: "Sector El Pare", tipo_parada: "Terminal Publico", parroquia_v_id: "BOLIVAR", ciudad_id: "LOS PUERTOS DE ALTAGRACIA", estado_id: "ZULIA"},
    {nombre: "Terminal de Pasajeros Ana Maria Campos", coordenada_utm: "POINT(-67.04234195979999811 10.34421445042000087)", avenida: "", calle: "", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "BOLIVAR", ciudad_id: "LA SIERRITA", estado_id: "ZULIA"},
    {nombre: "Terminal de Pasajeros Santa Barbara", coordenada_utm: "", avenida: "Quinta", calle: "", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "BOLIVAR", ciudad_id: "SANTA BARBARA ", estado_id: "ZULIA"},
    {nombre: "Terminal de Pasajeros Francisco Javier Pulgar", coordenada_utm: "", avenida: "", calle: "", sector: "", tipo_parada: "Terminal Publico", parroquia_v_id: "BOLIVAR", ciudad_id: "PUEBLO NUEVO ", estado_id: "ZULIA"},
    {nombre: "Terminal de Pasajeros Aeroexpresos Ejecutivos ", coordenada_utm: "POINT(-70.59773360235199391 9.32000420547660013)", avenida: "", calle: "90", sector: "Sector Nueva Via", tipo_parada: "Terminal Privado", parroquia_v_id: "CHIQUINQUIRA", ciudad_id: "MARACAIBO", estado_id: "ZULIA"}])

paradas_array.each do |p|
  parada = Parada.new(nombre: p[:nombre], coordenada_utm: p[:coordenada_utm], avenida: p[:avenida], calle: p[:calle], sector: p[:sector], tipo_parada: p[:tipo_parada], parroquia_v_id: buscador_parroquias(p[:parroquia_v_id]), ciudad_id: buscador_ciudades(p[:ciudad_id]), estado_id: buscador_estados(p[:estado_id]))
  parada.save(validate: false)
end