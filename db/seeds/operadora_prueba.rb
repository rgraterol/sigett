printf "<================= SEMBRANDO OPERADORA DE PRUEBA"
operadora = OperadoraTransporteP.new(razon_social: "WEBSOFTMERIDA", rif: "V-123232333", email: "zatkiel837@gmail.com", telefono: "", movil: "4262222222", fax: "", observacion: "", avenida: "libertador", calle: "", urbanizacion: "", edificio_cc: "", local: "", piso: "", sociedad_mercantil_otros: "", rif_imagen: "ley_universidades.pdf", documento_acreditacion: "ley_universidades.pdf", acta_socios: "ley_universidades.pdf", ciudad_id: 3, parroquia_v_id: 1139, usuario_id: 2, sociedad_mercantil_id: 2)
operadora.save(validate: false)

servicio = ServicioOperadoraC.new(codigo_rotp: "CSAS2222", fecha_expedicion: "2015-03-18", fecha_vencimiento: "2025-03-18", modalidad_servicio: ModalidadServicio.find_by_abreviacion('sub_urb_colectivo'), completado: true, dt: "ley_universidades.pdf", operadora_transporte_p_id: 1, estado: 2, cps: "ley_universidades.pdf")
servicio.save(validate: false)


Flota.create!([
                  {cupo_asignado: 3, fecha_autorizacion: "2025-03-18", fecha_vence_autorizacion: "2026-03-18", fecha_ultima_renovacion: nil, tiene_ruta: false, servicio_operadora_c_id: 1}
              ])

SeguroVehiculo.create([
                           {rif: nil, num_poliza: "32212121", documento_poliza: "ley_universidades.pdf", fecha_vencimiento: "2015-03-18", aseguradora_id: 1, vehiculo_id: nil, flota_id: 1}
                       ])

Ruta.create!([
                 {num_ruta: "001", fecha_autorizacion: "2015-03-18", tipo: false, ida_vuelta: false, flota_id: 1},
                 {num_ruta: "002", fecha_autorizacion: "2015-03-18", tipo: false, ida_vuelta: false, flota_id: 1}
             ])

Horario.create!([
                    {hora: "2000-01-01 13:55:00", ida: false, dia: "Todos los Dias", ruta_id: 1},
                    {hora: "2000-01-01 19:55:00", ida: true, dia: "Todos los Dias", ruta_id: 1},
                    {hora: "2000-01-01 08:00:00", ida: false, dia: "Todos los Dias", ruta_id: 2},
                    {hora: "2000-01-01 18:00:00", ida: true, dia: "Todos los Dias", ruta_id: 2}
                ])

Toque.create!([
                  {tipo_parada: "Origen", tipo: "terminal_publico", ruta_id: 1, next_id: 2, prev_id: nil, parada_id: 78},
                  {tipo_parada: "Destino", tipo: "terminal_publico", ruta_id: 1, next_id: nil, prev_id: 2, parada_id: 78},
                  {tipo_parada: "Intermedio", tipo: "terminal_publico", ruta_id: 1, next_id: 3, prev_id: 1, parada_id: 1},
                  {tipo_parada: "Destino", tipo: "terminal_publico", ruta_id: 2, next_id: nil, prev_id: 4, parada_id: 78},
                  {tipo_parada: "Origen", tipo: "terminal_publico", ruta_id: 2, next_id: 5, prev_id: nil, parada_id: 78}
              ])
Vehiculo.create!([
                     {placa: "LMAO12", s_carroceria: "ABC1238", ano: "2014", marca: "Lamborguini", modelo: "MURCIELAGO", color: "NULL", capacidad: 2, puesto: 100, serial_motor: nil, tipo_vehiculo: "BUSETA", propietario: "JESUS MANUEL SALCEDO TERAN", identificacion_propietario: "V 19145156 1", uso: "", tipo_unidad: nil, status: nil, flota_id: 1},
                     {placa: "LOL123", s_carroceria: "ABC1237", ano: "2014", marca: "Ferrari", modelo: "SKYLINE", color: "NULL", capacidad: 5, puesto: 3, serial_motor: nil, tipo_vehiculo: "MOTO", propietario: "JONATHAN TIRATE UN PASO MONSALVE GIRALDO", identificacion_propietario: "V 18797889 1", uso: "", tipo_unidad: nil, status: nil, flota_id: 1},
                     {placa: "XYZ126", s_carroceria: "XYZ126", ano: "2000", marca: "Chevrolet", modelo: "RAM", color: "NULL", capacidad: 7, puesto: 2, serial_motor: nil, tipo_vehiculo: "CAMION", propietario: "", identificacion_propietario: " ", uso: "", tipo_unidad: nil, status: nil, flota_id: 1}
                 ])