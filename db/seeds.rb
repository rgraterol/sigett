# -*- encoding : utf-8 -*-

printf "=================>Creando Usuarios\n"
UsuarioSput.create!( nombre: "PROBADOR",
                     apellido: "UNO",
                     numero_doc: "191",
                     nacionalidad: "V",
                     movil: "4247798988",
                     pregunta_secreta: "quien es",
                     respuesta:"chuy"
)

UsuarioSput.create!( nombre: "PROBADOR",
                     apellido: "DOS",
                     numero_doc: "192",
                     nacionalidad: "V",
                     movil: "4247528963",
                     pregunta_secreta: "ola ke ase",
                     respuesta:"trabajando "
)

UsuarioSput.create!( nombre: "admin",
                     apellido: "admin",
                     numero_doc: "156",
                     nacionalidad: "V",
                     movil: "4247798988",
                     pregunta_secreta: "quien es",
                     respuesta:"chuy"
)

#usuario que esta registrado y enlazado al SPUT debe ser borrado al entrar en
#producción
userbiosoft = SessionUser.create!(email: "biosoft@correo.com",
                                  email_confirmation: "biosoft@correo.com",
                                  password:              "12345678",
                                  password_confirmation: "12345678",
                                  confirmed_at: Date.today,
                                  usuario_sput_id: 1
)

Usuario.create!(
    estado_representante: 2,
    rif: 'V1911',
    usuario_sput_id:1, #borrar esta restriccion por ahora se queda
    remote_cedula_imagen_url: 'http://www.oocities.org/alvarezmiguel_uny/expediente/cedula.jpg',
    session_user_id: userbiosoft.id,
    direccion: 'Merida'
)

userViviana = SessionUser.create!(email: "biosoft2@correo.com",
                                  email_confirmation: "biosoft2@correo.com",
                                  password:              "12345678",
                                  password_confirmation: "12345678",
                                  confirmed_at: Date.today,
                                  usuario_sput_id: 2
)

Usuario.create!(
    estado_representante: 2,
    rif: 'V1922',
    usuario_sput_id:2, #borrar esta restriccion por ahora se queda
    remote_cedula_imagen_url: 'http://www.oocities.org/alvarezmiguel_uny/expediente/cedula.jpg',
    session_user_id: userViviana.id,
    direccion: 'Merida'
)

#sociedades mercantiles
SociedadMercantil.create!(codigo: "S.A.")
SociedadMercantil.create!(codigo: "C.A.")
SociedadMercantil.create!(codigo: "A.C.")
SociedadMercantil.create!(codigo: "S.C.")
SociedadMercantil.create!(codigo: "S.C.A.")
SociedadMercantil.create!(codigo: "S.C.S.")
SociedadMercantil.create!(codigo: "S.R.L.")
SociedadMercantil.create!(codigo: "S.N.C.")
SociedadMercantil.create!(codigo: "C.R.L.")
SociedadMercantil.create!(codigo: "Otros")

# usuario para administrar. Desde aca entra a producción
ref_usuario_id =  Rails.env.development? ? 3 : 1

useradmin = SessionUser.create!(  email: "admin@admin.com",
                                  email_confirmation: "admin@admin.com",
                                  password:              "12345678",
                                  password_confirmation: "12345678",
                                  confirmed_at: Date.today,
                                  usuario_sput_id: ref_usuario_id
)
Usuario.create!(
    estado_representante: 2,
    rif: 'V1569',
    usuario_sput_id:ref_usuario_id, #borrar esta restriccion por ahora se queda
    remote_cedula_imagen_url: 'http://www.oocities.org/alvarezmiguel_uny/expediente/cedula.jpg',
    session_user_id: useradmin.id,
    direccion: 'Caracas'
)

ModalidadServicio.create!([
  {nombre: 'Por Puesto SubUrbano', abreviacion: 'sub_urb_p_puesto', rotp: 'PS'},
  {nombre: 'Colectivo SubUrbano', abreviacion: 'sub_urb_colectivo', rotp: 'CS'},
  {nombre: 'Por Puesto InterUrbano', abreviacion: 'int_urb_p_puesto', rotp: 'PI'},
  {nombre: 'Colectivo InterUrbano', abreviacion: 'int_urb_colectivo', rotp: 'CI'},
  {nombre: 'Taxi', abreviacion: 'taxi', rotp: 'TX'},
  {nombre: 'Moto Taxi', abreviacion: 'moto_taxi', rotp: 'MT'},
  {nombre: 'Turismo', abreviacion: 'turismo', rotp: 'TT'},
  {nombre: 'Periferico InterUrbano', abreviacion: 'periferico_i', rotp: 'PE'},
  {nombre: 'Periferico SubUrbano', abreviacion: 'periferico_s', rotp: 'PE'},
  {nombre: 'Escolar', abreviacion: 'escolar', rotp: 'TE'},
  {nombre: 'Alquiler de Vehiculo con o sin chofer', abreviacion: 'alquiler', rotp: 'TA'}
 ])

printf "<=================Fin de la siembra=================>\n"