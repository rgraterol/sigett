class AddIndexToUsuarioSputs < ActiveRecord::Migration
  def change
    add_index :usuario_sputs , :numero_doc, unique: true
  end
end
