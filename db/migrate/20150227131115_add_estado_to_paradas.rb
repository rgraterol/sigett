class AddEstadoToParadas < ActiveRecord::Migration
  def change
    add_reference :paradas, :estado, index: true
  end
end
