class CreateUsuarioSputs < ActiveRecord::Migration
  def change
    create_table :usuario_sputs do |t|
      t.string :nombre  , :limit  => 60, null: false
      t.string :apellido, :limit  => 50, null: false
      t.string :nacionalidad , :limit  => 1, null: false
      t.string :numero_doc, :limit  => 8 , null: false
      t.string :telefono_local , :limit  => 10
      t.string :movil   , :limit  => 10, null: false
      t.string :correo_alterno , limit: 50
      t.string :pregunta_secreta , :limit  => 30, null: false
      t.string :respuesta, :limit  => 15, null: false

      t.timestamps
    end
  end
end
