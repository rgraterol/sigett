# -*- encoding : utf-8 -*-
class CreateVehiculos < ActiveRecord::Migration
  def change
    create_table :vehiculos do |t|
      t.string :placa
      t.string :s_carroceria
      t.string :ano
      t.string :marca
      t.string :modelo
      t.string :color
      t.integer :capacidad
      t.integer :puesto, default: 0, null:false
      t.string :serial_motor
      t.string :tipo_vehiculo

      t.string :propietario
      t.string :identificacion_propietario

      t.string :uso, null:false
      ######QUITAR CUANDO SE QUITEN LAS SEEDS; COLUMNA DE MAS
      t.string :tipo_unidad

      t.boolean :status
      t.references :flota, index: true
      t.timestamps
    end
  end
end

