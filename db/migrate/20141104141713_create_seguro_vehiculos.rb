# -*- encoding : utf-8 -*-
class CreateSeguroVehiculos < ActiveRecord::Migration
  def change
    create_table :seguro_vehiculos do |t|
      t.string :rif
      t.string :num_poliza
      #TODO ELIMINAR DOCUMENTO POLIZA SI NO ES NECESARIO EN NINGUNA VISTA
      t.string :documento_poliza

      t.date :fecha_vencimiento

      t.references :aseguradora, index: true
      t.references :vehiculo, index: true
      t.references :flota, index: true

      t.timestamps
    end
  end
end
