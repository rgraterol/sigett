class CreateSociedadMercantils < ActiveRecord::Migration
  def change
    create_table :sociedad_mercantils do |t|
      t.string :codigo

      t.timestamps
    end
  end
end
