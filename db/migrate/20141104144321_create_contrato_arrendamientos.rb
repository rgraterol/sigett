# -*- encoding : utf-8 -*-
class CreateContratoArrendamientos < ActiveRecord::Migration
  def change
    create_table :contrato_arrendamientos do |t|
      t.string :nombre_arrendatario
      t.string :rif_arrendatario
      t.date :fecha
      t.integer :duracion
      t.integer :tomo
      t.integer :folio
      t.string :notaria
      t.references :vehiculo, index: true
      t.timestamps
    end
  end
end

