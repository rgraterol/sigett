class CreateVehiculosInttt < ActiveRecord::Migration
  def change
    create_table :vehiculo_inttts do |t|
      t.string :placa , null: false
      t.date :anio, null: false
      t.string :s_carroceria, null: false
      t.string :color, null: false
      t.string :marca, null: false
      t.string :modelo, null: false
      t.integer :puestos,default:0, null: false
      t.string  :nombre_propietario, null: false
      t.string  :uso , default: "", null:false
      t.string  :tipo_unidad ,default:"", null:false


      t.timestamps
    end
  end
end
