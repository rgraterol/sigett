# -*- encoding : utf-8 -*-
class CreateRutas < ActiveRecord::Migration
  def change
    create_table :rutas do |t|
      t.string :num_ruta
      t.date :fecha_autorizacion
      t.boolean :tipo, default: false
      t.boolean :ida_vuelta, default: false #false = Solo ida y true = Ida u vuelta
      # t.references :municipio, index: true
      t.references :flota, index: true

      t.timestamps
    end
  end
end
