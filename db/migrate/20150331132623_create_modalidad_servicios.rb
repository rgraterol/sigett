class CreateModalidadServicios < ActiveRecord::Migration
  def change
    create_table :modalidad_servicios do |t|
      t.string :nombre
      t.string :abreviacion
      t.string :rotp

      t.timestamps
    end
    add_index :modalidad_servicios, :abreviacion, unique: true
  end
end
