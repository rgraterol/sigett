# -*- encoding : utf-8 -*-
class AddUsuarioSputIndexToUsuario < ActiveRecord::Migration
  def change
    add_index  :usuarios, :usuario_sput_id, unique: true
  end
end

