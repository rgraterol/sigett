# -*- encoding : utf-8 -*-
class CreateHorarios < ActiveRecord::Migration
  def change
    create_table :horarios do |t|
      t.time :hora
      t.boolean :ida # si es 0 es ida, 1 regreso
      # t.string :tipo
      t.string :dia

      t.references :ruta, index: true
      t.timestamps
    end
  end
end
