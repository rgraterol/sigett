# -*- encoding : utf-8 -*-
class CreateToques < ActiveRecord::Migration
  def change
    create_table :toques do |t|
      t.string :tipo_parada
      t.string :tipo
      # t.references :ciudad, index: true
      t.references :ruta, index: true
      t.references :next
      t.references :prev
      t.references :parada, index: true
      t.timestamps
    end
  end
end

