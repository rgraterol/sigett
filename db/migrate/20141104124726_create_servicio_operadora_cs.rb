# -*- encoding : utf-8 -*-
class CreateServicioOperadoraCs < ActiveRecord::Migration
  def change
    create_table :servicio_operadora_cs do |t|
      t.string :codigo_rotp
      t.date :fecha_expedicion
      t.date :fecha_vencimiento
      t.boolean :completado, default: false
      t.string :dt

      t.references :operadora_transporte_p, index: true
      t.references :modalidad_servicio, index: true

      t.timestamps
    end
  end
end

