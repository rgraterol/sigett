# -*- encoding : utf-8 -*-


class CreateOperadoraTransportePs < ActiveRecord::Migration
  def change
    create_table :operadora_transporte_ps do |t|
      t.string :razon_social
      t.string :rif
      t.string :email
      t.string :telefono
      t.string :movil
      t.string :fax
      t.string :observacion
      t.string :avenida
      t.string :calle
      t.string :urbanizacion
      t.string :edificio_cc
      t.string :local
      t.string :piso
      t.string :sociedad_mercantil_otros
      t.string :rif_imagen
      t.string :documento_acreditacion
      t.string :acta_socios

      # t.string :nombre
      # t.string :razon_social
      # t.string :rif
      # t.string :email
      # t.string :direccion_fiscal
      # t.string :telefono
      # t.string :movil
      # t.string :url
      # t.string :codigo_otp
      t.references :ciudad, index: true
      t.references :parroquia_v, index: true
      t.references :usuario, index: true
      t.references :sociedad_mercantil, index: true
      t.timestamps
    end
  end
end

