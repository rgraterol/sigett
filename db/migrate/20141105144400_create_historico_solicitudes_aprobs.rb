# -*- encoding : utf-8 -*-
class CreateHistoricoSolicitudesAprobs < ActiveRecord::Migration
  def change
    create_table :historico_solicitudes_aprobs do |t|
      t.string :tipo_certificacion
      t.string :descripcion
      t.date :fecha_expedicion
      t.string :documento_historico
      t.string :estatus_historico, default: 'Aprobadas'
      t.references :servicio_operadora_c, index: true

      t.timestamps
    end
  end
end

