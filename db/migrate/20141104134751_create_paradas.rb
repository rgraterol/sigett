# -*- encoding : utf-8 -*-
class CreateParadas < ActiveRecord::Migration
  def change
    create_table :paradas do |t|
      t.string :nombre
      t.string :coordenada_utm
      t.string :avenida
      t.string :calle
      t.string :sector
      t.string :tipo_parada
      t.references :parroquia_v, index: true
      t.references :ciudad, index: true

      t.timestamps
    end
  end
end
