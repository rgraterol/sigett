class CreateVistaBd < ActiveRecord::Migration
  def up
    execute '
      create view vista_estados_modalidads as

      SELECT
          operadora_transporte_ps.id,
          operadora_transporte_ps.razon_social,
          operadora_transporte_ps.rif,
          operadora_transporte_ps.email,
          operadora_transporte_ps.movil,
          estados.id as estado_id,
          estados.nombre
      FROM
        public.operadora_transporte_ps,
        public.estados,
        public.parroquia_vs,
        public.municipios,
        public.servicio_operadora_cs
      WHERE
        operadora_transporte_ps.id = servicio_operadora_cs.operadora_transporte_p_id AND
        estados.id = municipios.estado_id AND
        parroquia_vs.id = operadora_transporte_ps.parroquia_v_id AND
        municipios.id = parroquia_vs.municipio_id
      group by
        operadora_transporte_ps.id,
        operadora_transporte_ps.razon_social,
        estados.id,
        estados.nombre
      order by
        operadora_transporte_ps.id
    '

    execute '
      create view vista_modalidad_tipo_unidads as
        SELECT
          operadora_transporte_ps.id,
          operadora_transporte_ps.rif,
          operadora_transporte_ps.razon_social,
          servicio_operadora_cs.id as servicio_id,
          estados.id as estado_id,
          estados.nombre,
          vehiculos.id as vehiculos_id,
          vehiculos.placa,
          vehiculos.uso,
          vehiculos.tipo_unidad
        FROM
          public.estados,
          public.municipios,
          public.parroquia_vs,
          public.servicio_operadora_cs,
          public.operadora_transporte_ps,
          public.flotas,
          public.vehiculos
        WHERE
          estados.id = municipios.estado_id AND
          municipios.id = parroquia_vs.municipio_id AND
          parroquia_vs.id = operadora_transporte_ps.parroquia_v_id AND
          servicio_operadora_cs.id = flotas.servicio_operadora_c_id AND
          operadora_transporte_ps.id = servicio_operadora_cs.operadora_transporte_p_id AND
          flotas.id = vehiculos.flota_id
        group by
          operadora_transporte_ps.id,
          operadora_transporte_ps.razon_social,
          servicio_operadora_cs.id,
          estados.id,
          estados.nombre,
          vehiculos.id,
          vehiculos.placa,
          vehiculos.uso,
          vehiculos.tipo_unidad
        order by
          operadora_transporte_ps.id
    '

    execute '
        create view vista_operadoras_estados as
          SELECT
            estados.nombre as nombre_estado,
            estados.id as id_estado,
            operadora_transporte_ps.id as id_operadora,
            operadora_transporte_ps.razon_social,
            operadora_transporte_ps.rif, operadora_transporte_ps.email,
            operadora_transporte_ps.movil,
            servicio_operadora_cs.id as servicio_id,
            municipios.id as municipio_id,
            municipios.nombre as nombre_municipio,
            parroquia_vs.id as parroquia_id,
            parroquia_vs.nombre as nombre_parroquia
          FROM
            public.estados,
            public.municipios,
            public.parroquia_vs,
            public.operadora_transporte_ps,
            public.servicio_operadora_cs
          WHERE
            estados.id = municipios.estado_id AND
            municipios.id = parroquia_vs.municipio_id AND
            parroquia_vs.id = operadora_transporte_ps.parroquia_v_id AND
            operadora_transporte_ps.id = servicio_operadora_cs.operadora_transporte_p_id

          ORDER BY
            estados.nombre ASC;

    '

    execute '
      create view vista_modalidad_ciudads as
        SELECT
          operadora_transporte_ps.id,
          operadora_transporte_ps.razon_social,
          operadora_transporte_ps.rif,
          operadora_transporte_ps.email,
          operadora_transporte_ps,movil,
          servicio_operadora_cs.id as servicio_id,
          ciudads.id as ciudad_id,
          ciudads.nombre as nom_ciudad,
          estados.id as estado_id,
          estados.nombre as nom_estado
        FROM
          public.operadora_transporte_ps,
          public.servicio_operadora_cs,
          public.ciudads,
          public.estados
        WHERE
          operadora_transporte_ps.id = servicio_operadora_cs.operadora_transporte_p_id AND
          ciudads.id = operadora_transporte_ps.ciudad_id AND
          estados.id = ciudads.estado_id
       GROUP BY
          operadora_transporte_ps.id,
          operadora_transporte_ps.razon_social,
          operadora_transporte_ps.rif,
          operadora_transporte_ps.email,
          operadora_transporte_ps,movil,
          servicio_operadora_cs.id,
          ciudads.id,
          ciudads.nombre,
          estados.id,
          estados.nombre
    '
  end

  def down
    execute 'drop view vista_estados_modalidads;'
    execute 'drop view vista_modalidad_tipo_unidads;'
    execute 'drop view vista_operadoras_estados;'
    execute 'drop view vista_modalidad_ciudads;'
  end
end
