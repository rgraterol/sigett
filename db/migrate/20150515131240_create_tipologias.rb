class CreateTipologias < ActiveRecord::Migration
  def change
    create_table :tipologias do |t|
      t.string :id_clase
      t.string :nombre

      t.timestamps null: false
    end

    create_table :modalidad_servicios_tipologias do |t|
      t.references :tipologia, :modalidad_servicio, index: true
      t.timestamps null: false
    end

    add_index :modalidad_servicios_tipologias, [:tipologia_id, :modalidad_servicio_id], name: :my_index
  end
end