# -*- encoding : utf-8 -*-
class CreateUsuario < ActiveRecord::Migration
  def change
    create_table :usuarios do |t|
      t.integer :estado_representante , default: 0 , null:false
      t.string :rif , limit: 10, null: false
      t.string :cedula_imagen, null:false
      t.references :session_user, index: true, unique: true
      t.references :usuario_sput , null: false
    end
  end
end

