# -*- encoding : utf-8 -*-
class CreateFlotas < ActiveRecord::Migration
  def change
    create_table :flotas do |t|
      t.integer :cupo_asignado
      t.date :fecha_autorizacion
      t.date :fecha_vence_autorizacion
      t.date :fecha_ultima_renovacion
      t.boolean :tiene_ruta, default: false
      t.references :servicio_operadora_c, index: true

      t.timestamps
    end
  end
end

