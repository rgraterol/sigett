# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150520142805) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "aseguradoras", force: true do |t|
    t.string   "nombre"
    t.string   "rif"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ciudads", force: true do |t|
    t.string   "nombre"
    t.integer  "estado_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ciudads", ["estado_id"], name: "index_ciudads_on_estado_id", using: :btree

  create_table "contrato_arrendamientos", force: true do |t|
    t.string   "nombre_arrendatario"
    t.string   "rif_arrendatario"
    t.date     "fecha"
    t.integer  "duracion"
    t.integer  "tomo"
    t.integer  "folio"
    t.string   "notaria"
    t.integer  "vehiculo_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "contrato_arrendamientos", ["vehiculo_id"], name: "index_contrato_arrendamientos_on_vehiculo_id", using: :btree

  create_table "estados", force: true do |t|
    t.string   "nombre"
    t.string   "codificacion"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "flotas", force: true do |t|
    t.integer  "cupo_asignado"
    t.date     "fecha_autorizacion"
    t.date     "fecha_vence_autorizacion"
    t.date     "fecha_ultima_renovacion"
    t.boolean  "tiene_ruta",               default: false
    t.integer  "servicio_operadora_c_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "flotas", ["servicio_operadora_c_id"], name: "index_flotas_on_servicio_operadora_c_id", using: :btree

  create_table "historico_solicitudes_aprobs", force: true do |t|
    t.string   "tipo_certificacion"
    t.string   "descripcion"
    t.date     "fecha_expedicion"
    t.string   "documento_historico"
    t.string   "estatus_historico",       default: "Aprobadas"
    t.integer  "servicio_operadora_c_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "historico_solicitudes_aprobs", ["servicio_operadora_c_id"], name: "index_historico_solicitudes_aprobs_on_servicio_operadora_c_id", using: :btree

  create_table "horarios", force: true do |t|
    t.time     "hora"
    t.boolean  "ida"
    t.string   "dia"
    t.integer  "ruta_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "horarios", ["ruta_id"], name: "index_horarios_on_ruta_id", using: :btree

  create_table "modalidad_servicios", force: true do |t|
    t.string   "nombre"
    t.string   "abreviacion"
    t.string   "rotp"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "modalidad_servicios", ["abreviacion"], name: "index_modalidad_servicios_on_abreviacion", unique: true, using: :btree

  create_table "modalidad_servicios_tipologias", force: true do |t|
    t.integer  "tipologia_id"
    t.integer  "modalidad_servicio_id"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  add_index "modalidad_servicios_tipologias", ["modalidad_servicio_id"], name: "index_modalidad_servicios_tipologias_on_modalidad_servicio_id", using: :btree
  add_index "modalidad_servicios_tipologias", ["tipologia_id", "modalidad_servicio_id"], name: "my_index", using: :btree
  add_index "modalidad_servicios_tipologias", ["tipologia_id"], name: "index_modalidad_servicios_tipologias_on_tipologia_id", using: :btree

  create_table "municipios", force: true do |t|
    t.string   "nombre"
    t.integer  "estado_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "municipios", ["estado_id"], name: "index_municipios_on_estado_id", using: :btree

  create_table "operadora_transporte_ps", force: true do |t|
    t.string   "razon_social"
    t.string   "rif"
    t.string   "email"
    t.string   "telefono"
    t.string   "movil"
    t.string   "fax"
    t.string   "observacion"
    t.string   "avenida"
    t.string   "calle"
    t.string   "urbanizacion"
    t.string   "edificio_cc"
    t.string   "local"
    t.string   "piso"
    t.string   "sociedad_mercantil_otros"
    t.string   "rif_imagen"
    t.string   "documento_acreditacion"
    t.string   "acta_socios"
    t.integer  "ciudad_id"
    t.integer  "parroquia_v_id"
    t.integer  "usuario_id"
    t.integer  "sociedad_mercantil_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "documento_cooperativa"
  end

  add_index "operadora_transporte_ps", ["ciudad_id"], name: "index_operadora_transporte_ps_on_ciudad_id", using: :btree
  add_index "operadora_transporte_ps", ["parroquia_v_id"], name: "index_operadora_transporte_ps_on_parroquia_v_id", using: :btree
  add_index "operadora_transporte_ps", ["sociedad_mercantil_id"], name: "index_operadora_transporte_ps_on_sociedad_mercantil_id", using: :btree
  add_index "operadora_transporte_ps", ["usuario_id"], name: "index_operadora_transporte_ps_on_usuario_id", using: :btree

  create_table "paradas", force: true do |t|
    t.string   "nombre"
    t.string   "coordenada_utm"
    t.string   "avenida"
    t.string   "calle"
    t.string   "sector"
    t.string   "tipo_parada"
    t.integer  "parroquia_v_id"
    t.integer  "ciudad_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "estado_id"
  end

  add_index "paradas", ["ciudad_id"], name: "index_paradas_on_ciudad_id", using: :btree
  add_index "paradas", ["estado_id"], name: "index_paradas_on_estado_id", using: :btree
  add_index "paradas", ["parroquia_v_id"], name: "index_paradas_on_parroquia_v_id", using: :btree

  create_table "parroquia_vs", force: true do |t|
    t.string   "nombre"
    t.integer  "municipio_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "parroquia_vs", ["municipio_id"], name: "index_parroquia_vs_on_municipio_id", using: :btree

  create_table "rutas", force: true do |t|
    t.string   "num_ruta"
    t.date     "fecha_autorizacion"
    t.boolean  "tipo",               default: false
    t.boolean  "ida_vuelta",         default: false
    t.integer  "flota_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "rutas", ["flota_id"], name: "index_rutas_on_flota_id", using: :btree

  create_table "seguro_vehiculos", force: true do |t|
    t.string   "rif"
    t.string   "num_poliza"
    t.string   "documento_poliza"
    t.date     "fecha_vencimiento"
    t.integer  "aseguradora_id"
    t.integer  "vehiculo_id"
    t.integer  "flota_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "seguro_vehiculos", ["aseguradora_id"], name: "index_seguro_vehiculos_on_aseguradora_id", using: :btree
  add_index "seguro_vehiculos", ["flota_id"], name: "index_seguro_vehiculos_on_flota_id", using: :btree
  add_index "seguro_vehiculos", ["vehiculo_id"], name: "index_seguro_vehiculos_on_vehiculo_id", using: :btree

  create_table "servicio_operadora_cs", force: true do |t|
    t.string   "codigo_rotp"
    t.date     "fecha_expedicion"
    t.date     "fecha_vencimiento"
    t.boolean  "completado",                default: false
    t.string   "dt"
    t.integer  "operadora_transporte_p_id"
    t.integer  "modalidad_servicio_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "estado"
    t.string   "cps"
  end

  add_index "servicio_operadora_cs", ["modalidad_servicio_id"], name: "index_servicio_operadora_cs_on_modalidad_servicio_id", using: :btree
  add_index "servicio_operadora_cs", ["operadora_transporte_p_id"], name: "index_servicio_operadora_cs_on_operadora_transporte_p_id", using: :btree

  create_table "session_users", force: true do |t|
    t.string   "email",                  limit: 50, default: "", null: false
    t.string   "encrypted_password",                default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                     default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "usuario_sput_id",                                null: false
  end

  add_index "session_users", ["confirmation_token"], name: "index_session_users_on_confirmation_token", unique: true, using: :btree
  add_index "session_users", ["email"], name: "index_session_users_on_email", unique: true, using: :btree
  add_index "session_users", ["reset_password_token"], name: "index_session_users_on_reset_password_token", unique: true, using: :btree
  add_index "session_users", ["usuario_sput_id"], name: "index_session_users_on_usuario_sput_id", unique: true, using: :btree

  create_table "sociedad_mercantils", force: true do |t|
    t.string   "codigo"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tipologias", force: true do |t|
    t.string   "id_clase"
    t.string   "nombre"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "toques", force: true do |t|
    t.string   "tipo_parada"
    t.string   "tipo"
    t.integer  "ruta_id"
    t.integer  "next_id"
    t.integer  "prev_id"
    t.integer  "parada_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "toques", ["parada_id"], name: "index_toques_on_parada_id", using: :btree
  add_index "toques", ["ruta_id"], name: "index_toques_on_ruta_id", using: :btree

  create_table "usuario_sputs", force: true do |t|
    t.string   "nombre",           limit: 60, null: false
    t.string   "apellido",         limit: 50, null: false
    t.string   "nacionalidad",     limit: 1,  null: false
    t.string   "numero_doc",       limit: 8,  null: false
    t.string   "telefono_local",   limit: 10
    t.string   "movil",            limit: 10, null: false
    t.string   "correo_alterno",   limit: 50
    t.string   "pregunta_secreta", limit: 30, null: false
    t.string   "respuesta",        limit: 15, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "usuario_sputs", ["numero_doc"], name: "index_usuario_sputs_on_numero_doc", unique: true, using: :btree

  create_table "usuarios", force: true do |t|
    t.integer "estado_representante",            default: 0, null: false
    t.string  "rif",                  limit: 10,             null: false
    t.string  "cedula_imagen",                               null: false
    t.integer "session_user_id"
    t.integer "usuario_sput_id",                             null: false
    t.string  "direccion"
  end

  add_index "usuarios", ["session_user_id"], name: "index_usuarios_on_session_user_id", using: :btree
  add_index "usuarios", ["usuario_sput_id"], name: "index_usuarios_on_usuario_sput_id", unique: true, using: :btree

  create_table "vehiculo_inttts", force: true do |t|
    t.string   "placa",                           null: false
    t.date     "anio",                            null: false
    t.string   "s_carroceria",                    null: false
    t.string   "color",                           null: false
    t.string   "marca",                           null: false
    t.string   "modelo",                          null: false
    t.integer  "puestos",            default: 0,  null: false
    t.string   "nombre_propietario",              null: false
    t.string   "uso",                default: "", null: false
    t.string   "tipo_unidad",        default: "", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "vehiculo_inttts", ["placa"], name: "index_vehiculo_inttts_on_placa", unique: true, using: :btree

  create_table "vehiculos", force: true do |t|
    t.string   "placa"
    t.string   "s_carroceria"
    t.string   "ano"
    t.string   "marca"
    t.string   "modelo"
    t.string   "color"
    t.integer  "capacidad"
    t.integer  "puesto",                     default: 0, null: false
    t.string   "serial_motor"
    t.string   "tipo_vehiculo"
    t.string   "propietario"
    t.string   "identificacion_propietario"
    t.string   "uso",                                    null: false
    t.string   "tipo_unidad"
    t.boolean  "status"
    t.integer  "flota_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "vehiculos", ["flota_id"], name: "index_vehiculos_on_flota_id", using: :btree

end
