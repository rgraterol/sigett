# -*- encoding : utf-8 -*-
require 'test_helper'

class ToquesControllerTest < ActionController::TestCase
  setup do
    @toque = toques(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:toques)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create toque" do
    assert_difference('Toque.count') do
      post :create, toque: {  }
    end

    assert_redirected_to toque_path(assigns(:toque))
  end

  test "should show toque" do
    get :show, id: @toque
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @toque
    assert_response :success
  end

  test "should update toque" do
    patch :update, id: @toque, toque: {  }
    assert_redirected_to toque_path(assigns(:toque))
  end

  test "should destroy toque" do
    assert_difference('Toque.count', -1) do
      delete :destroy, id: @toque
    end

    assert_redirected_to toques_path
  end
end
