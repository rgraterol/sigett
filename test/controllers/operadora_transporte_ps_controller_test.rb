# -*- encoding : utf-8 -*-
require 'test_helper'

class OperadoraTransportePsControllerTest < ActionController::TestCase
  setup do
    @operadora_transporte_p = operadora_transporte_ps(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:operadora_transporte_ps)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create operadora_transporte_p" do
    assert_difference('OperadoraTransporteP.count') do
      post :create, operadora_transporte_p: {  }
    end

    assert_redirected_to operadora_transporte_p_path(assigns(:operadora_transporte_p))
  end

  test "should show operadora_transporte_p" do
    get :show, id: @operadora_transporte_p
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @operadora_transporte_p
    assert_response :success
  end

  test "should update operadora_transporte_p" do
    patch :update, id: @operadora_transporte_p, operadora_transporte_p: {  }
    assert_redirected_to operadora_transporte_p_path(assigns(:operadora_transporte_p))
  end

  test "should destroy operadora_transporte_p" do
    assert_difference('OperadoraTransporteP.count', -1) do
      delete :destroy, id: @operadora_transporte_p
    end

    assert_redirected_to operadora_transporte_ps_path
  end
end
