# -*- encoding : utf-8 -*-
require 'test_helper'

class FlotaControllerTest < ActionController::TestCase
  setup do
    @flotum = flota(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:flota_d)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create flotum" do
    assert_difference('Flotum.count') do
      post :create, flotum: {  }
    end

    assert_redirected_to flotum_path(assigns(:flotum))
  end

  test "should show flotum" do
    get :show, id: @flotum
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @flotum
    assert_response :success
  end

  test "should update flotum" do
    patch :update, id: @flotum, flotum: {  }
    assert_redirected_to flotum_path(assigns(:flotum))
  end

  test "should destroy flotum" do
    assert_difference('Flotum.count', -1) do
      delete :destroy, id: @flotum
    end

    assert_redirected_to flota_path
  end
end
