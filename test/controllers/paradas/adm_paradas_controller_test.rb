require 'test_helper'

class Paradas::AdmParadasControllerTest < ActionController::TestCase
  setup do
    @paradas_adm_parada = paradas_adm_paradas(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:paradas_adm_paradas)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create paradas_adm_parada" do
    assert_difference('Paradas::AdmParada.count') do
      post :create, paradas_adm_parada: {  }
    end

    assert_redirected_to paradas_adm_parada_path(assigns(:paradas_adm_parada))
  end

  test "should show paradas_adm_parada" do
    get :show, id: @paradas_adm_parada
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @paradas_adm_parada
    assert_response :success
  end

  test "should update paradas_adm_parada" do
    patch :update, id: @paradas_adm_parada, paradas_adm_parada: {  }
    assert_redirected_to paradas_adm_parada_path(assigns(:paradas_adm_parada))
  end

  test "should destroy paradas_adm_parada" do
    assert_difference('Paradas::AdmParada.count', -1) do
      delete :destroy, id: @paradas_adm_parada
    end

    assert_redirected_to paradas_adm_paradas_path
  end
end
