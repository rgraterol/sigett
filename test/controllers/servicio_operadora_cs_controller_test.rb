# -*- encoding : utf-8 -*-
require 'test_helper'

class ServicioOperadoraCsControllerTest < ActionController::TestCase
  setup do
    @servicio_operadora_c = servicio_operadora_cs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:servicio_operadora_cs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create servicio_operadora_c" do
    assert_difference('ServicioOperadoraC.count') do
      post :create, servicio_operadora_c: {  }
    end

    assert_redirected_to servicio_operadora_c_path(assigns(:servicio_operadora_c))
  end

  test "should show servicio_operadora_c" do
    get :show, id: @servicio_operadora_c
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @servicio_operadora_c
    assert_response :success
  end

  test "should update servicio_operadora_c" do
    patch :update, id: @servicio_operadora_c, servicio_operadora_c: {  }
    assert_redirected_to servicio_operadora_c_path(assigns(:servicio_operadora_c))
  end

  test "should destroy servicio_operadora_c" do
    assert_difference('ServicioOperadoraC.count', -1) do
      delete :destroy, id: @servicio_operadora_c
    end

    assert_redirected_to servicio_operadora_cs_path
  end
end
