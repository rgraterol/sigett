# -*- encoding : utf-8 -*-
require 'test_helper'

class SeguroVehiculosControllerTest < ActionController::TestCase
  setup do
    @seguro_vehiculo = seguro_vehiculos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:seguro_vehiculos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create seguro_vehiculo" do
    assert_difference('SeguroVehiculo.count') do
      post :create, seguro_vehiculo: {  }
    end

    assert_redirected_to seguro_vehiculo_path(assigns(:seguro_vehiculo))
  end

  test "should show seguro_vehiculo" do
    get :show, id: @seguro_vehiculo
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @seguro_vehiculo
    assert_response :success
  end

  test "should update seguro_vehiculo" do
    patch :update, id: @seguro_vehiculo, seguro_vehiculo: {  }
    assert_redirected_to seguro_vehiculo_path(assigns(:seguro_vehiculo))
  end

  test "should destroy seguro_vehiculo" do
    assert_difference('SeguroVehiculo.count', -1) do
      delete :destroy, id: @seguro_vehiculo
    end

    assert_redirected_to seguro_vehiculos_path
  end
end
