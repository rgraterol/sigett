require 'test_helper'

class Admin::ModalidadServiciosControllerTest < ActionController::TestCase
  setup do
    @admin_modalidad_servicio = admin_modalidad_servicios(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:admin_modalidad_servicios)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create admin_modalidad_servicio" do
    assert_difference('Admin::ModalidadServicio.count') do
      post :create, admin_modalidad_servicio: {  }
    end

    assert_redirected_to admin_modalidad_servicio_path(assigns(:admin_modalidad_servicio))
  end

  test "should show admin_modalidad_servicio" do
    get :show, id: @admin_modalidad_servicio
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @admin_modalidad_servicio
    assert_response :success
  end

  test "should update admin_modalidad_servicio" do
    patch :update, id: @admin_modalidad_servicio, admin_modalidad_servicio: {  }
    assert_redirected_to admin_modalidad_servicio_path(assigns(:admin_modalidad_servicio))
  end

  test "should destroy admin_modalidad_servicio" do
    assert_difference('Admin::ModalidadServicio.count', -1) do
      delete :destroy, id: @admin_modalidad_servicio
    end

    assert_redirected_to admin_modalidad_servicios_path
  end
end
