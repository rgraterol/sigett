# -*- encoding : utf-8 -*-
# == Schema Information
#
# Table name: vehiculos
#
#  id                         :integer          not null, primary key
#  placa                      :string(255)
#  s_carroceria               :string(255)
#  ano                        :string(255)
#  marca                      :string(255)
#  modelo                     :string(255)
#  color                      :string(255)
#  capacidad                  :integer
#  puesto                     :integer          default(0), not null
#  serial_motor               :string(255)
#  tipo_vehiculo              :string(255)
#  propietario                :string(255)
#  identificacion_propietario :string(255)
#  uso                        :string(255)      not null
#  tipo_unidad                :string(255)
#  status                     :boolean
#  flota_id                   :integer
#  created_at                 :datetime
#  updated_at                 :datetime
#

require 'test_helper'

class VehiculoTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
