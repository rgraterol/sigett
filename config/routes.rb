# -*- encoding : utf-8 -*-
Rails.application.routes.draw do
  root :to =>'static_pages#home'
  get '/registrado',to: 'static_pages#later_register', as: :registrado

  namespace :admin do
    resources :modalidad_servicios
  end
  namespace :estadistica_module do
    get '/modalidad/todos/estados', to: 'estadistica#modalidad_estado_all', as: :modalidad_estado_all
    get '/modalidad/cupo/estados', to: 'estadistica#modalidad_cupo_autorizado', as: :modalidad_cupo_autorizado
    post '/modalidad/estado', to: 'estadistica#modalidad_estado', as: :modalidad_estado
    post '/modalidad/parroquias', to: 'estadistica#modalidad_parroquias', as: :modalidad_parroquias
    post '/modalidad/ciudad', to: 'estadistica#modalidad_ciudad', as: :modalidad_ciudad
    get '/modalidad/estado/seleccion', to: 'estadistica#selector_ano_modalidad', as: :selector_ano_modalidad
    get '/modalidad/parroquias/seleccion', to: 'estadistica#selector_parroquias', as: :selector_parroquias
    get '/modalidad/ciudad/seleccion', to: 'estadistica#selector_ciudad', as: :selector_ciudad
    post '/unidades/todos/estado/', to: 'estadistica#vencimiento_flota', as: :vencimiento_flota
    get '/vencimiento/ano/seleccion/flota',to: 'estadistica#selector_ano_vencimiento_flota', as: :selector_ano_vencimiento_flota
    get '/vencimiento/ano/seleccion/cps',to: 'estadistica#selector_ano_vencimiento_cps', as: :selector_ano_vencimiento_cps
    post '/vencimiento/ano/cps', to: 'estadistica#vencimiento_cps', as: :vencimiento_cps
    get '/vistas/estados/modalidad', to: 'estadistica#vistas_estados', as: :vistas_estados
    get '/selector/proximas/vencer/flota/', to: 'estadistica#selector_proximas_vencer_flota', as: :selector_proximas_vencer_flota
    post '/proximas/vencer/flota/', to: 'estadistica#proximas_vencer_flota', as: :proximas_vencer_flota
    get '/modalidad/estados/tipounidad/', to: 'estadistica#selector_estados_tipo_unidad', as: :selector_estados_tipo
    post '/modalidad/tipounidad', to: 'estadistica#modalidad_tipo_unidad', as: :modalidad_tipo_unidad
    get '/form/igual/parada/', to: 'estadistica#form_igual_parada', as: :form_igual_parada
    post '/estadistica/igual/parada/', to: 'estadistica#estadistica_igual_parada', as: :estadistica_igual_parada
    get '/meses/seleccion/historicos', to: 'estadistica#selector_meses_historicos', as: :selector_meses_historicos
    post '/tramites/historicos', to: 'estadistica#nro_tramites_realizados_historicos', as: :tramites_historicos
    post '/vistas/parroquias', to: 'estadistica#vistas_parroquias', as: :vistas_parroquias
    post '/vistas/ciudad', to: 'estadistica#vistas_ciudad', as: :vistas_ciudad
  end

  scope module: 'listas' do
    post '/operadoras/listadas/rutas', to: 'listado#listar_por_rutas', as: :listar_por_rutas
    get '/creador/rutas/buscar', to: 'listado#creador_rutas_buscar', as: :creador_rutas
    get '/operadoras/index/', to: 'listado#index_operadora', as: :index_operadora
    get '/operadora/igual/parada', to: 'estadistica#form_igual_parada', as: :form_igual_parada
    post '/resultado/igual/parada', to: 'estadistica#estadistica_igual_parada', as: :estadistica_igual_parada
    post '/operadoras/est/mod', to: 'listado#index_operadora_est_mod', as: :index_est_mod
    post '/operadoras/vencimiento/mes', to: 'listado#index_operadora_mes_ven', as: :index_op_mes_venc
    post '/operadoras/tipounidadvehicular', to: 'listado#index_operadora_tipo_unidad', as: :index_tipo_unidad
    post '/operadoras/vencimiento/mes/cps', to: 'listado#index_op_mes_cps', as: :index_op_mes_cps
    post '/operadoras/igual/parada', to: 'listado#index_op_igual_par', as: :index_op_igual_parada
    post '/operadoras/flota/vencer', to: 'listado#index_ope_flo_venc', as: :index_op_fl_vencer
  end

  namespace :horario_d do
    # get '/index/', to: 'horarios#index', as: :index
    get '/index/', to: 'horarios_dinamico#index', as: :index
    get '/new/', to: 'horarios#new', as: :new
    get '/edit/:id', to: 'horarios#edit', as: :edit
    post '/create/', to: 'horarios#create', as: :create
    patch '/update/:id', to: 'horarios#update', as: :update
    delete '/destroy/:id', to: 'horarios#destroy', as: :destroy
    get '/show/:id', to: 'horarios#show', as: :show
    get 'consultar/show/:id', to: 'horarios#consultar_show', as: :consultar
    post '/horarios/guardar/', to: 'horarios_dinamico#crear_horario', as: :crear_horario
    get '/horarios/eliminar/horario/:id', to: 'horarios_dinamico#eliminar_horario', as: :eliminar_horario_lista
  end

  devise_scope :session_user do
    get 'iniciar_sesion', to: 'devise/sessions#new'
    get 'registro', to: 'session_users/registrations#nuevo_registro'
    get 'cambio_contrasena', to: 'session_users/sessions#edit_password'
    patch 'actualizar_contrasena', to: 'session_users/sessions#update_password'
    get 'recuperar_contrasena', to: 'session_users/passwords#new'
    post 'reestablecer_password',
                    to:'session_users/passwords#permit_reset_password'
    delete 'cerrar_sesion', to: 'devise/sessions#destroy'
  end

  devise_for :session_users ,  :controllers =>
      { :registrations => 'session_users/registrations',
        :sessions =>'session_users/sessions',
        :passwords => 'session_users/passwords'}

  namespace :operadora do
    get '/index/', to: 'operadora_transporte_ps#index', as: :index
    get '/new/', to: 'operadora_transporte_ps#new', as: :new
    get '/edit/:id', to: 'operadora_transporte_ps#edit', as: :edit
    post '/create/', to: 'operadora_transporte_ps#create', as: :create
    patch '/update/:id', to: 'operadora_transporte_ps#update', as: :update
    delete '/destroy/:id', to: 'operadora_transporte_ps#destroy', as: :destroy
    get '/show/:id', to: 'operadora_transporte_ps#show', as: :show
    get '/consultar/', to: 'operadora_transporte_ps#consultar', as: :consultar
    post 'ciudad_municipio', to: 'operadora_transporte_ps#ciudad_municipio', as: :ciudad_municipio
  end
  namespace :servicio do
    get '/index/consulta', to: 'servicio_operadora_cs#index_consulta', as: :index_consulta
    get '/index/', to: 'servicio_operadora_cs#index', as: :index
    get '/new/', to: 'servicio_operadora_cs#new', as: :new
    get '/edit/:id', to: 'servicio_operadora_cs#edit', as: :edit
    post '/create/', to: 'servicio_operadora_cs#create', as: :create
    patch '/update/:id', to: 'servicio_operadora_cs#update', as: :update
    delete '/destroy/:id', to: 'servicio_operadora_cs#destroy', as: :destroy
    get '/show/:id', to: 'servicio_operadora_cs#show', as: :show
    get '/show/consulta/:id', to: 'servicio_operadora_cs#show_consulta', as: :show_consulta
    post '/end/', to: 'servicio_operadora_cs#end_carga', as: :end_carga
    post '/set_codigo_rotp/', to: 'servicio_operadora_cs#set_codigo_rotp', as: :set_codigo_rotp
  end

  namespace :historico do
    get '/index', to: 'historico_solicitudes_aprobs#index', as: :index
    # get '/new/', to: 'historico_solicitudes_aprobs#new', as: :new
    get '/edit/:id_hist/:id/:id_from/:id_servicio', to: 'historico_solicitudes_aprobs#edit', as: :edit
    post '/create/:id/:id_from/:id_servicio', to: 'historico_solicitudes_aprobs#create', as: :create
    patch '/update/:id_hist/:id/:id_from/:id_servicio', to: 'historico_solicitudes_aprobs#update', as: :update
    delete '/destroy/:id_hist/:id/:id_from/:id_servicio', to: 'historico_solicitudes_aprobs#destroy', as: :destroy
    get '/show/:id', to: 'historico_solicitudes_aprobs#show',as: :show
    get '/consulta/final', to: 'historico_solicitudes_aprobs#consulta',as: :consulta
    get 'verificar/regreso', to: 'historico_solicitudes_aprobs#verificar_regreso', as: :verificar_regreso
    # get '/show/:id', to: 'historico_solicitudes_aprobs#show', as: :show
  end

  namespace :asegurador do
    get '/index/', to: 'aseguradoras#index', as: :index
    get '/edit/:id', to: 'aseguradoras#edit', as: :edit
    post '/create/', to: 'aseguradoras#create', as: :create
    patch '/update/:id', to: 'aseguradoras#update', as: :update
    delete '/destroy/:id', to: 'aseguradoras#destroy', as: :destroy
    post '/rif/', to: 'aseguradoras#rif_aseguradora', as: :rif_a
  end

  namespace :flota_d do
    get '/index/', to: 'flota#index', as: :index
    get '/new/', to: 'flota#new', as: :new
    get '/edit/:id', to: 'flota#edit', as: :edit
    post '/create/', to: 'flota#create', as: :create
    patch '/update/:id', to: 'flota#update', as: :update
    delete '/destroy/:id', to: 'flota#destroy', as: :destroy
    get '/show/:id', to: 'flota#show', as: :show
  end

  namespace :seguro do
    get '/index/', to: 'seguro_vehiculos#index', as: :index
    get '/new/', to: 'seguro_vehiculos#new', as: :new
    get '/edit/:id', to: 'seguro_vehiculos#edit', as: :edit
    post '/create/', to: 'seguro_vehiculos#create', as: :create
    patch '/update/:id', to: 'seguro_vehiculos#update', as: :update
    delete '/destroy/:id', to: 'seguro_vehiculos#destroy', as: :destroy
    get '/show/:id', to: 'seguro_vehiculos#show', as: :show
  end

  namespace :ruta_flota do
    get '/index/', to: 'ruta#index', as: :index
    get '/new/', to: 'ruta#new', as: :new
    get '/edit/:id', to: 'ruta#edit', as: :edit
    post '/create/', to: 'ruta#create', as: :create
    patch '/update/:id', to: 'ruta#update', as: :update
    delete '/destroy/:id', to: 'ruta#destroy', as: :destroy
    get '/show/:id', to: 'ruta#show', as: :show
    get '/new_taxi/', to: 'ruta#new_taxi', as: :new_taxi
    get '/edit_taxi/:id', to: 'ruta#edit_taxi', as: :edit_taxi
    patch '/update_taxi/:id', to: 'ruta#update_taxi', as: :update_taxi
    post '/create/taxi', to: 'ruta#create_taxi', as: :create_taxi
    get 'nueva/parada', to: 'ruta#nueva_parada', as: :nueva_parada
    post 'guardar/parada', to: 'ruta#guardar_parada', as: :guardar_parada
  end

  namespace :localizacion_select do
    post 'lista/municipios', to: 'busquedas#estado_municipios', as: 'estado_municipios'
    post 'lista/ciudades', to: 'busquedas#estado_ciudades', as: 'estado_ciudades'
    post 'lista/parroquias', to: 'busquedas#municipio_parroquias', as: 'municipio_parroquias'
    post 'lista/paradas', to: 'busquedas#parroquia_paradas', as: 'parroquia_paradas'
    post 'lista/estado/paradas', to: 'busquedas#estado_tipo_paradas', as: 'estado_tipo_paradas'
  end

  namespace :paradas do
    resources :adm_paradas
    get '/index/', to: 'toques_dinamico#index', as: :index
    # get '/new/', to: 'toques#new', as: :new
    get '/editar/:id_toque', to: 'toques_dinamico#editar', as: :editar
    post '/crear/', to: 'toques_dinamico#crear', as: :crear
    patch '/actualizar/:id_toque', to: 'toques_dinamico#actualizar', as: :actualizar
    get '/eliminar/:id_toque', to: 'toques_dinamico#eliminar', as: :eliminar
    get '/show/:id', to: 'toques_dinamico#show', as: :show
    get 'nueva/parada', to: 'toques_dinamico#nueva_parada', as: :toque_nueva_parada
    post 'guardar/parada', to: 'toques_dinamico#guardar_parada', as: :toque_guardar_parada
    get 'verificar/destino', to: 'toques_dinamico#verificar_destino_ruta', as: :verificar_cierre_toque

    get '/index/ubicacion/parada', to: 'paradas#index', as: :index_ubicacion
    get '/new/ubicacion/parada', to: 'paradas#new', as: :new_ubicacion
    post '/new_by_post/ubicacion/parada', to: 'paradas#new_by_post', as: :new_by_post_ubicacion
    get '/edit/:id/ubicacion/parada', to: 'paradas#edit', as: :edit_ubicacion
    post '/buscar/ubicacion/parada', to: 'paradas#buscar', as: :buscar_ubicacion
    post '/create/ubicacion/parada', to: 'paradas#create', as: :create_ubicacion
    post '/create/ubicacion/parada', to: 'paradas#create_ubicacion', as: :create_ubicacion_admin
    patch '/update/:id/ubicacion/parada', to: 'paradas#update', as: :update_ubicacion
    delete '/destroy/:id/ubicacion/parada', to: 'paradas#destroy', as: :destroy_ubicacion
    get '/show/:id/ubicacion/parada', to: 'paradas#show', as: :show_ubicacion

    post '/cookie/constructor/', to: 'paradas#cookie_contructor', as: :cookie_constructor
    get '/redirect/', to: 'paradas#redirect_parada', as: :redirect_parada
    get '/ruta_flota_new_path/full/', to: 'paradas#redirect_ruta_flota_new', as: :ruta_flota_redirect_new

    get '/error/parada', to: 'paradas#error_parada_ubicacion',as: :error_parada_ubicacion
  end

  namespace :dynamic_select do
    get ':estado_id/ciudades', to: 'ciudades#index', as: 'ciudades'
    get ':estado_id/municipios', to: 'municipios#index', as: 'municipios'
    get ':municipio_id/paradas', to: 'paradas#index', as: 'paradas'
    get ':parroquia_id/paradas/p/', to: 'paradas#index_parroquia', as: 'paradas_parroquia'
    get ':municipio_id/parroquias', to: 'parroquia_vs#index', as: 'parroquia_vs'
    post 'form_igual_parada/estado', to: 'paradas#estado_tipo_parada'

    post '/paradas/', to: 'paradas#parroquia_index', as: 'parroquia_index'
    post '/paradas/estado', to: 'paradas#estado_terminal_index', as: 'parroquia_estado_index'
  end

  #nested resources para seguro RCV
  resources :flota ,controller: 'flota_d/flota', only: :create  do
    resource :seguro_vehiculos , controller: 'seguro/seguro_vehiculos',
             only: [:create,:update]
  end

  #nested resources para crear vehiculos para una flota
  resources :flotas, controller: 'flota_d/flota' do
    resources :vehiculos, controller:  'vehiculos/vehiculos'
  end
  get 'vehiculo/arrendatario', to: 'vehiculos/vehiculos#tiene_arrendatario', as: :vehiculo_arrendatario
  post 'vehiculos/buscar_vehiculo', to: 'vehiculos/vehiculos#buscar_vehiculo'
  get 'vehiculo/verificar/modalidad', to: 'vehiculos/vehiculos#verificar_modalidad', as: :vehiculo_verificar_modalidad

end

