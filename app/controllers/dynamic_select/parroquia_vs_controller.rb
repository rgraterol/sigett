# -*- encoding : utf-8 -*-
module DynamicSelect
  class ParroquiaVsController < ApplicationController
    respond_to :json

    def index
      municipio = ActionController::Parameters.new(municipio: params[:municipio_id]).permit(:municipio)[:municipio]
      @parroquia = ParroquiaV.where(municipio_id: municipio).select('id','nombre')
      puts @parroquia.inspect
      respond_with(@parroquia)
    end

  end
end

