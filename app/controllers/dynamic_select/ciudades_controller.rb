# -*- encoding : utf-8 -*-
module DynamicSelect
  class CiudadesController < ApplicationController
    respond_to :json

    def index
      estado = ActionController::Parameters.new(estado: params[:estado_id]).permit(:estado)[:estado]
      @ciudades = Municipio.where(estado_id: estado).select('id','nombre')
      respond_with(@ciudades)
    end
  end
end

