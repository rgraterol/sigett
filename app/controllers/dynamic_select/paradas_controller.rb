# -*- encoding : utf-8 -*-
module DynamicSelect
  class ParadasController < ApplicationController
    respond_to :json

    def index
      parroquia_v_id = ActionController::Parameters.new(parroquia_v_id: params[:parroquia_v_id]).permit(:parroquia_v_id)[:parroquia_v_id]
      @parroquia_v_id = Parada.where(parroquia_v_id: parroquia_v_id).select('id','nombre')
      respond_with(@parroquia_v_id)
    end

    def parroquia_index
      parroquia_id = params[:parroquia_id]#ActionController::Parameters.new(parroquia_id: params[:parroquia_id]).permit(:parroquia_id)[:parroquia_id]
      # tipo_parada = ActionController::Parameters.new(tipo_parada: params[:toque_tipo_id]).permit(:tipo_parada)[:tipo_parada]
      tipo_parada = params[:toque_tipo]
      puts tipo_parada
      tipo_parada_2 = Parada.tipo_paradas[tipo_parada]
      puts tipo_parada_2
      json_ = []
      json_ << {id: nil, label: "Seleccione una Parada"}
      if tipo_parada.nil?
        para = Parada.where(parroquia_v_id: parroquia_id).select('id','nombre')
      else
        para = Parada.where(parroquia_v_id: parroquia_id, tipo_parada: tipo_parada_2).select('id','nombre')
      end

      para.each do |a|
        json_ << {id: a.id, label: a.nombre}
      end
      render json: json_
    end

    def estado_terminal_index
      estado = params[:estado_id]
      tipo_parada = Parada.tipo_paradas[params[:tipo_parada]]

      json_ = []
      json_ << {id: '', label: "Seleccione una Parada"}

      para =
          Parada.find_by_sql ['SELECT
                        paradas.nombre,
                        paradas.id
                      FROM
                        public.paradas
                      WHERE
                        paradas.tipo_parada = ? AND
                        paradas.estado_id = ?',tipo_parada,estado]

      para.each do |a|
        json_ << {id: a.id, label: a.nombre}
      end
      render json: json_
    end

	def index_parroquia
      parroquia_v_id = ActionController::Parameters.new(parroquia_id: params[:parroquia_id]).permit(:parroquia_id)[:parroquia_id]
      @parroquia_v_id = Parada.where(parroquia_v_id: parroquia_v_id).select('id','nombre')
      puts @parroquia_v_id.inspect
      respond_with(@parroquia_v_id)
    end
	def estado_tipo_parada
      estado      = params[:estado_id]
      tipo_parada = Parada.tipo_paradas[params[:toque_tipo].to_sym]


      if estado.present? && tipo_parada.present?
        parroquia = ParroquiaV.joins(:municipio).where(municipios: {estado_id: estado})
        paradas = Parada.where(parroquia_v_id: parroquia, tipo_parada: tipo_parada)
      else
        paradas = Parada.all
      end
      render json: paradas
    end  end
end

