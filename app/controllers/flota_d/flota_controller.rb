# -*- encoding : utf-8 -*-
module FlotaD
  class FlotaController < ApplicationController
    before_action :set_flotum, only: [:show, :edit, :update, :destroy]
    before_filter :authenticate_session_user!
    before_action :autorized_user
    # GET /flota
    # GET /flota.json
    def index
      @flota = Flota.all
    end

    # GET /flota/1
    # GET /flota/1.json
    def show
    end

    # GET /flota/new
    def new
      @sociedad_mercantil = []
      @sociedad_mercantil << false
      operadora = current_session_user.operadoras.joins(:servicio_operadora_cs).where(servicio_operadora_cs: {id: params[:id]}).last
      if operadora.nil?
      else
        if operadora.ca? || operadora.sa? || operadora.slr?
          @sociedad_mercantil << true
        else
          @sociedad_mercantil << false
        end
      end
      @flota = operadora.servicio_operadora_cs.where(id: params[:id]).last.flota
    end

    # GET /flota/1/edit
    def edit
    end

    # POST /flota
    # POST /flota.json
    def create
      @flota = Flota.new(flota_params)

      respond_to do |format|
        if @flota.save
          format.html { redirect_to @flota, notice: 'flota fue creado con éxito.' }
          format.json { render :show, status: :created, location: @flota }
        else
          format.html { render :new }
          format.json { render json: @flota.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /flota/1
    # PATCH/PUT /flota/1.json
    def update
      respond_to do |format|
        if @flota.update(flota_params)
          format.html { redirect_to @flota, notice: 'Flota fue actualizada con éxito' }
          format.json { render :show, status: :ok, location: @flota }
        else
          format.html { render :edit }
          format.json { render json: @flota.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /flota/1
    # DELETE /flota/1.json
    def destroy
      @flota.destroy
      respond_to do |format|
        format.html { redirect_to flota_url, notice: 'Flota fue eliminada con éxito.' }
        format.json { head :no_content }
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_flota
        @flota = Flota.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def flota_params
        params[:flota_d]
      end
  end
end

