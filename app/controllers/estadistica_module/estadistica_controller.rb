module EstadisticaModule

  class EstadisticaController < ApplicationController
    before_filter :authenticate_session_user!
    def modalidad_estado_all
      @total = []
      @estadistica, @total[0], @total[1] = Estadistica.modalidad_estado
    end

    def modalidad_estado
      @total = []
      if params[:estado_id][:id].nil? || params[:estado_id][:id].blank?
        @error = true
        render :selector_ano_modalidad
      else
        @total[3] = Estado.find_by(id: params[:estado_id][:id]).nombre
        @estadistica, @total[0], @total[1] = Estadistica.modalidad_estado(params[:estado_id][:id])
      end
    end

    def selector_ano_modalidad #No quitar este metodo
    end

    def selector_parroquias #No quitar este metodo
    end

    def modalidad_parroquias
      @total = []
      if params[:municipio_id][:id].nil? || params[:municipio_id][:id].blank? ||  params[:municipio_id][:id] == "null"
        @error = true
        render :selector_parroquias
      else
        @total[3] = Municipio.find_by(id: params[:municipio_id][:id]).nombre
        @total[2] = ParroquiaV.where(municipio_id: params[:municipio_id][:id]).pluck(:nombre)
        @estadistica, @total[0], @total[1] = Estadistica.modalidad_estado(params[:estado_id][:id])
      end
    end

    def selector_ciudad #No quitar este metodo
    end

    def modalidad_ciudad
      @total = []
      if params[:estado_id][:id].nil? || params[:estado_id][:id].blank?
        @error = true
        render :selector_ciudad
      else
        @total[3] = Estado.find_by(id: params[:estado_id][:id]).nombre
        @total[2] = Ciudad.where(estado_id: params[:estado_id][:id]).pluck(:nombre)
        @estadistica, @total[0], @total[1] = Estadistica.modalidad_estado(params[:estado_id][:id])
      end
    end

    def modalidad_cupo_autorizado
      @total = []
      @estadistica, @total[0], @total[1] = Estadistica.modalidad_cupo_autorizado
    end

    def selector_ano_vencimiento_flota
    end

    def selector_ano_vencimiento_cps
    end

    def vencimiento_flota
      @total = []
      fecha = params[:date][:year].nil? || params[:date][:year] == "" ? Date.today.year : params[:date][:year]
      @estadistica, @total[0], @total[1], @total[2] = Estadistica.vencimiento_flota(fecha)
      @total[3] = fecha
    end

    def vencimiento_cps
      @total = []
      fecha = params[:date][:year].nil? || params[:date][:year] == "" ? Date.today.year : params[:date][:year]
      @estadistica, @total[0], @total[1], @total[2] = Estadistica.vencimiento_cps(fecha)
      @total[3] = fecha
    end


    def selector_proximas_vencer_flota
    end

    def proximas_vencer_flota
      @total = []
      @estadistica, @total[0], @total[1], @total[2] = Estadistica.proximas_vencer_flota(params[:meses].to_i)

    end
    def vistas_estados
      @estados, @modalidad, @estadisticas, @totales = Estadistica.calc_vistas_estados
    end

    def modalidad_tipo_unidad
      @total = []
      @total[1] = Estado.find_by(id: params[:estado_id][:id]).nombre
      if params[:estado_id][:id].nil? || params[:estado_id][:id].blank?
        @error = true
        render :selector_estados_tipo_unidad
      else
        @tipo, @modalidad, @estadistica = Estadistica.calc_vistas_tipo_unidad(params[:estado_id][:id])

      end
    end

    def vistas_parroquias
       if params[:municipio_id][:id].nil? || params[:municipio_id][:id].blank? ||  params[:municipio_id][:id] == "null"
        @error = true
        render :selector_parroquias
      else
        @municipio = Municipio.find_by(id: params[:municipio_id][:id]).nombre
        @parroquias, @modalidad, @totales, @estadistica = Estadistica.calc_vistas_mod_parroquias(params[:municipio_id][:id])
      end
    end

    def vistas_ciudad
      if params[:estado_id][:id].nil? || params[:estado_id][:id].blank?
        @error = true
        render :selector_ciudad
      else
        @estado = Estado.find_by(id: params[:estado_id][:id]).nombre
        @ciudad, @modalidad, @totales, @estadistica = Estadistica.vistas_mod_ciudad(params[:estado_id][:id])
      end
    end

    def selector_estados_tipo_unidad
    end

    def selector_meses_historicos
    end
    def nro_tramites_realizados_historicos
      @total = []
      mes = params[:date][:month].nil? || params[:date][:month] == "" ? Date.today.month : params[:date][:month]
      @total[1]= mes
      @certificacion, @estatus, @estadistica = Estadistica.calc_tramites_historicos(mes)
    end

    def form_igual_parada

    end

    def estadistica_igual_parada
      @parametros = Hash.new
      @parametros[:estado]      = params[:value][:id]
      @parametros[:tipo_parada] = ActiveRecord::Base.connection.quote(params[:value][:tipo_parada])
      @parametros[:parada]      = params[:parada_igual]
      consulta = EstadisticasSql.new
      connection = ActiveRecord::Base.connection
      @registros = connection.exec_query(consulta.igual_parada(@parametros[:estado],
                                                               @parametros[:tipo_parada],
                                                               @parametros[:parada]))
    end
  end
end