# -*- encoding : utf-8 -*-
class StaticPagesController < ApplicationController
  # before_action :deshabilitar_wizard, only: [:home]
  helper_method :resource, :resource_name, :devise_mapping
  def home
    if session_user_signed_in?
      @operadoras = current_session_user.usuario.operadora_transporte_ps
      current_session_user.usuario.operadora_transporte_ps.each do |op|
        op.servicio_operadora_cs.each do |s|
          if !s.completado
            return @incompleto = false
          end
        end
      end
      @incompleto = true
    end
  end

  def later_register
    @usuario_sput = UsuarioSput.find_by(id: params[:id])
  end

 end

