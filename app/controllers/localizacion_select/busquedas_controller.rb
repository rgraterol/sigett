module LocalizacionSelect
  class BusquedasController < ApplicationController
    respond_to :json

    def estado_municipios
      estado = ActionController::Parameters.new(estado: params[:filtro_id]).permit(:estado)[:estado]
      if estado.empty?
        @municipios = Array.new
        @municipios <<{
            id: '',
            nombre: 'No hay municipios'
        }
      else
        @municipios = Municipio.where(estado_id: estado).select('id','nombre')
        puts @municipios
      end
      render json: @municipios
    end

    def estado_ciudades
      estado = ActionController::Parameters.new(estado: params[:filtro_id]).permit(:estado)[:estado]
      if estado.empty?
        @municipios = Array.new
        @municipios <<{
            id: '',
            nombre: 'No hay Municipios'
        }
      else
        @municipios = Ciudad.where(estado_id: estado).select('id','nombre')
      end
      render json: @municipios
    end

    def municipio_parroquias
      municipio = ActionController::Parameters.new(municipio: params[:filtro_id]).permit(:municipio)[:municipio]
      if municipio.empty?
        @parroquias = Array.new
        @parroquias <<{
            id: '',
            nombre: 'No hay Parroquias'
        }
      else
        @parroquias = ParroquiaV.where(municipio_id: municipio).select('id','nombre')
      end
      render json: @parroquias
    end

    def parroquia_paradas
      parroquia = ActionController::Parameters.new(parroquia: params[:filtro_id]).permit(:parroquia)[:parroquia]
      if parroquia.empty?
        @paradas = Array.new
        @paradas <<{
            id: '',
            nombre: 'No hay Paradas'
        }
      else
        @paradas = Parada.where(parroquia_v_id: parroquia).select('id','nombre')
      end
      render json: @paradas
    end

    def estado_tipo_paradas
      estado = params[:estado_id]
      tipo_parada = Parada.tipo_paradas[params[:tipo_parada]]

      json_ = []
      json_ << {id: nil, label: "Seleccione una Parada"}

      para =
          Parada.find_by_sql ['SELECT
                            paradas.nombre,
                            paradas.id
                          FROM
                            public.paradas,
                            public.municipios,
                            public.parroquia_vs
                          WHERE
                            municipios.id = parroquia_vs.municipio_id AND
                            parroquia_vs.id = paradas.parroquia_v_id AND
                            municipios.estado_id = ? AND
                            paradas.tipo_parada = ?;',estado,tipo_parada]
      para.each do |a|
        json_ << {id: a.id, label: a.nombre}
      end
      render json: json_
    end
  end
end