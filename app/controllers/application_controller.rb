# -*- encoding : utf-8 -*-
class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  add_breadcrumb 'Inicio', :root_path, title: 'Inicio'
  rescue_from ActionController::RoutingError, :with => :render_404
  # protect_from_forgery with: :exception
  include ApplicationHelper
  include SessionUsersHelper

  def routing
    render_404
  end

  #asignacion de las id por ruta
  def asignar_cookie(nombre = {}, val = nil)
    cookies[nombre.to_s.to_sym] = val
  end
  def obtener_cookie(nombre)
    cookies[nombre.to_s.to_sym]
  end
  #rutas predefinidas por controlador
  WIZAR       = 'W_EST'
  OPERADORAID = 'op_id'
  SERVICIOID  = 'ser_id'
  RUTAID      = 'ruta_id'
  SEGURO      = 'seg_val'
  FLOTA       = 'flo_val'
  RUTA        = 'ruta_val'
  TOQUES      = 'toques_val'
  HORARIO     = 'hor_val'
  HISTORICO   = 'hist_val'



  private
    def armar_breadcrumb
      if params[:controller] == 'operadora/operadora_transporte_ps'
        add_breadcrumb 'Operadora'
      elsif params[:controller] == 'servicio/servicio_operadora_cs' && params[:action] == 'index_consulta' && params[:action] == 'index'
        add_breadcrumb 'Modalidades de Servicio'
      elsif params[:controller] == 'servicio/servicio_operadora_cs' && params[:action] == 'new'
        add_breadcrumb 'Operadoras', servicio_index_consulta_path, title: 'Operadoras de Transporte'
        add_breadcrumb 'Modalidad de Servicio'
      elsif params[:controller] == 'servicio/servicio_operadora_cs' && params[:action] == 'end_carga'
        servicio = ServicioOperadoraC.find_by(id: ActionController::Parameters.new(servicio: params[:servicio]).permit(:servicio)[:servicio])
        breadcumb_list(servicio, true, false)
      elsif params[:controller] == 'servicio/servicio_operadora_cs' && (params[:action] != 'new' && params[:action] != 'index_consulta' && params[:action] != 'index')
        servicio = ServicioOperadoraC.find_by(id: ActionController::Parameters.new(id: params[:id]).permit(:id)[:id])
        breadcumb_list(servicio, false, false)
      elsif params[:controller] == 'seguro/seguro_vehiculos'
        servicio = ServicioOperadoraC.find_by(id: ActionController::Parameters.new(id: params[:id]).permit(:id)[:id])
        breadcumb_list(servicio, true, false)
      elsif params[:controller] == 'vehiculos/vehiculos'
        servicio = Flota.find_by(id: ActionController::Parameters.new(flota_id: params[:flota_id]).permit(:flota_id)[:flota_id]).servicio_operadora_c
        breadcumb_list(servicio, true, false)
      elsif params[:controller] == 'ruta_flota/ruta' && params[:action] == 'new_taxi'
        servicio = Flota.find_by(id: ActionController::Parameters.new(id: params[:id]).permit(:id)[:id]).servicio_operadora_c
        breadcumb_list(servicio, true, false)
      elsif params[:controller] == 'ruta_flota/ruta' && params[:action] != 'edit' && params[:action] != 'index' && params[:action] != 'new'
        servicio = Ruta.find_by(id: ActionController::Parameters.new(id: params[:id]).permit(:id)[:id]).flota.servicio_operadora_c
        breadcumb_list(servicio, true, false)
      elsif params[:controller] == 'ruta_flota/ruta' && (params[:action] == 'index' || params[:action] == 'new')
        servicio = Flota.find_by(id: ActionController::Parameters.new(id: params[:id]).permit(:id)[:id]).servicio_operadora_c
        breadcumb_list(servicio, true, false)
      elsif (params[:controller] == 'ruta_flota/ruta' && params[:action] == 'edit') || params[:controller] == 'paradas/toques_dinamico'
        servicio = Ruta.find_by(id: ActionController::Parameters.new(id: params[:id]).permit(:id)[:id]).flota.servicio_operadora_c
        breadcumb_list(servicio, true, false)
      elsif params[:controller] == 'horario_d/horarios_dinamico'
        servicio = Ruta.find_by(id: ActionController::Parameters.new(id: params[:id]).permit(:id)[:id]).flota.servicio_operadora_c
        breadcumb_list(servicio, true, true)
      elsif params[:controller] == 'historico/historico_solicitudes_aprobs' && (params[:action] == 'index' || params[:action] == 'edit' || params[:action] == 'create')
        if params[:id_from] == 'ruta'
          servicio = Ruta.find_by(id: ActionController::Parameters.new(id: params[:id]).permit(:id)[:id]).flota.servicio_operadora_c
        else
          servicio = Flota.find_by(id: ActionController::Parameters.new(id: params[:id]).permit(:id)[:id]).servicio_operadora_c
        end
        breadcumb_list(servicio, true, false)
      elsif params[:action] == 'consulta' &&  params[:controller] == 'historico/historico_solicitudes_aprobs'
        servicio = ServicioOperadoraC.find_by(id: params[:servicio])
        breadcumb_list(servicio, true, false)
      end
      # puts "HOLA"
      # puts params[:controller]
    end

    def breadcumb_list(servicio, vehiculos_tokem, horarios_tokem)
      add_breadcrumb 'Operadoras', servicio_index_consulta_path, title: 'Operadoras de Transporte'
      add_breadcrumb 'Modalidad de Servicio', servicio_edit_path(id: servicio.id)
      if servicio.operadora_transporte_p.usuario.session_user == current_session_user
        unless servicio.flota.seguro_vehiculo.nil? and not servicio.flota.vehiculos.empty?
          add_breadcrumb 'Seguro RCV', seguro_edit_path(id: servicio.flota.id)
        end
        if servicio.flota.vehiculos.present? || vehiculos_tokem || servicio.flota.rutas.any?
          add_breadcrumb 'Vehículos de Flota', flota_vehiculos_path(flota_id: servicio.flota)
        end
        if servicio.flota.rutas.any?
          add_breadcrumb 'Rutas', ruta_flota_index_path(id: servicio.flota.id)
        end
        if horarios_tokem
          add_breadcrumb 'Horarios de Ruta'
        end
        if servicio.historico_solicitudes_aprobs.any?
          if servicio.sub_urb_p_puesto? || servicio.int_urb_p_puesto? || servicio.sub_urb_colectivo? || servicio.int_urb_colectivo? || servicio.taxi? || servicio.moto_taxi? || servicio.periferico_i? || servicio.periferico_s?
            add_breadcrumb 'Historico Solicitudes', historico_index_path(id: servicio.flota.rutas.last.id, id_from: 'ruta', id_servicio: servicio) if servicio.flota.rutas.any?
          else
            add_breadcrumb 'Historico Solicitudes', historico_index_path(id: servicio.flota.id, id_from: 'flota', id_servicio: servicio)
          end
        end
        add_breadcrumb ''
      end
    end

    #metodos que se ejecutan cuando regreso en el wizar
    # def paso_operadora
    #   self.asignar_cookie(SERVICIOID,'0')
    #   self.asignar_cookie(SEGURO,'FALSE')
    #   self.asignar_cookie(FLOTA,'FALSE')
    #   self.asignar_cookie(RUTA,'FALSE')
    #   self.asignar_cookie(TOQUES,'FALSE')
    #   self.asignar_cookie(HISTORICO,'FALSE')
    # end
    #
    # def paso_servicio
    #   self.asignar_cookie(SEGURO,'FALSE')
    #   self.asignar_cookie(FLOTA,'FALSE')
    #   self.asignar_cookie(RUTA,'FALSE')
    #   self.asignar_cookie(TOQUES,'FALSE')
    #   self.asignar_cookie(HORARIO,'FALSE')
    #   self.asignar_cookie(HISTORICO,'FALSE')
    # end
    #
    # def paso_seguro
    #   self.asignar_cookie(FLOTA,'FALSE')
    #   self.asignar_cookie(RUTA,'FALSE')
    #   self.asignar_cookie(TOQUES,'FALSE')
    #   self.asignar_cookie(HORARIO,'FALSE')
    #   self.asignar_cookie(HISTORICO,'FALSE')
    # end
    #
    # def paso_flota
    #   self.asignar_cookie(RUTA,'FALSE')
    #   self.asignar_cookie(TOQUES,'FALSE')
    #   self.asignar_cookie(HORARIO,'FALSE')
    #   self.asignar_cookie(HISTORICO,'FALSE')
    # end
    #
    # def paso_ruta
    #   self.asignar_cookie(TOQUES,'FALSE')
    #   self.asignar_cookie(HORARIO,'FALSE')
    #   self.asignar_cookie(HISTORICO,'FALSE')
    # end
    #
    # def paso_toques
    #   self.asignar_cookie(HORARIO,'FALSE')
    #   self.asignar_cookie(HISTORICO,'FALSE')
    # end
    #
    # def paso_horario
    #   self.asignar_cookie(HISTORICO,'FALSE')
    # end
    #
    #
    # #deshabilitar wizard
    # def deshabilitar_wizard
    #   self.asignar_cookie(ApplicationController::WIZAR,'FALSE')
    #   self.asignar_cookie(ApplicationController::OPERADORAID,'0')
    #   self.asignar_cookie(ApplicationController::SERVICIOID,'0')
    #   self.asignar_cookie(ApplicationController::RUTAID,'0')
    #   self.asignar_cookie(ApplicationController::SEGURO,'FALSE')
    #   self.asignar_cookie(ApplicationController::FLOTA,'FALSE')
    #   self.asignar_cookie(ApplicationController::RUTA,'FALSE')
    #   self.asignar_cookie(ApplicationController::TOQUES,'FALSE')
    #   self.asignar_cookie(ApplicationController::HISTORICO,'FALSE')
    # end
    #
    # #asignar la cookie antes de actualizar
    # def asignar_cookies
    #   # id =
    #   # @operadora_transporte_p = current_session_user.operadoras.where(id: id).last
    #   # self.asignar_cookie(ApplicationController::WIZAR,'TRUE')
    #   # self.asignar_cookie(ApplicationController::OPERADORAID,@operadora_transporte_p.id)
    # end
    def render_404(exception = nil)
      if exception
        logger.info "Rendering 404: #{exception.message}"
      end

      render :file => "#{Rails.root}/public/404.html", :status => 404, :layout => false
    end

end

