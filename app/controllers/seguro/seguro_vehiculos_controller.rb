# -*- encoding : utf-8 -*-
module Seguro
  class SeguroVehiculosController < ApplicationController
    before_action :armar_breadcrumb, except: [:update, :create, :destroy]
    before_action :set_lista_aseguradora
    before_action :set_flota, only: [:new, :edit]
    before_action :set_flota_db, only: [:create, :update]
    before_action :verificar_usuario
    before_filter :authenticate_session_user!
    before_action :autorized_user
    # GET /seguro_vehiculos
    # GET /seguro_vehiculos.json
    def index
      @seguro_vehiculos = SeguroVehiculo.all
    end

    # GET /seguro_vehiculos/1
    # GET /seguro_vehiculos/1.json
    def show
    end

    # GET /seguro_vehiculos/new
    def new
      @seguro_vehiculo = @flota.build_seguro_vehiculo
      @rif_operadora = @flota.servicio_operadora_c.operadora_transporte_p.rif
      @colectivo = false
      @servicio_operadora_c = @flota.servicio_operadora_c
      self.asignar_cookie(ApplicationController::SEGURO,'TRUE')
    end

    # GET /seguro_vehiculos/1/edit
    def edit

      @colectivo = @flota.seguro_vehiculo.nil? ? false : true
      @seguro_vehiculo = @flota.seguro_vehiculo.nil? ? @flota.build_seguro_vehiculo : @flota.seguro_vehiculo
      @rif_operadora = @flota.servicio_operadora_c.operadora_transporte_p.rif
      @servicio_operadora_c = @flota.servicio_operadora_c
    end


    # POST /seguro_vehiculos
    # POST /seguro_vehiculos.json
    def create
      @seguro_vehiculo = @flota.build_seguro_vehiculo(seguro_vehiculo_params)
      @rif_operadora = @flota.servicio_operadora_c.operadora_transporte_p.rif
      respond_to do |format|
        if @seguro_vehiculo.save
          self.asignar_cookie(ApplicationController::FLOTA,'TRUE')
          format.html { redirect_to flota_vehiculos_url(@flota),
                                    notice: 'Seguro vehículos se ha creado con éxito.' }
          format.json { render :show, status: :created, location: @seguro_vehiculo }
        else
          format.html { render :new }
          format.json { render json: @seguro_vehiculo.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /seguro_vehiculos/1
    # PATCH/PUT /seguro_vehiculos/1.json
    def update
      respond_to do |format|
        @seguro_vehiculo = @flota.seguro_vehiculo
        @rif_operadora = @flota.servicio_operadora_c.operadora_transporte_p.rif

        if @seguro_vehiculo.update(seguro_vehiculo_params)
          self.asignar_cookie(ApplicationController::FLOTA,'TRUE')
          format.html { redirect_to flota_vehiculos_url(@flota)}
          format.json { render :show, status: :ok, location: @seguro_vehiculo }
        else
          format.html { render :edit }
          format.json { render json: @seguro_vehiculo.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /seguro_vehiculos/1
    # DELETE /seguro_vehiculos/1.json
    def destroy
      @seguro_vehiculo.destroy
      respond_to do |format|
        format.html { redirect_to seguro_vehiculos_url, notice: 'Seguro vehículos fue eliminado con éxito.' }
        format.json { head :no_content }
      end
    end

    private

    def set_flota_db
      @flota = Flota.find_by(id: params[:flota_id])
    end

    def set_flota
      @flota = Flota.find_by(id: params[:id])
    end

    def verificar_usuario
      @session_user = @flota.servicio_operadora_c.operadora_transporte_p.usuario.session_user
      unless @session_user == current_session_user
        flash[:danger] =
            "El Id de flota con que se está intentando accesar no pertenece al representante."
        redirect_to root_path
      end
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_lista_aseguradora
      list_all= Aseguradora.all
      @lista_aseguradora = Hash.new

      list_all.each do |aseguradora|
        @lista_aseguradora[aseguradora.nombre] = aseguradora.id
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def seguro_vehiculo_params
      params.require(:seguro_vehiculo).permit(:rif, :num_poliza,
                                              :fecha_vencimiento,
                                              :aseguradora_id, :flota_id,
                                              :documento_poliza)
    end
  end
end

