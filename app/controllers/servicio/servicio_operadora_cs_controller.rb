# -*- encoding : utf-8 -*-
module Servicio
  class ServicioOperadoraCsController < ApplicationController
    before_action :set_servicio_operadora_c, only: [:edit, :destroy, :update]
    before_action :armar_breadcrumb, except: [:create, :set_codigo_rotp, :update, :destroy, :decide_n_e, :end_carga ]
    before_filter :authenticate_session_user!
    before_action :detectar_fecha_renovacion, only: [:create, :update]
    before_action :autorized_user

    # GET /servicio_operadora_cs
    # GET /servicio_operadora_cs.json
    def index_consulta
      @operadoras = current_session_user.operadoras
    end

    def show_consulta
      @vehiculos = Array.new
      id = ActionController::Parameters.new(id: params[:id]).permit(:id)[:id]
      servicio_operadora_c = ServicioOperadoraC.find_by(id: id)
      @operadora_transporte_p = servicio_operadora_c.operadora_transporte_p
      if servicio_operadora_c.operadora_transporte_p.usuario.session_user == current_session_user
        @servicio_operadora_c = servicio_operadora_c
        servicio_operadora_c.flota.vehiculos.find_each.with_index do |vehiculo, index|
          @vehiculos << Vehiculo.find_by(placa: vehiculo.placa.to_s.upcase)
        end
      else
        redirect_to root_path, notice: "Está intentando accesar a una sección no autorizada para este usuario"
      end
    end

    def index
      @servicio_operadora_cs =
          current_session_user.operadoras.where(id: params[:id]).last.servicio_operadora_cs

      if @servicio_operadora_cs.empty?
        redirect_to servicio_new_path(id: params[:id]),
                    notice: "No posee servicios asociados a la operadora. Agregue un servicio"
      end
    end

    def decide_n_e

    end
    # GET /servicio_operadora_cs/1
    # GET /servicio_operadora_cs/1.json
    def show
      @vehiculos = Array.new
      id = ActionController::Parameters.new(id: params[:id]).permit(:id)[:id]
      servicio_operadora_c = ServicioOperadoraC.find_by(id: id)
      @operadora_transporte_p = servicio_operadora_c.operadora_transporte_p
      if servicio_operadora_c.operadora_transporte_p.usuario.session_user == current_session_user
        @servicio_operadora_c = servicio_operadora_c
        servicio_operadora_c.flota.vehiculos.find_each.with_index do |vehiculo, index|
          puts 'paso por show'
          @vehiculos << Vehiculo.find_by(placa: vehiculo.placa.to_s.upcase)
        end
      else
        redirect_to root_path, notice: "Está intentando accesar a una sección no autorizada para este usuario"
      end
    end

    # GET /servicio_operadora_cs/new
    def new
      @servicio_operadora_c = current_session_user.operadoras.where(id: params[:id]).last.servicio_operadora_cs.build
      @servicio_operadora_c.build_flota
      @edit = false
    end

    # GET /servicio_operadora_cs/1/edit
    def edit
      @edit = true
    end


    # POST /servicio_operadora_cs
    # POST /servicio_operadora_cs.json
    def create
      @servicio_operadora_c = current_session_user.operadoras.where(id: servicio_operadora_c_params[:operadora_transporte_p_id]).last.servicio_operadora_cs.build(servicio_operadora_c_params)
      # if @servicio_operadora_c.modalidad_servicio.exi
      #
      # end
      
      respond_to do |format|
        if @servicio_operadora_c.save
          # self.asignar_cookie(ApplicationController::SERVICIOID,@servicio_operadora_c.id)
          self.asignar_cookie(ApplicationController::SERVICIOID,@servicio_operadora_c.id)
          self.asignar_cookie(ApplicationController::SEGURO,'TRUE')
          #@servicio_operadora_c.id
          format.html { redirect_to seguro_new_path(id: @servicio_operadora_c.flota.id), notice: 'El servicio fue guardado con éxito.' }
          format.json { render :show, status: :created, location: @servicio_operadora_c }
        else
          @edit = false
          format.html { render :new }
          format.json { render json: @servicio_operadora_c.errors, status: :unprocessable_entity }
        end
      end
    end



    # PATCH/PUT /servicio_operadora_cs/1
    # PATCH/PUT /servicio_operadora_cs/1.json
    def update
      respond_to do |format|
        if @servicio_operadora_c.update(servicio_operadora_c_params)
          # self.asignar_cookie(ApplicationController::SERVICIOID,@servicio_operadora_c.id)
          self.asignar_cookie(ApplicationController::SERVICIOID,@servicio_operadora_c.id)
          self.asignar_cookie(ApplicationController::SEGURO,'TRUE')
          format.html { salto_wizard }
          format.json { render :show, status: :ok, location: @servicio_operadora_c }
        else
          @edit = true
          format.html { render :edit }
          format.json { render json: @servicio_operadora_c.errors, status: :unprocessable_entity }
        end
      end
    end

    def end_carga
      servicio = ServicioOperadoraC.find_by(id: params[:servicio])
      servicio.update_attribute(:completado,true)

      r1 = params[:flota]
      r2 = params[:ruta]

      if r1 == 's' and r2 == 'n'
        servicio.update_attribute(:estado,5)
      elsif r1 == 'n' and r2 == 's'
        servicio.update_attribute(:estado,6)
      elsif r1 == 's' and r2 == 's'
        servicio.update_attribute(:estado,2)
      elsif r1 == 'n' and r2 == 'n'
        servicio.update_attribute(:estado,7)
      end

      redirect_to root_path, notice: "Se ha completado la carga de datos para este servicio"
    end
    # DELETE /servicio_operadora_cs/1
    # DELETE /servicio_operadora_cs/1.json
    def destroy
      @servicio_operadora_c.destroy
      respond_to do |format|
        format.html { redirect_to servicio_operadora_cs_url, notice: 'Servicio operadora fue eliminado con éxito.' }
        format.json { head :no_content }
      end
    end

    def set_codigo_rotp
      rotp = ModalidadServicio.find_by(id: params[:modalidad_id]).rotp
      codigo = []
      codigo << (rotp + params[:cd])
      render json: codigo
    end

    private
    def salto_wizard
      @flota = @servicio_operadora_c.flota

      if @flota.seguro_vehiculo.nil? and not @flota.vehiculos.empty?
        #tiene seguro particular salto a vehiculos
        redirect_to flota_vehiculos_url(@flota),
                    notice: 'El servicio fue guardado.'
      else
        #Aun no ha escogido seguro lo llevamos para que el usuario escoja
        #Ó Escogio seguro colectivo puedo pasar a editarlo
        redirect_to seguro_edit_path(id: @flota.id),
                    notice: 'El servicio fue guardado.'
      end
    end

    def detectar_fecha_renovacion_u
      @servicio_operadora_c = ServicioOperadoraC.find_by(id: params[:id])
      if params[:fecha_ultima_box] == "true"
      else
        params[:servicio_operadora_c][:flota_attributes].delete("fecha_ultima_renovacion(3i)")
        params[:servicio_operadora_c][:flota_attributes].delete("fecha_ultima_renovacion(2i)")
        params[:servicio_operadora_c][:flota_attributes].delete("fecha_ultima_renovacion(1i)")
        params[:servicio_operadora_c][:flota_attributes][:fecha_ultima_renovacion] = ""
      end
    end

    def detectar_fecha_renovacion
      if params[:fecha_ultima_box] == "true"
      else
        params[:servicio_operadora_c][:flota_attributes].delete("fecha_ultima_renovacion(3i)")
        params[:servicio_operadora_c][:flota_attributes].delete("fecha_ultima_renovacion(2i)")
        params[:servicio_operadora_c][:flota_attributes].delete("fecha_ultima_renovacion(1i)")
        params[:servicio_operadora_c][:flota_attributes][:fecha_ultima_renovacion] = ""
      end
    end
    # Use callbacks to share common setup or constraints between actions.
    def set_servicio_operadora_c
      @servicio_operadora_c = ServicioOperadoraC.find(params[:id])
      self.asignar_cookie(ApplicationController::SERVICIOID,@servicio_operadora_c.id)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def servicio_operadora_c_params
      params.require(:servicio_operadora_c).permit( :codigo_rotp,
                                                    :fecha_expedicion,
                                                    :fecha_vencimiento,
                                                    :modalidad_servicio_id,
                                                    :dt,
                                                    :cps,
                                                    :operadora_transporte_p_id,:fecha_ultima_box, flota_attributes: [:cupo_asignado,
                                                                                                                     :fecha_autorizacion, :id, :fecha_ultima_renovacion])
    end
  end
end

