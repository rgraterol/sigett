# -*- encoding : utf-8 -*-
module Historico
  class HistoricoSolicitudesAprobsController < ApplicationController
    before_action :set_historico_solicitudes_aprob, only: [:edit, :update, :destroy]
    before_action :set_variables_historico_index, only: [:index, :edit]
    before_action :set_variables_historico_create, only: [:create]

    before_filter :authenticate_session_user!
    before_action :autorized_user
    before_filter :armar_breadcrumb, except: [:create, :update, :verificar_regreso, :destroy]
    # GET /historico_solicitudes_aprobs
    # GET /historico_solicitudes_aprobs.json
    def index
      @servicio_operadora_c = @historico_solicitudes_aprobs.first.servicio_operadora_c
    end

    # GET /historico_solicitudes_aprobs/1
    # GET /historico_solicitudes_aprobs/1.json
    def show
      servicio = ServicioOperadoraC.find_by(id: params[:id])
      if servicio.operadora_transporte_p.usuario.session_user == current_session_user
        @historico_solicitudes_aprobs = servicio.historico_solicitudes_aprobs
        # @ruta = ruta
      else
        redirect_to root_path, notice: "Acceso no permitido"
      end
    end

    # GET /historico_solicitudes_aprobs/new
    def new
      @historico_solicitudes_aprob = HistoricoSolicitudesAprob.new
    end

    # GET /historico_solicitudes_aprobs/1/edit
    def edit

      @servicio_operadora_c = @historico_solicitudes_aprob.servicio_operadora_c
      if @historico_solicitudes_aprob.servicio_operadora_c.operadora_transporte_p.usuario.session_user == current_session_user
        return
        # @historico_solicitudes_aprobs = ruta.flota.servicio_operadora_c.historico_solicitudes_aprobs
      else
        redirect_to root_path, notice: "Acceso no permitido"
      end

      # ruta = Ruta.find_by(id: params[:id])
      # if ruta.flota.servicio_operadora_c.operadora_transporte_p.usuario.session_user == current_session_user
      #   @historico_solicitudes_aprobs = ruta.flota.servicio_operadora_c.historico_solicitudes_aprobs
      # else
      #   redirect_to root_path, notice: "El Id de flota con que se está intentando accesar no pertenece al representante. Vuelva a intentar el proceso"
      # end
      # @servicio_operadora_c = @historico_solicitudes_aprobs.first.servicio_operadora_c
    end

    # POST /historico_solicitudes_aprobs
    # POST /historico_solicitudes_aprobs.json
    def create
      respond_to do |format|
        @servicio_operadora_c = @historico_solicitudes_aprob.servicio_operadora_c
        if @historico_solicitudes_aprob.save
          format.html { redirect_to historico_index_path(id: params[:id], id_from: params[:id_from], id_servicio: params[:id_servicio]), notice: 'Histórico Solicitudes Aprobadas fue creado con éxito.' }
          format.json { render :show, status: :created, location: @historico_solicitudes_aprob }
        else
          format.html { render 'historico/historico_solicitudes_aprobs/index', object: @historico_solicitudes_aprobs = HistoricoSolicitudesAprob.all, notice: 'Error' }
          format.json { render json: @historico_solicitudes_aprob.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /historico_solicitudes_aprobs/1
    # PATCH/PUT /historico_solicitudes_aprobs/1.json
    def update
      @servicio_operadora_c = @historico_solicitudes_aprob.servicio_operadora_c
      respond_to do |format|
        if @historico_solicitudes_aprob.update(historico_solicitudes_aprob_params)
          format.html { redirect_to historico_index_path(id: params[:id], id_from: params[:id_from], id_servicio: params[:id_servicio]), notice: 'Historico Solicitudes Aprobadas fue actualizado con éxito.' }
          format.json { render :show, status: :ok, location: @historico_solicitudes_aprob }
        else

          format.html { render 'historico/historico_solicitudes_aprobs/edit', object: @historico_solicitudes_aprobs = HistoricoSolicitudesAprob.all }
          format.json { render json: @historico_solicitudes_aprob.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /historico_solicitudes_aprobs/1
    # DELETE /historico_solicitudes_aprobs/1.json
    def destroy
      @historico_solicitudes_aprob.destroy
      respond_to do |format|
        format.html { redirect_to historico_index_path(id: params[:id], id_from: params[:id_from], id_servicio: params[:id_servicio]), notice: 'Historico Solicitudes Aprobadas fue eliminado con éxito.' }
        format.json { head :no_content }
      end
    end

    def consulta
      @servicio = ServicioOperadoraC.find_by(id: params[:servicio])
      @servicio_operadora_c = @servicio
    end

    def verificar_regreso
      id = ActionController::Parameters.new(id: params[:id])
               .permit(:id)[:id]

      case params[:id_from]
        when 'ruta'
          ruta = Ruta.find_by(id: id)
          s = ruta.flota.servicio_operadora_c
          if s.sub_urb_p_puesto? || s.int_urb_p_puesto? || s.sub_urb_colectivo? || s.int_urb_colectivo?
            render js: "window.location.replace('#{horario_d_index_path(id: ruta)}');"
          elsif s.taxi? || s.moto_taxi?
            render js: "window.location.replace('#{ruta_flota_edit_taxi_path(id: ruta)}');"
          elsif  s.periferico_i? || s.periferico_s?
            render js: "window.location.replace('#{paradas_index_path(id: ruta)}');"
          end
        when 'flota'
          flota = Flota.find_by(id: id)
          render js: "window.location.replace('#{flota_vehiculos_url(flota)}');"
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_variables_historico_index
        @servicio = params[:id_servicio]
        case params[:id_from]
          when 'ruta'
            ruta = Ruta.find_by(id: params[:id])
            if ruta.flota.servicio_operadora_c.operadora_transporte_p.usuario.session_user == current_session_user
              @historico_solicitudes_aprobs = ruta.flota.servicio_operadora_c.historico_solicitudes_aprobs
              @historico_solicitudes_aprob ||= ruta.flota.servicio_operadora_c.historico_solicitudes_aprobs.build
              @historico_solicitudes_aprob.servicio_operadora_c_id = ruta.flota.servicio_operadora_c.id
            else
              redirect_to root_path, notice: "Acceso no permitido"
            end
          when 'flota'
            flota = Flota.find_by(id: params[:id])
            if flota.servicio_operadora_c.operadora_transporte_p.usuario.session_user == current_session_user
              @historico_solicitudes_aprobs = flota.servicio_operadora_c.historico_solicitudes_aprobs
              @historico_solicitudes_aprob ||= flota.servicio_operadora_c.historico_solicitudes_aprobs.build
              @historico_solicitudes_aprob.servicio_operadora_c_id = flota.servicio_operadora_c.id
            else
              redirect_to root_path, notice: "Acceso no permitido"
            end
        end
      end

      def set_variables_historico_create
        @servicio = ServicioOperadoraC.find(params[:id_servicio])
        case params[:id_from]
          when 'ruta'
            ruta = Ruta.find_by(id: params[:id])
            if ruta.flota.servicio_operadora_c.operadora_transporte_p.usuario.session_user == current_session_user
              @historico_solicitudes_aprobs = ruta.flota.servicio_operadora_c.historico_solicitudes_aprobs
              @historico_solicitudes_aprob ||= ruta.flota.servicio_operadora_c.historico_solicitudes_aprobs.build(historico_solicitudes_aprob_params)
              @historico_solicitudes_aprob.servicio_operadora_c_id = @servicio.id
            else
              redirect_to root_path, notice: "Acceso no permitido"
            end
          when 'flota'
            flota = Flota.find_by(id: params[:id])
            if flota.servicio_operadora_c.operadora_transporte_p.usuario.session_user == current_session_user
              @historico_solicitudes_aprobs = flota.servicio_operadora_c.historico_solicitudes_aprobs
              @historico_solicitudes_aprob ||= flota.servicio_operadora_c.historico_solicitudes_aprobs.build(historico_solicitudes_aprob_params)
              @historico_solicitudes_aprob.servicio_operadora_c_id = @servicio.id
            else
              redirect_to root_path, notice: "Acceso no permitido"
            end
        end
      end

      def set_historico_solicitudes_aprob
        @historico_solicitudes_aprob = HistoricoSolicitudesAprob.find(params[:id_hist])
      end

      def historico_solicitudes_aprob_params
        params.require(:historico_solicitudes_aprob).permit(:tipo_certificacion,
                                                       :descripcion, :fecha_expedicion, :documento_historico)
      end
      def historico_params
        historico_param = ActionController::Parameters.new(tipo_certificacion: params[:tipo_certificacion],
                                                           descripcion: params[:descripcion],
                                                           fecha_expedicion: params[:fecha_expedicion],
                                                           documento_historico: params[:documento_historico],
        )
        historico_param.permit(:id, :tipo_certificacion,
                               :descripcion, :fecha_expedicion,:documento_historico,:id_retornable )
      end
      def weak_params
        params[:historico_solicitudes_aprob]
      end

  end
end

