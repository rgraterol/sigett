# -*- encoding : utf-8 -*-
module Operadora
  class OperadoraTransportePsController < ApplicationController
    #habilitacion y deshabilitacion del breadcrumb
    before_filter :armar_breadcrumb, except: [:create, :update, :ciudad_municipio, :destroy]

    before_action :set_operadora_transporte_p, only: [:show, :edit, :update, :destroy]
    before_filter :authenticate_session_user!
    before_action :autorized_user
    before_action :generar_lista_operadoras_movil, only: [:create,:new,:edit,:update]

    # GET /operadora_transporte_ps
    # GET /operadora_transporte_ps.json
    def index
      @operadora_transporte_ps = current_session_user.usuario.operadora_transporte_ps.page(params[:page]).per(25)
      if @operadora_transporte_ps.empty?
        redirect_to root_path, notice: "No posee Operadoras registradas. Registre una nueva Operadora"
      end
    end

    def consultar
      @operadora_transporte_ps = current_session_user.usuario.operadora_transporte_ps
    end

    # GET /operadora_transporte_ps/1
    # GET /operadora_transporte_ps/1.json
    def show
      if current_session_user.usuario.operadora_transporte_ps.empty?
        redirect_to root_path, notice: "No posee Operadoras registradas. Registre una nueva Operadora"
      end
    end

    def ciudad_municipio
      estado = ActionController::Parameters.new(estado: params[:estado_id]).permit(:estado)[:estado]
      municipio_a = []
      ciudad_b = []
      json_ = []
      municipio_a << {
          id: nil,
          label: "Seleccione un Municipio",
      }
      ciudad_b << {
          id: nil,
          label: "Seleccione una Ciudad",
      }
      m = Municipio.where(estado_id: estado)
      c = Ciudad.where(estado_id: estado)
      m.each do |i|
        municipio_a <<{
            id: i.id,
            label: i.nombre,
        }
      end
      c.each do |i|
        ciudad_b <<{
            id: i.id,
            label: i.nombre,
        }
      end
      json_ << municipio_a << ciudad_b
      render json: json_
    end

    # GET /operadora_transporte_ps/new
    def new
      self.asignar_cookie(ApplicationController::WIZAR,'TRUE')
      @operadora_transporte_p = OperadoraTransporteP.new
      @edit = false
      @operadora_movil_value = 416
    end

    # GET /operadora_transporte_ps/1/edit
    def edit
      self.asignar_cookie(ApplicationController::SERVICIOID,'0')
      @edit = true
      @operadora_movil_value = @operadora_transporte_p.movil[0..2]
      @operadora_transporte_p.movil=@operadora_transporte_p.movil[3..9]
    end


    # POST /operadora_transporte_ps
    # POST /operadora_transporte_ps.json
    def create
      @operadora_transporte_p = current_session_user.operadoras.build(operadora_transporte_p_params)
      original_movil = params[:operadora_transporte_p][:movil]

      unless original_movil.blank?
        @operadora_transporte_p.movil=
            params[:operadora_movil] + original_movil
      end


      respond_to do |format|
        if @operadora_transporte_p.save
          self.asignar_cookie(ApplicationController::OPERADORAID,@operadora_transporte_p.id)
          format.html { redirect_to servicio_index_path(id: @operadora_transporte_p.id), notice: 'La operadora fue creada con éxito.' }
          format.json { render :show, status: :created, location: @operadora_transporte_p }
        else
          @edit = false
          @operadora_movil_value = params[:operadora_movil]
          @operadora_transporte_p.movil = original_movil
          @operadora_transporte_p.rif_imagen = nil
          format.html { render :new }
          format.json { render json: @operadora_transporte_p.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /operadora_transporte_ps/1
    # PATCH/PUT /operadora_transporte_ps/1.json
    def update
      original_movil = params[:operadora_transporte_p][:movil]

      unless original_movil.blank?
        params[:operadora_transporte_p][:movil]=
            params[:operadora_movil] + original_movil
      end
      respond_to do |format|
        if @operadora_transporte_p.update(operadora_transporte_p_params)
          self.asignar_cookie(ApplicationController::OPERADORAID,@operadora_transporte_p.id)
          format.html { redirect_to servicio_index_path(id: @operadora_transporte_p.id), notice: 'La operadora fue actualizada con éxito.' }
          format.json { render :show, status: :ok, location: @operadora_transporte_p }
        else
          @edit = true
          @operadora_movil_value = params[:operadora_movil]
          @operadora_transporte_p.movil = original_movil
          format.html { render :edit }
          format.json { render json: @operadora_transporte_p.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /operadora_transporte_ps/1
    # DELETE /operadora_transporte_ps/1.json
    def destroy
      @operadora_transporte_p.destroy
      respond_to do |format|
        format.html { redirect_to operadora_index_path, notice: 'Operadora transporte fue eliminada con éxito.' }
        format.json { head :no_content }
      end
    end

    private


    #lista de operadoras de moviles
    def generar_lista_operadoras_movil
      @lista_operadoras = {
          '0416' =>'416',
          '0426' =>'426',
          '0412' =>'412',
          '0414' =>'414',
          '0424' =>'424'
      }
    end
    # Use callbacks to share common setup or constraints between actions.
    def set_operadora_transporte_p
      id = ActionController::Parameters.new(id: params[:id]).permit(:id)[:id]
      @operadora_transporte_p = current_session_user.operadoras.where(id: id).last
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def operadora_transporte_p_params
      params.require(:operadora_transporte_p).permit(
                                                    :razon_social,
                                                    :rif,
                                                    :email,
                                                    :telefono,
                                                    :movil,
                                                    :fax,
                                                    :observacion,
                                                    :avenida,
                                                    :calle,
                                                    :urbanizacion,
                                                    :edificio_cc,
                                                    :local,
                                                    :piso,
                                                    :parroquia_v_id,
                                                    :sociedad_mercantil_otros,
                                                    :sociedad_mercantil_id,
                                                    :rif_imagen,
                                                    :ciudad_id,
                                                    :documento_acreditacion,
                                                    :acta_socios)
    end
    def operadora_params
      operadora_param = ActionController::Parameters.new(
          razon_social: params[:razon_social],
          rif: params[:rif],
          email: params[:telefono],
          movil: params[:movil],
          fax: params[:fax],
          url: params[:url],
          observacion: params[:observacion],
          avenida: params[:avenida],
          calle: params[:calle],
          urbanizacion: params[:urbanizacion],
          edificio_cc: params[:edificio_cc],
          local: params[:local],
          piso: params[:piso],
          municipio_id: params[:ciudad_id],
          sociedad_mercantil_id: params[:sociedad_mercantil_id],
          sociedad_mercantil_otros: params[:sociedad_mercantil_otros],
          rif_imagen: params[:rif_imagen],
          documento_acreditacion: params[:documento_acreditacion],
          acta_socios: params[:acta_socios]
      )
      operadora_param.permit(:id,
                             :razon_social, :rif,
                             :email, :telefono, :movil, :fax,
                             :observacion, :avenida,
                             :calle, :urbanizacion, :edificio_cc,
                             :local, :piso, :municipio_id,  :sociedad_mercantil_id, :sociedad_mercantil_otros, :rif_imagen,
                             :documento_acreditacion,:acta_socios
      )
    end
    def weak_params
      params[:operadora_transporte_p]
    end
  end
end

