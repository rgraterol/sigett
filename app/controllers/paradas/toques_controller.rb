# -*- encoding : utf-8 -*-
module Paradas
  class ToquesController < ApplicationController
    before_action :armar_breadcrumb
    before_action :set_toque, only: [:show, :edit, :update, :destroy]
    before_action :autorized_user
    # GET /toques
    # GET /toques.json
    def index
      ruta = Ruta.find_by(id: params[:id])
      if ruta.flota.servicio_operadora_c.operadora_transporte_p.usuario.session_user == current_session_user
        @toques = ruta.toques
      else
        redirect_to root_path, notice: "El Id de flota con que se está intentando accesar no pertenece al representante. Vuelva a intentar el proceso"
      end
    end

    # GET /toques/1
    # GET /toques/1.json
    def show
      ruta = Ruta.find_by(id: params[:id])
      if ruta.flota.servicio_operadora_c.operadora_transporte_p.usuario.session_user == current_session_user
        @toques = ruta.toques
      else
        redirect_to root_path, notice: "El Id de flota con que se está intentando accesar no pertenece al representante. Vuelva a intentar el proceso"
      end
    end

    # GET /toques/new
    def new
      flash.delete(:alert) #limpia el flash
      @edit = false
      ruta = Ruta.find_by(id: params[:id])
      if ruta.flota.servicio_operadora_c.operadora_transporte_p.usuario.session_user == current_session_user
        if ruta.flota.servicio_operadora_c.int_urb_p_puesto? || ruta.flota.servicio_operadora_c.int_urb_colectivo? || ruta.flota.servicio_operadora_c.sub_urb_colectivo? || ruta.flota.servicio_operadora_c.sub_urb_p_puesto? || ruta.flota.servicio_operadora_c.periferico_i? || ruta.flota.servicio_operadora_c.periferico_s?
          @toque = ruta.toques.build
          @toque_origen = ruta.toques.where(tipo_parada: 'Origen')
        else
          # redirect_to
        end
      else
        redirect_to root_path, notice: "El Id de flota con que se está intentando accesar no pertenece al representante. Vuelva a intentar el proceso"
      end
    end

    # GET /toques/1/edit
    def edit
      @edit = true
    end

    # POST /toques
    # POST /toques.json
    def create
      ruta = Ruta.find_by(id: params[:id])
      if ruta.flota.servicio_operadora_c.operadora_transporte_p.usuario.session_user == current_session_user
        @toque = Toque.set_new_toque(ruta, toque_params)
      else
        redirect_to root_path, notice: "El Id de ruta con que se está intentando accesar no pertenece al representante. Vuelva a intentar el proceso"
      end

      respond_to do |format|
        if Toque.actualizar_prev_next(@toque)
          format.html { redirect_to paradas_index_path(id: params[:id]), notice: 'Toque fue guardado con éxito.' }
          format.json { render :show, status: :created, location: @toque }
        else
          format.html { render :new }
          format.json { render json: @toque.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /toques/1
    # PATCH/PUT /toques/1.json
    def update
      respond_to do |format|
        if @toque.update(toque_params)
          format.html { redirect_to paradas_index_path(id: params[:id]), notice: 'El toque/parada fue guardada con éxito' }
          format.json { render :show, status: :ok, location: @toque }
        else
          format.html { render :edit }
          format.json { render json: @toque.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /toques/1
    # DELETE /toques/1.json
    def destroy
      ruta = @toque.ruta
      ruta.toques.each do |toque|
        if toque.origen?
          toque.prev_id = nil
          toque.next_id = nil
          toque.save
        else
          toque.destroy
        end
      end
      respond_to do |format|
        format.html { redirect_to paradas_index_path(id: ruta.id), notice: 'Los toques fueron eliminados. Excepto el Origen. Para modificar el origen Edite la sección de Ruta' }
        format.json { head :no_content }
      end
    end

    def nueva_parada
      @ruta = Ruta.find_by(id: params[:ruta_id])
      @parada = Parada.new()
      @parada.estado_id = params[:estado_id]
      @parada.tipo_parada = params[:tipo]

      respond_to do |format|
        # format.html
        format.js
      end
    end

    def guardar_parada
      @ruta = Ruta.find_by(id: params[:ruta_id])
      @parada = Parada.new(nueva_parada_params)

      respond_to do |format|
        @parada.save
        format.js
      end
    end

    def verificar_destino_ruta
      id = ActionController::Parameters.new(id: params[:id])
               .permit(:id)[:id]
      @ruta = Ruta.find_by(id: id)
      s = @ruta.flota.servicio_operadora_c

      #verificar que pertenesca al usuarios
      if @ruta.flota.servicio_operadora_c.operadora_transporte_p.usuario.session_user == current_session_user
        @toques = @ruta.toques
      else
        flash[:alert] = 'Problema de seguridad, Vuelva a intentar el proceso.'
        render js: "window.location.replace('#{root_path}');"
      end
      #verifico segun la tabla de modalidades
      if s.sub_urb_p_puesto? || s.int_urb_p_puesto? || s.sub_urb_colectivo? || s.int_urb_colectivo? || s.periferico_i? || s.periferico_s?
        #verifico si tiene toque destino
        if @toques.where(tipo_parada: "Destino").any?
          #segun la modalidad voy a horario o a historico
          if s.sub_urb_p_puesto? || s.int_urb_p_puesto? || s.sub_urb_colectivo? || s.int_urb_colectivo?
            render js: "window.location.replace('#{horario_d_index_path(id: id)}');"
          elsif s.periferico_i? || s.periferico_s?
            render js: "window.location.replace('#{historico_index_path(id_from: id, from_toques: true)}');"
          end
        else
          flash[:alert] = 'Debe agregar un destino a la ruta.'
        end
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_toque
        @toque = Toque.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def toque_params
        params.require(:toque).permit(:tipo_parada, :tipo, :parada_id, :ciudad_id, :prev)
      end

      def nueva_parada_params
        params.require(:parada).permit(:estado_id,
                                       :ciudad_id,
                                       :parroquia_v_id,
                                       :tipo_parada,
                                       :nombre)
      end
  end
end

