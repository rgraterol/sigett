# -*- encoding : utf-8 -*-
module Paradas
  class ParadasController < ApplicationController
    before_action :armar_breadcrumb
    before_action :set_parada, only: [:show, :edit, :update, :destroy]
    before_filter :authenticate_session_user!
    before_action :autorized_user
    # GET /paradas
    # GET /paradas.json
    def index
      @lista = Hash.new
      @paradas = Parada.new
      @lista[:municipios] = Array.new
    end

    # GET /paradas/1
    # GET /paradas/1.json
    def show
    end

    # GET /paradas/new
    def new
      @parada = Parada.new
    end

    def new_by_post
      @edit = false
      @parada = Parada.new
      @parada[:parroquia_v_id] = params[:parada][:parroquia_v_id]
    end

    # GET /paradas/1/edit
    def edit
    end

    def buscar
      paradas = Parada.where(municipio_id: params[:municipio_id])
      render partial: 'parada', collection: paradas
    end
    def create_ubicacion
      @parada = Parada.new(parada_params)

      respond_to do |format|
        if @parada.save
          format.html { redirect_to paradas_index_ubicacion_path, notice: 'Parada fue creada con éxito.' }
          format.json { render :show, status: :created, location: @parada }
        else
          format.html { render :new_by_post}
          format.json { render json: @parada.errors, status: :unprocessable_entity }
        end
      end
    end
    # POST /paradas
    # POST /paradas.json
    def create
      @parada = Parada.new(parada_params)
      respond_to do |format|
        if @parada.save
          if params[:create_t_f] == nil
            format.html { redirect_to paradas_index_ubicacion_path, notice: 'Parada fue creada con éxito.' }
          else
            if params[:create_t_f] == "false" or params[:create_t_f] == false
              format.html { redirect_to paradas_new_path(id: params[:id_para_retornar],municipio: cookies[:municipio_id],estado: cookies[:estado_id],tipo_parada: cookies[:tipo_parada], tipo: cookies[:tipo], parroquia: cookies[:parroquia_v_id], ciudad: cookies[:ciudad_id]), notice: 'La parada fue guardada.' }
            else
              format.html { redirect_to ruta_flota_new_path(id: params[:id_para_retornar], estado: cookies[:estado_id],
                                                            municipio: cookies[:municipio_id], num: cookies[:num_ruta],
                                                            fecha_1: cookies[:fecha_auto_dia], fecha_2: cookies[:fecha_auto_mes],fecha_3: cookies[:fecha_auto_ano],
                                                            tipo_parada: cookies[:tipo_parada], tipo: cookies[:tipo], parroquia: cookies[:parroquia_v_id], ciudad: cookies[:ciudad_id]), notice: 'La parada fue guardada.' }
            end
          end
          format.json { render :show, status: :created, location: @parada }
        else
          format.html { redirect_to paradas_error_parada_ubicacion_path(id: params[:id_para_retornar], estado: cookies[:estado_id],
                                                        municipio: cookies[:municipio_id], num: cookies[:num_ruta],
                                                        fecha_1: cookies[:fecha_auto_dia], fecha_2: cookies[:fecha_auto_mes],fecha_3: cookies[:fecha_auto_ano],
                                                        tipo_parada: cookies[:tipo_parada], tipo: cookies[:tipo], parroquia: cookies[:parroquia_v_id], ciudad: cookies[:ciudad_id],
                                                        new: false, nombre: @parada.nombre, coordenada: @parada.coordenada_utm, avenida: @parada.avenida, calle: @parada.calle, sector: @parada.sector, create: params[:create_t_f]) }
          format.json { render json: @parada.errors, status: :unprocessable_entity }
        end
      end
    end


    def error_parada_ubicacion
      @parada = Parada.new(nombre: params[:nombre], coordenada_utm: params[:coordenada], avenida: params[:avenida], calle: params[:calle], sector: params[:sector], ciudad_id: params[:ciudad], parroquia_v_id: params[:parroquia])
      @parada.valid?
      render :new
    end
    # PATCH/PUT /paradas/1
    # PATCH/PUT /paradas/1.json
    def update
      respond_to do |format|
        if @parada.update(parada_params)
          if params[:create_t_f] == nil
            format.html { redirect_to paradas_index_ubicacion_path, notice: 'Parada fue reada correctamente.' }
          else
            if params[:create_t_f] == "false" or params[:create_t_f] == false
              format.html { redirect_to paradas_new_path(id: params[:id_para_retornar],municipio: cookies[:municipio_id],estado: cookies[:estado_id],tipo_parada: cookies[:tipo_parada], tipo: cookies[:tipo], parroquia: cookies[:parroquia_v_id], ciudad: cookies[:ciudad_id]), notice: 'La parada fue guardada.' }
            else
              format.html { redirect_to ruta_flota_new_path(id: params[:id_para_retornar], estado: cookies[:estado_id],
                                                            municipio: cookies[:municipio_id], num: cookies[:num_ruta],
                                                            fecha_1: cookies[:fecha_auto_dia], fecha_2: cookies[:fecha_auto_mes],fecha_3: cookies[:fecha_auto_ano],
                                                            tipo_parada: cookies[:tipo_parada], tipo: cookies[:tipo], parroquia: cookies[:parroquia_v_id], ciudad: cookies[:ciudad_id]), notice: 'Parada fue guardada con éxito.' }
            end
          end
          format.json { render :show, status: :ok, location: @parada }
        else
          format.html { render :edit }
          # format.html { redirect_to ruta_flota_new_path(id: params[:id_para_retornar], estado: cookies[:estado_id],
          #                                             municipio: cookies[:municipio_id], num: cookies[:num_ruta],
          #                                             fecha_1: cookies[:fecha_auto_dia], fecha_2: cookies[:fecha_auto_mes],fecha_3: cookies[:fecha_auto_ano],
          #                                             tipo_parada: cookies[:tipo_parada], tipo: cookies[:tipo], parroquia: cookies[:parroquia_v_id], ciudad: cookies[:ciudad_id]) }
          format.json { render json: @parada.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /paradas/1
    # DELETE /paradas/1.json
    def destroy
      @parada.destroy
      respond_to do |format|
        format.html { redirect_to paradas_index_ubicacion_path, notice: 'Parada fue eliminada con éxito.' }
        format.json { head :no_content }
      end
    end

    def cookie_contructor
      cookies.permanent[:estado_id] = params[:estado_id]
      cookies.permanent[:municipio_id] = params[:municipio_id]
      cookies.permanent[:num_ruta] = params[:num_ruta]
      cookies.permanent[:ida_vuelta] = params[:ida_vuelta]
      cookies.permanent[:fecha_auto_dia] = params[:fecha_auto_dia]
      cookies.permanent[:fecha_auto_mes] = params[:fecha_auto_mes]
      cookies.permanent[:fecha_auto_ano] = params[:fecha_auto_ano]
      cookies.permanent[:tipo_parada] = params[:tipo_parada]
      cookies.permanent[:tipo] = params[:tipo]
      cookies.permanent[:id_cookie] = params[:id_cookie]
      cookies.permanent[:create_band] = params[:create_band]
      cookies.permanent[:ciudad_id] = params[:ciudad_id]
      cookies.permanent[:parroquia_v_id] = params[:parroquia_v_id]
      render :json => []
    end

    def redirect_parada
      redirect_to paradas_new_ubicacion_path(id: cookies[:id_cookie], creando: true, estado: cookies[:estado_id], municipio: cookies[:municipio_id], parroquia: cookies[:parroquia_v_id])
    end

    def redirect_ruta_flota_new
      redirect_to ruta_flota_new_path(id: params[:id], estado: cookies[:estado_id],
                            municipio: cookies[:municipio_id], num: cookies[:num_ruta],
                            fecha_1: cookies[:fecha_auto_dia], fecha_2: cookies[:fecha_auto_mes],fecha_3: cookies[:fecha_auto_ano],
                            tipo_parada: cookies[:tipo_parada], tipo: cookies[:tipo], parroquia: cookies[:parroquia_v_id])
    end

    private
      # Use callbacks to share common setup or constraints between actions.

      def set_parada
        @parada = Parada.find(params[:id])
      end

      def parada_params
        params.require(:parada).permit(:nombre, :coordenada_utm,
                                       :avenida, :calle, :sector, :ciudad_id, :parroquia_v_id, :tipo_parada)
      end

      def paradas_params
        paradas_param = ActionController::Parameters.new(nombre: params[:nombre],
                                                         coordenada_utm: params[:coordenada_utm],
                                                         avenida: params[:avenida],
                                                         calle: params[:calle],
                                                         sector: params[:sector],
                                                         municipio_id: params[:municipio_id], parroquia_v_id: params[:parroquia_v_id],
                                                         tipo_parada: params[:tipo_parada])

        paradas_param.permit(:id, :nombre, :coordenada_utm,
                             :avenida,:calle, :sector, :ciudad_id, :parroquia_v_id, :tipo_parada)
      end

      def weak_params
        params[:parada]
      end

  end
end
