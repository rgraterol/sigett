class Paradas::ToquesDinamicoController < ApplicationController
  before_action :set_toque, only: [:eliminar,
                                   :editar,
                                   :actualizar]
  before_action :armar_breadcrumb, only: [:index]

  def index
    @ruta = Ruta.find_by(id: params[:id])
    if @ruta.flota.servicio_operadora_c.operadora_transporte_p.usuario.session_user == current_session_user
      @toques = @ruta.toques
      @toque = @ruta.toques.build
      @servicio_operadora_c = @ruta.flota.servicio_operadora_c
    else
      redirect_to root_path, notice: "El Id de flota con que se está intentando accesar no pertenece al representante. Vuelva a intentar el proceso"
    end
  end

  def show
    ruta = Ruta.find_by(id: params[:id])
    if ruta.flota.servicio_operadora_c.operadora_transporte_p.usuario.session_user == current_session_user
      @toques = ruta.toques
    else
      redirect_to root_path, notice: "El Id de flota con que se está intentando accesar no pertenece al representante. Vuelva a intentar el proceso"
    end
  end

  def crear
    ruta = Ruta.find_by(id: params[:id])
    @toquenuevo = ruta.toques.build
    if ruta.flota.servicio_operadora_c.operadora_transporte_p.usuario.session_user == current_session_user
      @toque = Toque.set_new_toque(ruta, toque_params)
      if ruta.toques.where(tipo_parada: "Destino").any? && params[:toque][:tipo_parada] == 'Destino'
        flash.now[:alert] = 'Esta ruta posee un destino, para agregar más toques intermedios, elimine el destino.'
        render partial: 'paradas/toques_dinamico/parciales/carga_flash_toques'
        return
      end
    else
      flash.now[:alert] = 'El Id de ruta con que se está intentando accesar no pertenece al representante. Vuelva a intentar el proceso.'
      render js: "window.location.replace('#{root_path}');"
      return
    end

    Toque.set_new_toque(ruta, toque_params)
    respond_to do |format|
      Toque.actualizar_prev_next(@toque)
      format.js
    end
  end

  def editar

  end

  def actualizar
    ruta = @toque.ruta
    @toquenuevo = ruta.toques.build
    if ruta.toques.where(tipo_parada: "Destino").any? && params[:toque][:tipo_parada] == 'Destino'
      flash.now[:alert] = 'Esta ruta posee un destino, para agregar más toques intermedios, elimine el destino.'
      render partial: 'paradas/toques_dinamico/parciales/carga_flash_toques'
      return
    end
    respond_to do |format|
      @toque.update(toque_params)
      format.js
    end
  end

  def eliminar
    ruta = @toque.ruta
    @toquenuevo = ruta.toques.build
    Toque.delete_toque(@toque)
    respond_to do |format|
      format.js
    end
  end

  def nueva_parada
    @ruta = Ruta.find_by(id: params[:ruta_id])
    @parada = Parada.new()
    @parada.estado_id = params[:estado_id]
    @parada.tipo_parada = params[:tipo]

    respond_to do |format|
      # format.html
      format.js
    end
  end

  def guardar_parada
    @ruta = Ruta.find_by(id: params[:ruta_id])
    @parada = Parada.new(nueva_parada_params)

    respond_to do |format|
      @parada.save
      format.js
    end
  end

  def verificar_destino_ruta
    id = ActionController::Parameters.new(id: params[:id])
             .permit(:id)[:id]
    @ruta = Ruta.find_by(id: id)
    s = @ruta.flota.servicio_operadora_c

    #verificar que pertenesca al usuarios
    if @ruta.flota.servicio_operadora_c.operadora_transporte_p.usuario.session_user == current_session_user
      @toques = @ruta.toques
    else
      flash.now[:alert] = 'Problema de seguridad, Vuelva a intentar el proceso.'
      render js: "window.location.replace('#{root_path}');"
    end
    #verifico segun la tabla de modalidades
    if s.sub_urb_p_puesto? || s.int_urb_p_puesto? || s.sub_urb_colectivo? || s.int_urb_colectivo? || s.periferico_i? || s.periferico_s?
      #verifico si tiene toque destino
      if @toques.where(tipo_parada: "Destino").any?
        #segun la modalidad voy a horario o a historico
        if s.sub_urb_p_puesto? || s.int_urb_p_puesto? || s.sub_urb_colectivo? || s.int_urb_colectivo?
          render js: "window.location.replace('#{horario_d_index_path(id: id)}');"
        elsif s.periferico_i? || s.periferico_s?
          render js: "window.location.replace('#{historico_index_path(id: id, id_servicio: s.id, id_from: 'ruta')}');"
        end
      else
        flash.now[:alert] = 'Debe agregar un destino a la ruta.'
      end
    end
  end

  private
  def set_toque
    id = ActionController::Parameters.new(id: params[:id_toque]).permit(:id)[:id]
    @toque = Toque.where(id: id).first
    if @toque.nil?
      flash.now[:alert] = 'Error este toque ya fue eliminado o no existe.'
      render js: "window.location.replace('#{root_path}');"
      return
    end
    unless @toque.ruta.flota.servicio_operadora_c.operadora_transporte_p.usuario.session_user == current_session_user
      flash.now[:alert] = 'Error de seguridad.'
      render js: "window.location.replace('#{root_path}');"
      return
    end
  end

  def toque_params
    params.require(:toque).permit(:ruta_id,
                                  :id,
                                  :ruta_solicitada_id,
                                  :tipo_parada,
                                  :tipo,
                                  :parada_id)
  end

  def nueva_parada_params
    params.require(:parada).permit(:estado_id,
                                   :ciudad_id,
                                   :parroquia_v_id,
                                   :tipo_parada,
                                   :nombre)
  end
end
