class Paradas::AdmParadasController < ApplicationController
  before_action :set_paradas_adm_parada, only: [:show, :edit, :update, :destroy]

  # GET /paradas/adm_paradas
  # GET /paradas/adm_paradas.json
  def index
    @paradas_adm_paradas = Parada.all.order(:nombre)
  end

  # GET /paradas/adm_paradas/1
  # GET /paradas/adm_paradas/1.json
  def show
  end

  # GET /paradas/adm_paradas/new
  def new
    @paradas_adm_parada = Parada.new
  end

  # GET /paradas/adm_paradas/1/edit
  def edit
  end

  # POST /paradas/adm_paradas
  # POST /paradas/adm_paradas.json
  def create
    @paradas_adm_parada = Parada.new(paradas_adm_parada_params)

    respond_to do |format|
      if @paradas_adm_parada.save
        format.html { redirect_to paradas_adm_paradas_path, notice: 'El terminal fue creado exitosamente.' }
        # format.json { render :show, status: :created, location: @paradas_adm_parada }
      else
        format.html { render :new }
        format.json { render json: @paradas_adm_parada.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /paradas/adm_paradas/1
  # PATCH/PUT /paradas/adm_paradas/1.json
  def update
    respond_to do |format|
      if @paradas_adm_parada.update(paradas_adm_parada_params)
        format.html { redirect_to  paradas_adm_paradas_path, notice: 'El terminal fue actualizado exitosamente.' }
        format.json { render :show, status: :ok, location: @paradas_adm_parada }
      else
        format.html { render :edit }
        format.json { render json: @paradas_adm_parada.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /paradas/adm_paradas/1
  # DELETE /paradas/adm_paradas/1.json
  def destroy
    @paradas_adm_parada.destroy
    respond_to do |format|
      format.html { redirect_to paradas_adm_paradas_url, notice: 'El terminal fue eliminado exitosamente.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_paradas_adm_parada
      @paradas_adm_parada = Parada.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def paradas_adm_parada_params
      params.require(:parada).permit(:id, :ciudad_id, :parroquia_v_id, :nombre, :estado_id, :tipo_parada)
    end
end
