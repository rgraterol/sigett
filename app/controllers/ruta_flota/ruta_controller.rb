# -*- encoding : utf-8 -*-
module RutaFlota
  class RutaController < ApplicationController
    before_action :armar_breadcrumb, except: [:create,
                                              :update ,
                                              :update_taxi,
                                              :create_taxi,
                                              :destroy,
                                              :nueva_parada,
                                              :guardar_parada]
    before_action :set_ruta, only: [:update,:update_taxi, :destroy]
    before_filter :authenticate_session_user!
    before_action :autorized_user
    # GET /ruta
    # GET /ruta.json
    def index
      flota = Flota.find_by(id: params[:id])
      if flota.servicio_operadora_c.operadora_transporte_p.usuario.session_user == current_session_user
        @servicio_operadora_c = flota.servicio_operadora_c
        # puts "MODALIDAD #{flota.servicio_operadora_c.taxi?} **********************"
        @rutas = flota.rutas
        if flota.servicio_operadora_c.taxi? || flota.servicio_operadora_c.moto_taxi?
          if @rutas.empty?
            redirect_to ruta_flota_new_taxi_path(id: flota.id)
          else
            redirect_to ruta_flota_edit_taxi_path(id: @rutas.first)
          end
        elsif @rutas.empty?
          redirect_to ruta_flota_new_path(id: params[:id]), notice: "No posee Rutas o Paradas registradas. Registre una nueva ruto o parada "
        end
      else
        redirect_to root_path, notice: "El Id de flota con que se está intentando accesar no pertenece al representante. Vuelva a intentar el proceso"
      end
    end

    # GET /ruta/1
    # GET /ruta/1.json
    def show
      # puts "545444822482842428"
      flota = Flota.find_by(id: params[:id])
      # operadora_transporte_p = OperadoraTransporteP.find_by(id: params[:oper])
      if flota.servicio_operadora_c.operadora_transporte_p.usuario.session_user == current_session_user
        @servicio_operadora_c = flota.servicio_operadora_c
        @ruta = Flota.find_by(id: params[:id]).rutas
      else
        redirect_to root_path, notice: "El Id de flota con que se está intentando accesar no pertenece al representante. Vuelva a intentar el proceso"
      end
    end

    # GET /ruta/new
    def new
      @edit = []
      @edit[0] = false
      flota = Flota.find_by(id: params[:id])
      if flota.servicio_operadora_c.operadora_transporte_p.usuario.session_user == current_session_user
        @servicio_operadora_c = flota.servicio_operadora_c
        if flota.rutas.any?
          @edit[1] = true
        else
          @edit[1] = false
        end
        @ruta = flota.rutas.build
        @toque = @ruta.toques.build
        @parada = Parada.new
      else
        redirect_to root_path, notice: "El Id de flota con que se está intentando accesar no pertenece al representante. Vuelva a intentar el proceso"
      end
    end

    # GET /ruta/1/edit
    def edit
      @edit = []
      @edit[0] = true
      ruta = Ruta.find_by(id: params[:id])
      if ruta.flota.servicio_operadora_c.operadora_transporte_p.usuario.session_user == current_session_user
        @servicio_operadora_c = ruta.flota.servicio_operadora_c
        #si es taxi o mototaxi
        if ruta.flota.servicio_operadora_c.taxi? || ruta.flota.servicio_operadora_c.moto_taxi?
          unless ruta.flota.tiene_ruta
            redirect_to ruta_flota_edit_taxi_path(id: params[:id])
          end
        end

        if ruta.flota.rutas.any?
          @edit[1] = true
        else
          @edit[1] = false
        end
        @ruta = ruta
        # @toque = @ruta.toques.where(prev_id: nil).last
      else
        redirect_to root_path, notice: "El Id de flota con que se está intentando accesar no pertenece al representante. Vuelva a intentar el proceso"
      end
    end

    # POST /ruta
    # POST /ruta.json
    def create
      flota = Flota.find_by(id: params[:id])
      if flota.servicio_operadora_c.operadora_transporte_p.usuario.session_user == current_session_user
        @ruta = flota.rutas.build(ruta_params)
        # @toque = @ruta.toques.build(parada_id: ruta_params[:toques_attributes][:parada_id], tipo_parada: ruta_params[:toques_attributes][:tipo_parada] ,tipo: ruta_params[:toques_attributes][:tipo])
        @toque = @ruta.toques.last
      else
        redirect_to root_path, notice: "El Id de flota con que se está intentando accesar no pertenece al representante. Vuelva a intentar el proceso"
      end
      respond_to do |format|
        if @ruta.save
          self.asignar_cookie(ApplicationController::RUTA,'TRUE')
          self.asignar_cookie(ApplicationController::RUTAID,@ruta.id)
          format.html { redirect_to paradas_index_path(id: @ruta.id), notice: 'Las ruta fue guardada con éxito.' }
          format.json { render :show, status: :created, location: @ruta }
        else
          @edit = false
          format.html { render :new }
          format.json { render json: @ruta.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /ruta/1
    # PATCH/PUT /ruta/1.json
    def update
      respond_to do |format|
        if @ruta.update(ruta_params)
          self.asignar_cookie(ApplicationController::RUTA,'TRUE')
          self.asignar_cookie(ApplicationController::RUTAID,@ruta.id)
          format.html { redirect_to paradas_index_path(id: @ruta.id), notice: 'Las ruta fue guardada con éxito.' }
          format.json { render :show, status: :ok, location: @ruta }
        else
          @edit = true
          format.html { render :edit }
          format.json { render json: @ruta.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /ruta/1
    # DELETE /ruta/1.json
    def destroy
      @ruta.destroy
      respond_to do |format|
        format.html { redirect_to ruta_url, notice: 'La ruta fue eliminada con éxito.' }
        format.json { head :no_content }
      end
    end

    def new_taxi
      flota = Flota.find_by(id: params[:id])
      @ruta = flota.rutas.build
      @ruta.toques.build.build_parada
      @servicio_operadora_c = flota.servicio_operadora_c
    end

    def edit_taxi
      ruta = Ruta.find_by(id: params[:id])
      if ruta.flota.servicio_operadora_c.operadora_transporte_p.usuario.session_user == current_session_user
        @ruta = ruta
        @servicio_operadora_c = ruta.flota.servicio_operadora_c
      else
        redirect_to root_path, notice: "El Id de flota con que se está intentando accesar no pertenece al representante. Vuelva a intentar el proceso"
      end
    end

    def update_taxi
      respond_to do |format|
        if @ruta.update(ruta_taxi_params)
          self.asignar_cookie(ApplicationController::RUTA,'TRUE')
          self.asignar_cookie(ApplicationController::RUTAID,@ruta.id)
          s = @ruta.flota.servicio_operadora_c
          format.html { redirect_to historico_index_path(id: @ruta.id, id_servicio: s.id, id_from: 'ruta'), notice: 'La ruta fue actualizada con éxito.' }
          # format.html { redirect_to paradas_index_path(id: @ruta.id), notice: 'La ruta fue guardada con éxito.' }
          format.json { render :show, status: :ok, location: @ruta }
        else
          @edit = true
          format.html { render :edit_taxi }
          format.json { render json: @ruta.errors, status: :unprocessable_entity }
        end
      end
    end

    def nueva_parada
      @parada = Parada.new()
      @parada.estado_id = params[:estado_id]
      @parada.tipo_parada = params[:tipo]

      respond_to do |format|
        # format.html
        format.js
      end
    end

    def guardar_parada
      @parada = Parada.new(nueva_parada_params)
      @parada.estado_id = params[:parada][:estado_id]

      respond_to do |format|
        @parada.save
        format.js
      end
    end

    def create_taxi
      flota = Flota.find_by(id: params[:id])
      s = flota.servicio_operadora_c
      if flota.servicio_operadora_c.operadora_transporte_p.usuario.session_user == current_session_user
        @ruta = flota.rutas.build(ruta_taxi_params)
        @toque = @ruta.toques.last
        respond_to do |format|
          if @ruta.save(validate: false)
            #deberia ir a historico
            format.html { redirect_to historico_index_path(id: @ruta.id, id_servicio: s.id, id_from: 'ruta'), notice: 'La ruta fue guardada con éxito.' }
            # format.html { redirect_to paradas_index_path(id: @ruta.id), notice: 'La ruta fue guardada con éxito.' }
            format.json { render :show, status: :created, location: @ruta }
          else
            # @edit = false
            format.html { render :new_taxi }
            format.json { render json: @ruta.errors, status: :unprocessable_entity }
          end
        end
      else
        redirect_to root_path, notice: "El Id de flota con que se está intentando accesar no pertenece al representante. Vuelva a intentar el proceso"
      end
    end


    private

      # Use callbacks to share common setup or constraints between actions.
      def set_ruta
        @ruta = Ruta.find_by(id: params[:id])
        # self.asignar_cookie(ApplicationController::RUTAID,@ruta.id)
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def ruta_params
        params.require(:ruta).permit(:num_ruta,
                                     :fecha_autorizacion,
                                     :tipo,
                                     :ida_vuelta,
                                     :municipio_id,
                                     toques_attributes: [:id,
                                                         :parada_id,
                                                         :tipo_parada,
                                                         :tipo])
      end

      def nueva_parada_params
        params.require(:parada).permit(:estado_id,
                                       :ciudad_id,
                                       :parroquia_v_id,
                                       :tipo_parada,
                                       :nombre)
      end

      def ruta_taxi_params
        params.require(:ruta).permit(:id,
                                     :num_ruta,
                                     :fecha_autorizacion,
                                     :tipo,
                                     :ida_vuelta,
                                     :municipio_id,
                                     toques_attributes: [:id,
                                                         :parada_id,
                                                         :ciudad_id,
                                                         :tipo_parada,
                                                         :tipo,
                                                         parada_attributes: [:id,
                                                                             :nombre,
                                                                             :estado_id,
                                                                             :parroquia_v_id,
                                                                             :ciudad_id]])
      end
  end
end

