# -*- encoding : utf-8 -*-
module HorarioD
  class HorariosController < ApplicationController
    before_action :set_horario, only: [:edit, :update, :destroy]
    before_filter :authenticate_session_user!
    before_action :autorized_user
    before_action :armar_breadcrumb, except: [:create, :update, :destroy]
    # protect_from_forgery only: :verificar_destino_ruta

    # GET /horarios
    # GET /horarios.json
    def index
      ruta = Ruta.find_by(id: params[:id])
      @ruta = ruta
      @servicio_operadora_c = ruta.flota.servicio_operadora_c
      if ruta.flota.servicio_operadora_c.operadora_transporte_p.usuario.session_user == current_session_user
        if ruta.flota.servicio_operadora_c.int_urb_p_puesto? || ruta.flota.servicio_operadora_c.int_urb_colectivo? || ruta.flota.servicio_operadora_c.sub_urb_p_puesto? || ruta.flota.servicio_operadora_c.sub_urb_colectivo? || ruta.flota.servicio_operadora_c.periferico_i? || ruta.flota.servicio_operadora_c.periferico_s?
          self.asignar_cookie(ApplicationController::HORARIO,'TRUE')
          @horarios = ruta.horarios
        else
          redirect_to historico_index_path(id_from: ruta.id, from_toques: true)
        end
      else
        redirect_to root_path, notice: "El Id de flota con que se está intentando accesar no pertenece al representante. Vuelva a intentar el proceso"
      end
    end

    # GET /horarios/1
    # GET /horarios/1.json
    def show
      ruta = Ruta.find_by(id: params[:id])
      if ruta.flota.servicio_operadora_c.operadora_transporte_p.usuario.session_user == current_session_user
        if ruta.flota.servicio_operadora_c.int_urb_p_puesto? || ruta.flota.servicio_operadora_c.int_urb_colectivo? || ruta.flota.servicio_operadora_c.sub_urb_p_puesto? || ruta.flota.servicio_operadora_c.sub_urb_colectivo?
          @horarios = ruta.horarios
          @ruta = ruta
        else
          redirect_to historico_index_path(id_from: ruta.id, from_toques: true)
        end
      else
        redirect_to root_path, notice: "El Id de flota con que se está intentando accesar no pertenece al representante. Vuelva a intentar el proceso"
      end
    end

    def consultar_show
      ruta = Ruta.find_by(id: params[:id])
      if ruta.flota.servicio_operadora_c.operadora_transporte_p.usuario.session_user == current_session_user
        if ruta.flota.servicio_operadora_c.int_urb_p_puesto? || ruta.flota.servicio_operadora_c.int_urb_colectivo? || ruta.flota.servicio_operadora_c.sub_urb_p_puesto? || ruta.flota.servicio_operadora_c.sub_urb_colectivo?
          @horarios = ruta.horarios
          @ruta = ruta
        else
          redirect_to historico_index_path(id_from: ruta.id, from_toques: true)
        end
      else
        redirect_to root_path, notice: "El Id de flota con que se está intentando accesar no pertenece al representante. Vuelva a intentar el proceso"
      end
    end

    # GET /horarios/new
    def new
      @edit = false
      ruta = Ruta.find_by(id: params[:id])
      if ruta.flota.servicio_operadora_c.operadora_transporte_p.usuario.session_user == current_session_user
        @hora = ruta.horarios.build
      else
        redirect_to root_path, notice: "El Id de flota con que se está intentando accesar no pertenece al representante. Vuelva a intentar el proceso"
      end
    end

    # GET /horarios/1/edit
    def edit
      @edit = true
    end

    # POST /horarios
    # POST /horarios.json
    def create
      @horario = Horario.new(horario_params)

      respond_to do |format|
        if @horario.save
          self.asignar_cookie(ApplicationController::HORARIO,'TRUE')
          format.html { redirect_to horario_d_index_path(id: @horario.ruta.id), notice: 'Horario fue creado con éxito.' }
          format.json { render :show, status: :created, location: @horario }
        else
          format.html { render :new }
          format.json { render json: @horario.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /horarios/1
    # PATCH/PUT /horarios/1.json
    def update
      respond_to do |format|
        if @horario.update(horario_params)
          self.asignar_cookie(ApplicationController::HORARIO,'TRUE')
          format.html { redirect_to horario_d_index_path(id: @horario.ruta.id), notice: 'Horario fue actualizada con éxito.' }
          format.json { render :show, status: :ok, location: @horario }
        else
          format.html { render :edit }
          format.json { render json: @horario.errors, status: :unprocessable_entity }
        end
      end
    end


    # DELETE /horarios/1
    # DELETE /horarios/1.json
    def destroy
      @horario.destroy
      respond_to do |format|
        format.html { redirect_to horario_d_index_path(id: params[:id_return]), notice: 'Horario fue eliminado con éxito.' }
        format.json { head :no_content }
      end
    end




    private
      # Use callbacks to share common setup or constraints between actions.
      def set_horario
        @horario = Horario.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def horario_params
        params.require(:horario).permit(:dia, :hora, :ruta_id, :ida)
      end
  end
end

