module HorarioD
  class HorariosDinamicoController < ApplicationController
    before_action :set_horario, only: [:edit, :update, :destroy]
    before_filter :authenticate_session_user!
    before_action :autorized_user
    before_action :armar_breadcrumb, only: [:index]

    def index
      @ruta = Ruta.find_by(id: params[:id])
      @servicio_operadora_c = @ruta.flota.servicio_operadora_c
      if @ruta.flota.servicio_operadora_c.operadora_transporte_p.usuario.session_user == current_session_user
        @horarios = @ruta.horarios
        @horario = Horario.new
      else
        redirect_to root_path, notice: "El Id de flota con que se está intentando accesar no pertenece al representante. Vuelva a intentar el proceso"
      end
    end


    def crear_horario
      @ruta = Ruta.find(params[:id])
      if @ruta.flota.servicio_operadora_c.operadora_transporte_p.usuario.session_user == current_session_user
        self.asignar_cookie(ApplicationController::HORARIO,'TRUE')
        @horario = @ruta.horarios.build(horario_params)
        respond_to do |format|
          if @horario.save
            format.js
          else
            format.js
          end
        end
      else
        flash[:alert] = 'Error de seguridad. Vuelva a intentar el proceso.'
        render js: "window.location.replace('#{root_path}');"
        return
      end
    end

    #eliminar en la lista de horarios
    def eliminar_horario
      @horario = Horario.find(params[:id])
      Horario.delete(@horario)
      respond_to do |format|
        format.js
      end
    end


    private
    # Use callbacks to share common setup or constraints between actions.
    def set_horario
      @horario = Horario.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def horario_params
      params.require(:horario).permit(:dia, :hora, :ruta_id, :ida)
    end


  end
end
