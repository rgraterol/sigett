class Admin::ModalidadServiciosController < ApplicationController
  before_action :set_admin_modalidad_servicio, only: [:show, :edit, :update, :destroy]

  # GET /admin/modalidad_servicios
  # GET /admin/modalidad_servicios.json
  def index
    @admin_modalidad_servicios = ModalidadServicio.all
  end

  # GET /admin/modalidad_servicios/1
  # GET /admin/modalidad_servicios/1.json
  def show
  end

  # GET /admin/modalidad_servicios/new
  def new
    @admin_modalidad_servicio = ModalidadServicio.new
  end

  # GET /admin/modalidad_servicios/1/edit
  def edit
  end

  # POST /admin/modalidad_servicios
  # POST /admin/modalidad_servicios.json
  def create
    @admin_modalidad_servicio = ModalidadServicio.new(admin_modalidad_servicio_params)

    respond_to do |format|
      if @admin_modalidad_servicio.save
        format.html { redirect_to admin_modalidad_servicios_path, notice: 'La modalidad de serivicio fue creada exitosamente.' }
        format.json { render :show, status: :created, location: @admin_modalidad_servicio }
      else
        format.html { render :new }
        format.json { render json: @admin_modalidad_servicio.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/modalidad_servicios/1
  # PATCH/PUT /admin/modalidad_servicios/1.json
  def update
    respond_to do |format|
      if @admin_modalidad_servicio.update(admin_modalidad_servicio_params)
        format.html { redirect_to admin_modalidad_servicios_path, notice: 'La modalidad de serivicio fue actualizada exitosamente.' }
        format.json { render :show, status: :ok, location: @admin_modalidad_servicio }
      else
        format.html { render :edit }
        format.json { render json: @admin_modalidad_servicio.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/modalidad_servicios/1
  # DELETE /admin/modalidad_servicios/1.json
  def destroy
    @admin_modalidad_servicio.destroy
    respond_to do |format|
      format.html { redirect_to admin_modalidad_servicios_url, notice: 'Modalidad servicio was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_modalidad_servicio
      @admin_modalidad_servicio = ModalidadServicio.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_modalidad_servicio_params
      params.require(:modalidad_servicio).permit(:id, :nombre, :abreviacion, :rotp)
    end
end
