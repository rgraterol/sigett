# -*- encoding : utf-8 -*-
module Asegurador
  class AseguradorasController < ApplicationController
    before_action :set_aseguradora, only: [:show, :edit, :update, :destroy]
    before_filter :authenticate_session_user!
    before_action :autorized_user
    # GET /aseguradoras
    # GET /aseguradoras.json
    def index
      if current_session_user.email == "admin@admin.com"
        @aseguradoras = Aseguradora.all
        @aseguradora ||= Aseguradora.new
      else
        redirect_to root_path, notice: "No posee permisos para entrar a este lugar"
      end
    end

    # GET /aseguradoras/1
    # GET /aseguradoras/1.json
    def show
    end

    def rif_aseguradora
      render json: [params[:id].blank? ? "" : Aseguradora.find_by(id: params[:id]).rif]
    end

    # GET /aseguradoras/new
    def new
      @edit = false
      @aseguradora = Aseguradora.new
    end

    # GET /aseguradoras/1/edit
    def edit
      @edit = true
      @aseguradoras = Aseguradora.all
    end

    # POST /aseguradoras
    # POST /aseguradoras.json
    def create
      @aseguradora = Aseguradora.new(aseguradora_params)

      respond_to do |format|
        if @aseguradora.save
          format.html { redirect_to asegurador_index_path, notice: 'Aseguradora fue creada con éxito.' }
          format.json { render :show, status: :created, location: @aseguradora }
        else
          format.html { render :new }
          format.json { render json: @aseguradora.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /aseguradoras/1
    # PATCH/PUT /aseguradoras/1.json
    def update
      respond_to do |format|
        if @aseguradora.update(aseguradora_params)
          format.html { redirect_to asegurador_index_path, notice: 'Aseguradora fue actualizada con éxito.' }
          format.json { render :show, status: :ok, location: @aseguradora }
        else
          format.html { render :edit }
          format.json { render json: @aseguradora.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /aseguradoras/1
    # DELETE /aseguradoras/1.json
    def destroy
      @aseguradora.destroy
      respond_to do |format|
        format.html { redirect_to asegurador_index_path, notice: 'Aseguradora fue eliminada con éxito.' }
        format.json { head :no_content }
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_aseguradora
        @aseguradora = Aseguradora.find(params[:id])
      end

    def aseguradora_params
      params.require(:aseguradora).permit(:nombre, :rif)
    end
    def asegurador_params
      asegurador_param = ActionController::Parameters.new(nombre: params[:nombre])
      asegurador_param.permit(:id, :nombre, :rif)
    end
    def weak_params
      params[:aseguradora]
    end

  end
end

