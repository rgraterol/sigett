module Listas
  class ListadoController < ApplicationController
    before_filter :authenticate_session_user!

    def index_operadora
      a = ActiveRecord::Base.connection
      query =  a.exec_query(Listado.todas_operadoras_estado)
      puts query
      @operadoras = Kaminari.paginate_array(query.to_a).page(params[:page]).per(25)
    end

    def index_operadora_est_mod
      estado = ActionController::Parameters.new(estado: params[:estado_id][:id]).permit(:estado)[:estado]
      modalidad = ActionController::Parameters.new(modalidad: params[:value][:modalidad_servicio]).permit(:modalidad)[:modalidad]
      a = ActiveRecord::Base.connection
      query =  a.exec_query(Listado.operadoras_estado_modalidad(modalidad, estado))
      @operadoras = Kaminari.paginate_array(query.to_a).page(params[:page]).per(25)
    end

    def index_operadora_mes_ven
      m = params[:date][:month]
      y = params[:date][:year]
      est = params[:estado][:id].to_i

      connection = ActiveRecord::Base.connection
      inicio = connection.quote(Date.new(y.to_i,m.to_i).beginning_of_month)
      final = connection.quote(Date.new(y.to_i,m.to_i).end_of_month)

      consulta = ListadoSql.new
      @registros = connection.exec_query(consulta.op_mes_vence(inicio,final,est))
    end

    def index_op_mes_cps
      # m = params[:date][:month]
      # y = params[:date][:year]
      # est = params[:estado][:id].to_i
      connection = ActiveRecord::Base.connection
      inicio = connection.quote(Date.new(params[:date][:year].to_i,params[:date][:month].to_i).beginning_of_month)
      final = connection.quote(Date.new(params[:date][:year].to_i,params[:date][:month].to_i).end_of_month)
      consulta = ListadoSql.new
      @registros = connection.exec_query(consulta.op_mes_cps(inicio,final,params[:estado][:id].to_i))
    end

    def index_op_igual_par
      consulta = ListadoSql.new
      connection = ActiveRecord::Base.connection
      @registros = connection.exec_query(consulta.op_igual_parada(params[:value][:estado],
                                                                  params[:value][:tipo_parada],
                                                                  params[:value][:parada]))
    end

    def creador_rutas_buscar
    end

    def listar_por_rutas
      paradas = ActionController::Parameters.new(paradas: params[:parada]).permit(paradas: [])[:paradas]
      @paradas = []
      if paradas.nil? || params[:parada] == "" || params[:parada] == "null"
        @operadoras = {}
      else
        paradas.each do |p|
          @paradas << Parada.find_by(id: p).nombre
        end
        @operadoras = {}
        @operadoras = Kaminari.paginate_array(Listado.operadoras_por_rutas_toques(paradas)).page(params[:page]).per(25)
      end

    end

    def index_operadora_tipo_unidad
      if params[:value][:modalidad_servicio].nil? || params[:value][:modalidad_servicio].blank? ||
          params[:value][:tipo_unidad].nil? || params[:value][:tipo_unidad].blank?
        @error = true
        render :modalidad_tipo_unidad
      else
        @operadoras = Listado.operadoras_tipo_unidad( params[:estado], params[:value][:tipo_unidad], params[:value][:modalidad_servicio] )
        oper = @operadoras.to_a
        @paginado = Kaminari.paginate_array(oper).page(params[:page]).per(25)
      end
    end

    def index_ope_flo_venc
      connection = ActiveRecord::Base.connection
      est    = params[:value][:estado]
      inicio = connection.quote(Date.today)
      final  = connection.quote(Date.today + params[:value][:meses].to_i.months)
      consulta = ListadoSql.new
      @registros = connection.exec_query(consulta.op_prox_flotas_venc(est,inicio,final))
    end

  end
end