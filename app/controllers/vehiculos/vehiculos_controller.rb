# -*- encoding : utf-8 -*-
module Vehiculos
  class VehiculosController < ApplicationController
    before_filter :authenticate_session_user!
    before_action :autorized_user
    before_action :armar_breadcrumb, except:[:buscar_vehiculo, :verificar_modalidad, :create, :update, :destroy]
    before_action :pertece_flota_user, except: [:tiene_arrendatario,
                                                :verificar_modalidad]
    before_action :petenece_vehiculo_flota, only: [:show,:edit,:update,:destroy]

    def index
      @url_wizard_prev= salto_wizard
      @servicio_operadora_c = @flota.servicio_operadora_c
    end

    def show
      @servicio_operadora_c = @flota.servicio_operadora_c
    end

    def edit
      @servicio_operadora_c = @flota.servicio_operadora_c
    end

    def create
      if Vehiculo.find_by(placa: tramite_vehiculo_params[:placa]).nil?
        vehiculo_intt = TramiteVehiculo.find_by_placa_carroceria_ano(tramite_vehiculo_params[:placa],
                                                                     tramite_vehiculo_params[:s_carroceria])
        respond_to do |format|
          if vehiculo_intt.present? && (vehiculo_intt.ID_USO == 'TL' || vehiculo_intt.ID_USO == 'TP') && @flota.servicio_operadora_c.corresponde_modalidad_tipo?(vehiculo_intt)

            @vehiculo = Vehiculo.build_vehiculo_intt(vehiculo_intt, contrato_arrendamiento_params, tramite_vehiculo_params, @flota)
            if @vehiculo.save
              format.html { redirect_to flota_vehiculos_path(@flota),
                                        notice: 'El vehículo fue guardado con éxito.' }
              format.json { render :show, status: :created, location: @vehiculo }
            else
              format.html {render :index }
            end
          else
            format.html { render :index, alert: 'No se pudo encontrar el vehículo.'}
          end
        end
      else
        format.html { render :index, alert: 'Vehículo ya registrado en una operadora de transporte.'}
      end
    end

    def update
      if tramite_vehiculo_params.nil? && contrato_arrendamiento_params.nil?
        redirect_to flota_vehiculos_path(@flota)
      else
        respond_to do |format|
          seguro = @vehiculo.seguro_vehiculo.present? ? @vehiculo.seguro_vehiculo.update(tramite_vehiculo_params[:seguro_vehiculo]) : true
          if seguro
             bandera = contrato_arrendamiento_params.nil? ? true : @vehiculo.contrato_arrendamiento_update_or_create(contrato_arrendamiento_params)
             if bandera
              format.html { redirect_to flota_vehiculos_path(@flota), notice: "Infomación de vehículo con placa #{@vehiculo.placa} actualizada" }
             else
               format.html { render :edit }
               format.json { render json: @vehiculo.errors, status: :unprocessable_entity }
             end
          else
            format.html { render :edit }
            format.json { render json: @vehiculo.errors, status: :unprocessable_entity }
          end
        end
      end
    end

    def destroy
      @vehiculo.destroy
      respond_to do |format|
        format.html { redirect_to flota_vehiculos_path(@flota), notice: 'Vehículo se ha eliminado con éxito.' }
        format.json { head :no_content }
      end
    end

    def tiene_arrendatario
      @vehiculo = Vehiculo.new
      @vehiculo.build_contrato_arrendamiento
      render partial: 'vehiculos/vehiculos/contrato_arrendamiento'
    end

    def buscar_vehiculo
      render partial: 'vehiculo_in_use' and return unless Vehiculo.find_by(placa: ActionController::Parameters.new(placa: params[:placa]).permit(:placa)[:placa].upcase).nil?
      @vehiculo = find_vehiculo_parametros
      flota = Flota.find_by(id: params[:flota_id])
      render partial:'vehiculo_no_carga' and return if @vehiculo.nil?
      render partial: 'vehiculo_not_tipo' and return unless flota.servicio_operadora_c.corresponde_modalidad_tipo?(@vehiculo)
      render partial: 'vehiculo_in_quota' and return unless Vehiculo.where('flota_id = ?',params[:flota_id]).count < flota.cupo_asignado
      @propietario = TramitePropietario.find_by(NRO_TRAMITE: @vehiculo.NRO_TRAMITE) || TramitePropietario.new
      render partial: 'found_vehiculo'
    end

    def verificar_modalidad
      id = ActionController::Parameters.new(id: params[:id])
               .permit(:id)[:id]
      flota = Flota.find_by(id: id)
      #compruebo seguridad
      # p 'verificar modalidad'
      # p flota.servicio_operadora_c.operadora_transporte_p.usuario.session_user == current_session_user
      if flota.servicio_operadora_c.operadora_transporte_p.usuario.session_user == current_session_user
        s = flota.servicio_operadora_c
      else
        flash[:alert] = 'Problema de seguridad, Vuelva a intentar el proceso.'
        render js: "window.location.replace('#{root_path}');"
      end

      if s.sub_urb_p_puesto? || s.sub_urb_colectivo? || s.int_urb_p_puesto? || s.int_urb_colectivo? || s.periferico_i? || s.periferico_s? || s.taxi? || s.moto_taxi?
        render js: "window.location.replace('#{ruta_flota_index_path(id: flota.id)}');"
      elsif s.escolar? || s.turismo? || s.alquiler?
        render js: "window.location.replace('#{historico_index_path(id: flota.id, id_servicio: s.id, id_from: 'flota')}');"
      end


    end

    private

      def find_vehiculo_parametros
        TramiteVehiculo.find_by_placa_carroceria_ano(espacios_blancos_l_r(ActionController::Parameters.new(placa: params[:placa]).permit(:placa)[:placa]),
                                                     espacios_blancos_l_r(ActionController::Parameters.new(s_carroceria: params[:s_carroceria]).permit(:s_carroceria)[:s_carroceria])                                                  )
      end

      def salto_wizard
        if @flota.seguro_vehiculo.nil? and not @flota.vehiculos.empty?
          #tiene seguro particular salto a servicio
          servicio_edit_path(@flota.servicio_operadora_c)
        else
          #Aun no ha escogido seguro lo llevamos para que el usuario escoja
          #Ó Escogio seguro colectivo puedo pasar a editarlo
          seguro_edit_path(id: @flota.id)
        end
      end

      def pertece_flota_user
        begin
          @flota = Flota.find_by(id: ActionController::Parameters.new(flota_id: params[:flota_id]).permit(:flota_id)[:flota_id])
          usuarioAccedido = @flota.servicio_operadora_c.operadora_transporte_p.usuario.session_user
          unless  usuarioAccedido.id == current_session_user.id
            flash[:danger] ='Operación no permitida'
            redirect_to root_path and return
          end
        rescue
          flash[:danger] ='Operación no permitida'
          redirect_to root_path and return
        end
      end

      def petenece_vehiculo_flota
        @vehiculo = @flota.vehiculos.find_by(id: params[:id])
        if @vehiculo.nil?
          flash[:danger]= "Vehículo no pertence a flota"
          redirect_to flota_vehiculos_url(params[:flota_id])
        end
      end

      def tramite_vehiculo_params
        params.require(:vehiculo).permit(:placa,
                                         :s_carroceria,
                                         :ano,
                                         seguro_vehiculo: [:rif,
                                                           :num_poliza,
                                                           :fecha_vencimiento,
                                                           :aseguradora_id,
                                                           :flota_id,
                                                           :documento_poliza]) rescue nil
      end

      def contrato_arrendamiento_params
        params.require(:contrato_arrendamiento).permit(:duracion,
                                                       :fecha,
                                                       :tomo,
                                                       :folio,
                                                       :notaria,
                                                       :nombre_arrendatario,
                                                       :rif_arrendatario) rescue nil
      end
      # def buscar_vehiculo_inttt
      #   @vehiculo_intt = TramiteVehiculo.find_by_placa_carroceria_ano(espacios_blancos_l_r(tramite_vehiculo_params[:placa]),
      #                                                                 espacios_blancos_l_r(tramite_vehiculo_params[:s_carroceria]),
      #                                                                 espacios_blancos_l_r(tramite_vehiculo_params[:ano]))
      #
      #   if(@vehiculo_intt.nil?)
      #     flash[:danger]= "Vehículo con Placa \"#{tramite_vehiculo_params[:placa]}\", Serial de Carroceria: \" #{tramite_vehiculo_params[:s_carroceria]}\", Año: \"#{tramite_vehiculo_params[:ano]}\" no Registrado"
      #     redirect_to flota_vehiculos_url(params[:flota_id]) and return
      #   elsif action_name == 'new'
      #     if @vehiculo_intt.ID_USO != 'CG'
      #       flash[:notice]=
      #           "El vehiculo con Placa: \"#{tramite_vehiculo_params[:placa]}\", Serial de Carroceria: \" #{tramite_vehiculo_params[:s_carroceria]}\", Año: \"#{tramite_vehiculo_params[:ano]}\" tiene un uso distinto al servicio que presta, debe realizar el proceso de matriculación"
      #       redirect_to flota_vehiculos_url(params[:flota_id]) and return
      #     else
      #       flash[:notice]=
      #           "Vehículo con Placa \"#{tramite_vehiculo_params[:placa]}\", Serial de Carroceria: \" #{tramite_vehiculo_params[:s_carroceria]}\", Año: \"#{tramite_vehiculo_params[:ano]}\" encontrado. Por favor complete el siguiente formulario."
      #     end
      #   end
      # end



  end
end

