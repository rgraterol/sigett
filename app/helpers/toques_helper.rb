# -*- encoding : utf-8 -*-
module ToquesHelper
  def paradas_lista(estado_id,tipo_id)
    estado = estado_id
    tipo_parada = tipo_id

    Parada.find_by_sql ['SELECT
                        paradas.nombre,
                        paradas.id
                      FROM
                        public.paradas
                      WHERE
                        paradas.tipo_parada = ? AND
                        paradas.estado_id = ?;',tipo_parada, estado]
  end
end

