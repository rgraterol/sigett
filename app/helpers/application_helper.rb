# -*- encoding : utf-8 -*-
module ApplicationHelper
  def resource_name
    :session_user
  end

  def resource
    @resource ||= SessionUser.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:session_user]
  end

  def extlink(link)
    if link.include?("://")
      link
    else
      link.insert(0, "http://")
      link
    end
  end

  def full_title(page_title='')
    base_title= 'SIGETT'
    page_title.empty? ? base_title : "#{base_title} | #{page_title}"
  end

  def menor_edad? (fecha_nacimiento)
    ( (Date.today - fecha_nacimiento).to_f/365.25).to_i < 18
  end

  def espacios_blancos_l_r(cad)
    cad.lstrip.rstrip
  end

  def igual_i_sin_blancos ( cad1, cad2)
    espacios_blancos_l_r(cad1).casecmp(espacios_blancos_l_r(cad2)) == 0
  end

  def is_active_module(module_name)
    params[:controller].split('/').first == module_name ? 'active' : nil
  end

  def is_active_controller(controller_name)
    params[:controller] == controller_name ? 'active' : nil
  end

  def is_active_action(action_name)
    params[:action] == action_name ? 'active' : nil
  end
end

