# -*- encoding : utf-8 -*-
module RegexHelper
  NOMBRE_REGEX = /\A([a-zA-Z ñáéíóúÑÁÉÍÓÚ 0-9 \. \- ,]{2,})\z/i
  TELEFONO_REGEX = /\A\+?\d{1,3}?[-.]?\(?(?:\d{2,3})\)?[-.]?\d\d\d[-.]?\d\d[-.]?\d\d\z/
  RIF_REGEX  = /\A[VE](([0-9]{1,2}?[0-9]{1,3}[0-9]{3})|([0-9]{1,3}?[0-9]{3})|([0-9]{3}))\d\z/i
  VALID_EMAIL_REGEX = /\A([^@\s]+)@((?:[-a-zA-Z0-9]+\.)+[a-zA-Z]{2,})\z/i
  SOLO_CARACTERES_REGEX = /\A([a-zA-Z ñáéíóúÑÁÉÍÓÚ]{2,})\z/i
  CEDULA_REGEX  = /\A(([0-9]{1,2}?[0-9]{1,3}[0-9]{3})|([0-9]{1,3}?[0-9]{3})|([0-9]{3}))\z/
  SEXO_REGEX = /\A[MF]\z/
  NACIONALIDAD_REGEX= /\A[VE]\z/
  DIGITOS_REGEX = /\A\d+\Z/
  MOVIL_REGEX = /\A(426|416|412|414|424)\d{7}\z/
  ALFANUMERICO_REGEX = /\A([a-zA-Z ñáéíóúÑÁÉÍÓÚ\d])+\z/i
  PREGUNTA_REGEX    =  /\A([a-zA-Z ñáéíóúÑÁÉÍÓÚ\d])+\z/i
  DOC_REGEX = /.(gif|jpeg|jpg|png|pdf)\z/
  POLIZA_REGEX= /.(gif|jpeg|jpg|png|pdf)\z/
  FECHA_REGEX =  /\A\d{1,2}\/\d{1,2}\/\d{2,4}\z/
  CODIGOOPER_REGEX = /\A(\S{4}\d{4})\z/
  RIFOPER_REGEX = /\A([VEJPGvejpg][-]\d{9})\z/
  EMAILOPER_REGEX = /\A[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,3})\z/
  TELEFONOOPER_REGEX = /\A[\x28]\d{3}[\x29][-]\d{7}\z/
  URL_REGEX = /\A(HTTPS?:\/\/(?:WWW\.|(?!WWW))[^\s\.]+\.[^\s]{2,}|WWW\.[^\s]+\.[^\s]{2,})\z/
  TELEFONO_LOCAL_REGEX = /\A\d{10}\z/

end

