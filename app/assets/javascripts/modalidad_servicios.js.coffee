jQuery(document).ready ($) ->
  $('#form_modalidad_servicio').bootstrapValidator
    feedbackIcons:
      valid: 'fa fa-check',
      invalid: 'fa fa-times',
      validating: 'fa fa-refresh'
    live: 'enabled'
    fields:
      'modalidad_servicio[nombre]':
        validators:
          notEmpty:
            message: 'Es necesario indicar el nombre'
          stringLength:
            max: 60
            message: 'El nombre debe tener un maximo de 60 caracteres'
      'modalidad_servicio[abreviacion]':
        validators:
          notEmpty:
            message: 'Es necesario indicar la abreviación'
          stringLength:
            max: 60
            message: 'La abreviación debe tener un maximo de 60 caracteres'
      'modalidad_servicio[rotp]':
        validators:
          notEmpty:
            message: 'Es necesario indicar el codigo'
          stringLength:
            max: 3
            message: 'El codigo debe tener un maximo de 3 caracteres'