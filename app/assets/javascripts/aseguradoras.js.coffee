jQuery(document).ready ($) ->
  $('#aseguradora_t_id_form').bootstrapValidator
    feedbackIcons:
      valid: 'fa fa-check',
      invalid: 'fa fa-times',
      validating: 'fa fa-refresh'
    live: 'enabled'
    fields:
      'aseguradora[nombre]':
        validators:
          notEmpty:
            message: 'Es necesario el nombre de una aseguradora'