jQuery(document).ready ($) ->
#  $('select[data-dynamic-selectable-url][data-dynamic-selectable-target]').dynamicSelectable()
  $("#servicio_operadora_t_id_form").bind "submit", ->
    $(this).find(":input").removeAttr "disabled"
    return

  $('#servicio_operadora_t_id_form').bootstrapValidator
    feedbackIcons:
      valid: 'fa fa-check',
      invalid: 'fa fa-times',
      validating: 'fa fa-refresh'
    live: 'enabled'
    fields:
      'servicio_operadora_c[codigo_rotp]':
        validators:
          regexp:
            regexp: /^(\S{4}\d{4})$/
            message: 'El Código ROTP no válido, debe ingresar un código válido, Ej. PSAS9999'
      'servicio_operadora_c[modalidad_servicio]':
        validators:
          notEmpty:
            message: 'Es necesario seleccionar una opción en modalidad de servicio'
      'servicio_operadora_c[flota_attributes][cupo_asignado]':
        validators:
          notEmpty:
            message: 'Es necesario el cupo autorizado'
          regexp:
            regexp: /^\d*([1-9]\d*)$/
            message: 'No puede ser solo 0'

  $("#servicio_operadora_c_flota_attributes_cupo_asignado").inputmask "9{1,4}",
      placeholder: ""

  $("#servicio_operadora_c_codigo_rotp").inputmask "*{8}",
      placeholder: ""


  $('#servicio_operadora_c_flota_attributes_fecha_autorizacion_3i').change ->
    $('#fecha_vencimiento_asd_3i').val($('#servicio_operadora_c_flota_attributes_fecha_autorizacion_3i').val())

  $('#servicio_operadora_c_flota_attributes_fecha_autorizacion_2i').change ->
    $('#fecha_vencimiento_asd_2i').val($('#servicio_operadora_c_flota_attributes_fecha_autorizacion_2i').val())

  $('#servicio_operadora_c_flota_attributes_fecha_autorizacion_1i').change ->
    e = $('#servicio_operadora_c_flota_attributes_fecha_autorizacion_1i').val()
    $('#fecha_vencimiento_asd_1i').val((String(parseInt(e) + 1)))


  $('#servicio_operadora_c_fecha_expedicion_3i').change ->
    de = $('#servicio_operadora_c_fecha_expedicion_3i').val()
    m = $('#servicio_operadora_c_fecha_expedicion_2i').val()
    e = $('#servicio_operadora_c_fecha_expedicion_1i').val()
    $('#servicio_operadora_c_fecha_vencimiento_3i').val($('#servicio_operadora_c_fecha_expedicion_3i').val())
    d = new Date()
    funcion_para_chequear_vencimiento(de,m,e,d)

  $('#servicio_operadora_c_fecha_expedicion_2i').change ->
    de = $('#servicio_operadora_c_fecha_expedicion_3i').val()
    m = $('#servicio_operadora_c_fecha_expedicion_2i').val()
    e = $('#servicio_operadora_c_fecha_expedicion_1i').val()
    $('#servicio_operadora_c_fecha_vencimiento_2i').val($('#servicio_operadora_c_fecha_expedicion_2i').val())
    d = new Date()
    funcion_para_chequear_vencimiento(de,m,e,d)

  $('#servicio_operadora_c_fecha_expedicion_1i').change ->
    de = $('#servicio_operadora_c_fecha_expedicion_3i').val()
    m = $('#servicio_operadora_c_fecha_expedicion_2i').val()
    e = $('#servicio_operadora_c_fecha_expedicion_1i').val()
    $('#servicio_operadora_c_fecha_vencimiento_1i').val((String(parseInt(e) + 10)))
    d = new Date()
    funcion_para_chequear_vencimiento(de,m,e,d)


#  $('#fecha_ultima_box').change ->
#    if $('#fecha_ultima_id').attr('class') == "form-group"
#      $('#fecha_ultima_id').attr('class', 'form-group hide')
#      return
#
#    if $('#fecha_ultima_id').attr('class') == "form-group hide"
#      $('#fecha_ultima_id').attr('class','form-group')
#      return

  $('#estado_id_nombre').change ->
    $.ajax
      type: "POST"
      url: "/operadora/ciudad_municipio"
      dataType: "JSON"
      data:
        estado_id: $('#estado_id_nombre').val()
      success: (data) ->
        html_ciudad_select = $('#ruta_toques_attributes_0_ciudad_id')
        html_municipio_select = $('#municipio_id_nombre')
        html_ciudad_select.empty()
        html_municipio_select.empty()
        $.each data[1], (i, data_each) ->
          html_ciudad_select.append new Option(data_each.label, data_each.id)
        $.each data[0], (i, data_each) ->
          html_municipio_select.append new Option(data_each.label, data_each.id)

  $('#parroquia_v_id_nombre').change ->
    $.ajax
      type: "POST"
      url: "/dynamic_select/paradas"
      dataType: "JSON"
      data:
        parroquia_id: $('#parroquia_v_id_nombre').val()
      success: (data) ->
        html_parada_select = $('#ruta_toques_attributes_0_parada_id')
        html_parada_select.empty()
        $.each data, (i, data_each) ->
          html_parada_select.append new Option(data_each.label, data_each.id)

  $('#servicio_operadora_c_modalidad_servicio_id').change ->
    $.ajax
      type: "POST"
      url: "/servicio/set_codigo_rotp/"
      dataType: "JSON"
      data:
        modalidad_id: $(this).val()
        cd: $('#codigo').val()
      success: (data) ->
        $('#servicio_operadora_c_codigo_rotp').val(data)
        console.log(data)

  #generar el codigo rotp con la modalidad y el estado
#  $('#servicio_operadora_c_modalidad_servicio').change ->
#    modalidad = $('#servicio_operadora_c_modalidad_servicio option:selected').html()
#    cd = $('#codigo').val()
#    if modalidad == 'Por Puesto SubUrbano'
#      rotp = 'PS' + cd
#      $("#servicio_operadora_c_codigo_rotp").val(rotp)
#    else
#      if modalidad == 'Colectivo SubUrbano'
#        rotp = 'CS' + cd
#        $("#servicio_operadora_c_codigo_rotp").val(rotp)
#      else
#        if modalidad == 'Por Puesto InterUrbano'
#          rotp = 'PI' + cd
#          $("#servicio_operadora_c_codigo_rotp").val(rotp)
#        else
#          if modalidad == 'Colectivo InterUrbano'
#            rotp = 'CI'+ cd
#            $("#servicio_operadora_c_codigo_rotp").val(rotp)
#          else
#            if modalidad == 'Taxi'
#              rotp = 'TX'+ cd
#              $("#servicio_operadora_c_codigo_rotp").val(rotp)
#            else
#              if modalidad == 'Moto Taxi'
#                rotp = 'MT' + cd
#                $("#servicio_operadora_c_codigo_rotp").val(rotp)
#              else
#                if modalidad == 'Turismo'
#                  rotp = 'TT' + cd
#                  $("#servicio_operadora_c_codigo_rotp").val(rotp)
#                else
#                  if modalidad == 'Periferico InterUrbano'
#                    rotp = 'PE' + cd
#                    $("#servicio_operadora_c_codigo_rotp").val(rotp)
#                  else
#                    if modalidad == 'Periferico SubUrbano'
#                      rotp = 'PE' + cd
#                      $("#servicio_operadora_c_codigo_rotp").val(rotp)
#                    else
#                      if modalidad == 'Escolar'
#                        rotp = 'TE' + cd
#                        $("#servicio_operadora_c_codigo_rotp").val(rotp)
#                      else
#                        if modalidad == 'Alquiler de Vehiculo con o sin chofer'
#                          rotp = 'TA' + cd
#                          $("#servicio_operadora_c_codigo_rotp").val(rotp)
#                        else
#                          $("#servicio_operadora_c_codigo_rotp").val(rotp)
#                          rotp = ''

funcion_para_chequear_vencimiento = (de,m,e,d) ->
  if parseInt(e) <= 1992
    $('#fecha_to_hide_extinto').show('slow')
    $('#fecha_to_hide_vencido').hide('slow')
  else
    $('#fecha_to_hide_extinto').hide('slow')
    if (parseInt(e) <= (d.getFullYear() - 10))
      if (parseInt(e) == (d.getFullYear() - 10))
        if (parseInt(m) <= (d.getMonth() + 1))
          if (parseInt(m) == (d.getMonth() + 1))
            if (parseInt(de) < d.getDate())
              $('#fecha_to_hide_vencido').show('slow')
            else
              $('#fecha_to_hide_vencido').hide('slow')
            return
          else
            $('#fecha_to_hide_vencido').show('slow')
          return
        else
          $('#fecha_to_hide_vencido').hide('slow')
        return
      else
        $('#fecha_to_hide_vencido').show('slow')
      return
    else
      $('#fecha_to_hide_vencido').hide('slow')
    return
  return

