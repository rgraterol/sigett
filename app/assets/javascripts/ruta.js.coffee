jQuery(document).ready ($) ->
  #elementos ocultos en el form al entrar a nueva ruta
#  $('#boton-nueva-parada').hide()

  $('#ruta_t_id_form').bootstrapValidator
    feedbackIcons:
      valid: 'fa fa-check',
      invalid: 'fa fa-times',
      validating: 'fa fa-refresh'
    live: 'enabled'
    fields:
      'ruta[num_ruta]':
        validators:
          notEmpty:
            message: 'Es necesario un número de ruta'
      'ruta[fecha_autorizacion]':
        validators:
          regexp:
            regexp: /^\d{1,2}\/\d{1,2}\/\d{2,4}$/
            message: 'El formato de la fecha no está permitido, formatos para fecha:  dd/mm/aaaa '
      'ruta[toques_attributes][0][tipo_parada]':
        validators:
          notEmpty:
            message: 'Es necesario seleccionar tipo de parada'
      'estado_id[nombre]':
        validators:
          notEmpty:
            message: 'Es necesario seleccionar el estado de ubicación de la próxima parada'
      'ruta[toques_attributes][0][parada_id]':
        validators:
          notEmpty:
            message: 'Es necesario seleccionar la ubicación de la parada'

  $("#ruta_num_ruta").inputmask 'Regex', regex: '[1-9]{4}'

  $('#ruta_taxi_form').bootstrapValidator
    feedbackIcons:
      valid: 'fa fa-check',
      invalid: 'fa fa-times',
      validating: 'fa fa-refresh'
    live: 'enabled'
    fields:
      'ruta[num_ruta]':
        validators:
          notEmpty:
            message: 'Es necesario un número de ruta'
      'ruta[toques_attributes][0][parada_attributes][estado_id]':
        validators:
          notEmpty:
            message: 'Es necesario seleccionar el estado de ubicación para la ruta'
      'parada[municipio_id]':
        validators:
          notEmpty:
            message: 'Es necesario seleccionar el municipio de ubicación para la ruta'
      'ruta[toques_attributes][0][parada_attributes][ciudad_id]':
        validators:
          notEmpty:
            message: 'Es necesario seleccionar la ciudad de ubicación para la ruta'
      'ruta[toques_attributes][0][parada_attributes][parroquia_v_id]':
        validators:
          notEmpty:
            message: 'Es necesario seleccionar la parroquia de ubicación para la ruta'
      'ruta[toques_attributes][0][parada_attributes][nombre]':
        validators:
          notEmpty:
            message: 'Es necesario indicar la ubicación de la ruta'



  $('#ruta_toques_attributes_0_ciudad_id').change ->
    $('#ruta_toques_attributes_0_parada_attributes_ciudad_id').val($('#ruta_toques_attributes_0_ciudad_id').val())

  $('#parroquia-taxi-id').change ->
    $('#ruta_toques_attributes_0_parada_attributes_parroquia_v_id').val($('#parroquia-taxi-id').val())

#  $('#tipo-terminal').change ->
#    console.log 'cambios'
#    if $('#tipo-terminal').val() != ''
#      $('#select-estado-parada').show()
#    else
#      $('#select-estado-parada').hide()
#      $('#ruta-estado-id').val('')
#      $('#parada-ruta-id').val('')

  $('#ruta-estado-id').change ->
    $.ajax
      type: "POST"
      url: "/dynamic_select/paradas/estado"
      dataType: "JSON"
      data:
        estado_id: $('#ruta-estado-id').val()
        tipo_parada: $('input[type="radio"]:checked').val()
      beforeSend: ->
        $('#carga-parada-toque').html('<i class="fa fa-refresh fa-spin"></i>')
      success: (data) ->
        html_parada_select = $('#parada-ruta-id')
        html_parada_select.html('')
        $.each data, (i, data_each) ->
          html_parada_select.append new Option(data_each.label, data_each.id)
          return
        $('#carga-parada-toque').html('')
        return

    $('#link-nueva-ruta').attr('href',Routes.ruta_flota_nueva_parada_path() + '?estado_id=' + $('#ruta-estado-id').val() + '&tipo=' + $('input[type="radio"]:checked').val())
    return
#      html_ciudad_select = $('#operadora_transporte_p_ciudad_id')
#      html_municipio_select = $('#municipio_id_nombre')
#      html_ciudad_select.empty()
#      html_municipio_select.empty()

#      $.each data[0], (i, data_each) ->
#        html_municipio_select.append new Option(data_each.label, data_each.id)


#  $('#estado_id_nombre').change ->
#    console.log 'cambio estado'
#    tt = $('#ruta_toques_attributes_0_tipo').val()
#    console.log tt
#    if $('#ruta_toques_attributes_0_parada_id').children().size() <= 1
#      $('#add-ubicacion-target').attr('class','hide')
#  $("form input:radio").change ->
#    console.log $(this).val()
#    return
#  return
$(document).on 'change', '#estado-ruta-parada-id', ->
  #cargar municipio
  estado_id = $('#estado-ruta-parada-id').val()
  select_elem_municipio = $('#municipio-ruta-parada-id')
  loader_mun = $('carga-municipio-ruta-parada')
  url_municipio = Routes.localizacion_select_estado_municipios_path()
  carga_select_localizacion(estado_id, select_elem_municipio, url_municipio, loader_mun)
  #cargar ciudad
  select_elem_ciudad = $('#ciudad-ruta-parada-id')
  loader_ciud = $('#carga-ciudad-ruta-parada')
  url_ciudad = Routes.localizacion_select_estado_ciudades_path()
  carga_select_localizacion(estado_id, select_elem_ciudad, url_ciudad, loader_ciud)

$(document).on 'change', '#municipio-ruta-parada-id', ->
  #Cargar parroquias
  municipio_id = $('#municipio-ruta-parada-id').val()
  select_elem_parroquia = $('#parroquia-nueva-parada-id')
  loader_mun = $('#carga-parroquia-ruta-parada')
  url_parroquia = Routes.localizacion_select_municipio_parroquias_path()
  carga_select_localizacion(municipio_id, select_elem_parroquia, url_parroquia, loader_mun)


$(document).on 'change', 'form#ruta_t_id_form input:radio', ->
  ed = $('#edicion-form').val()
  console.log 'cambio'
  console.log ed
  if(ed == 'edicion')
    console.log 'solo edicion'
    $.ajax
      type: "POST"
      url: "/dynamic_select/paradas/estado"
      dataType: "JSON"
      data:
        estado_id: $('#ruta-estado-id').val()
        tipo_parada: $('input[type="radio"]:checked').val()
      beforeSend: ->
        $('#carga-parada-toque').html('<i class="fa fa-refresh fa-spin"></i>')
      success: (data) ->
        html_parada_select = $('#parada-ruta-id')
        html_parada_select.html('')
        $.each data, (i, data_each) ->
          html_parada_select.append new Option(data_each.label, data_each.id)
          return
        $('#carga-parada-toque').html('')
        return

    $('#link-nueva-ruta').attr('href',Routes.ruta_flota_nueva_parada_path() + '?estado_id=' + $('#ruta-estado-id').val() + '&tipo=' + $('input[type="radio"]:checked').val())
  else
    console.log 'solo nuevo'
    $('#link-nueva-ruta').attr('href',Routes.ruta_flota_nueva_parada_path() + '?estado_id=' + $('#ruta-estado-id').val() + '&tipo=' + $('input[type="radio"]:checked').val())
    val = $('input[type="radio"]:checked').val()
    if val == 'terminal_privado' || val == 'terminal_publico'
      $('#ruta-estado-id').val('')
      $('#boton-nueva-parada').hide()
      $('#grupo-municipio').hide()
      $('#parada-ruta-id').html('').append new Option('Seleccione una Parada', '')
    else if val == 'zona'
      $('#ruta-estado-id').val('')
      $('#boton-nueva-parada').show()
      $('#parada-ruta-id').html('').append new Option('Seleccione una Parada', '')
  return

carga_select_localizacion = (elemFiltro, elemHtml, ruta, loader) ->
  $.ajax
    type: "POST"
    url: ruta
    dataType: "JSON"
    beforeSend: (xhr) ->
      loader.html('<i class="fa fa-refresh fa-spin"></i>')
      xhr.setRequestHeader 'X-CSRF-Token', $('meta[name="csrf-token"]').attr('content')
      return
    data:
      filtro_id: elemFiltro
    success: (data) ->
      elemHtml.empty()
      elemHtml.append new Option('Seleccione...', '')
      $.each data, (i, data_each) ->
        elemHtml.append new Option(data_each.nombre, data_each.id)
        return
      loader.html('')
      return

