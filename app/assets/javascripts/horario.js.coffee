# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
jQuery(document).ready ($) ->
  $('#horarios_t_id_form').bootstrapValidator
    feedbackIcons:
      valid: 'fa fa-check',
      invalid: 'fa fa-times',
      validating: 'fa fa-refresh'
    live: 'enabled'
    fields:
      'horario[dia]':
        validators:
          notEmpty:
            message: 'Es necesario seleccionar una opción en día'
      'horario[tipo]':
        validators:
          notEmpty:
            message: 'Es necesario seleccionar una opción en tipo de horario'
