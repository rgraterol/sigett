jQuery(document).ready ($) ->
  $('#operadora_t_id_form').bootstrapValidator
    feedbackIcons:
      valid: 'fa fa-check',
      invalid: 'fa fa-times',
      validating: 'fa fa-refresh'
    live: 'enabled'
    fields:
      'operadora_transporte_p[sociedad_mercantil_id]':
        validators:
          notEmpty:
            message: 'Es necesario seleccionar una opción en tipo de sociedad mercantil'
      'operadora_transporte_p[municipio_id]':
        validators:
          notEmpty:
            message: 'Es necesario seleccionar un municipio'
      'operadora_transporte_p[direccion_fiscal]':
        validators:
          notEmpty:
            message: 'Es necesario escribir una Dirección'
      'estado_id[nombre]':
        validators:
          notEmpty:
            message: 'Es necesario seleccionar el estado de ubicación'
      'operadora_transporte_p[ciudad_id]':
        validators:
          notEmpty:
            message: 'Es necesario seleccionar la ciudad'
      'municipio_id[nombre]':
        validators:
          notEmpty:
            message: 'Es necesario seleccionar el municipio'
      'operadora_transporte_p[parroquia_v_id]':
        validators:
          notEmpty:
            message: 'Es necesario seleccionar la parroquia'
      'operadora_transporte_p[razon_social]':
        validators:
          notEmpty:
            message: 'Es necesario escribir la razón social'
          regexp:
            regexp: /^([a-zA-ZñÑáéíóúÁÉÍÓÚäëïöüÄËÏÖÜßÇç´0-9._\-\s+][a-zA-ZñÑáéíóúÁÉÍÓÚßÇç´0-9._\-\s+]{1,60})$/
            message: 'La Razón social debe tener de 2 a 60 carácteres y no puede contener carácteres especiales.'
      'operadora_transporte_p[telefono]':
        validators:
          regexp:
            regexp: /^([\x28]\d{3}[\x29][-]\d{7})$/
            message: 'Teléfono no válido, debe ingresar un teléfono válido: Ej. (9999)-9999999'
      'operadora_transporte_p[movil]':
        validators:
          regexp:
            regexp: /^\d{7}$/
            message: 'Teléfono celular no válido, debe ingresar un teléfono ' +
              'válido: Ej.operadora + 9999999'
      'operadora_transporte_p[fax]':
        validators:
          regexp:
            regexp: /^([\x28]\d{3}[\x29][-]\d{7})$/
            message: 'Teléfono fax no válido, debe ingresar un fax válido: Ej. (9999)-9999999'
      'operadora_transporte_p[rif]':
        validators:
          notEmpty:
            message: 'Es necesario escribir un RIF'
          regexp:
            regexp: /^([VEJPGvejpg][-]\d{9})$/
            message: 'RIF no válido, debe ingresar un RIF válido: Ej. J-999999999'
      'operadora_transporte_p[email]':
        validators:
          notEmpty:
            message: 'Debe introducir un Email'
          regexp:
            regexp: /^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,3})$/
            message: 'Email no válido, debe ingresar un Email válido'
      'operadora_transporte_p[piso]':
        group: '.group',
        validators:
          stringLength:
            max: 10
            message: 'Piso debe tener un maximo de 10 caracteres'
      'operadora_transporte_p[observacion]':
        validators:
          stringLength:
            max: 60
            message: 'Observacion debe tener un maximo de 60 caracteres'
      'operadora_transporte_p[avenida]':
        group: '.group',
        validators:
          stringLength:
            max: 60
            message: 'Avenida debe tener un maximo de 60 caracteres'
      'operadora_transporte_p[calle]':
        group: '.group',
        validators:
          stringLength:
            max: 60
            message: 'Calle debe tener un maximo de 60 caracteres'
      'operadora_transporte_p[urbanizacion]':
        group: '.group',
        validators:
          stringLength:
            max: 60
            message: 'Sector/Urbanización debe tener un maximo de 60 caracteres'
      'operadora_transporte_p[edificio_cc]':
        group: '.group',
        validators:
          stringLength:
            max: 60
            message: 'Edificio debe tener un maximo de 60 caracteres'
      'operadora_transporte_p[local]':
        group: '.group',
        validators:
          stringLength:
            max: 60
            message: 'Local debe tener un maximo de 60 caracteres'


#      'operadora_transporte_p[avenida]':
#        validators:
#          regexp:
#            regexp: /^([a-zA-ZñÑáéíóúÁÉÍÓÚäëïöüÄËÏÖÜßÇç´0-9\s+][a-zA-ZñÑáéíóúÁÉÍÓÚßÇç´0-9\s+]{0,60})$/
#            message: 'Avenida debe tener menos de 60 caracteres'
#      'operadora_transporte_p[calle]':
#        validators:
#          regexp:
#            regexp: /^([a-zA-ZñÑáéíóúÁÉÍÓÚäëïöüÄËÏÖÜßÇç´0-9\s+][a-zA-ZñÑáéíóúÁÉÍÓÚßÇç´0-9\s+]{0,59})$/
#            message: 'Calle debe tener menos de 60 caracteres'
#      'operadora_transporte_p[urbanizacion]':
#        validators:
#          regexp:
#            regexp: /^([a-zA-ZñÑáéíóúÁÉÍÓÚäëïöüÄËÏÖÜßÇç´0-9\s+][a-zA-ZñÑáéíóúÁÉÍÓÚßÇç´0-9\s+]{0,59})$/
#            message: 'Urbanización debe tener menos de 60 caracteres'
#      'operadora_transporte_p[edificio_cc]':
#        validators:
#          regexp:
#            regexp: /^([a-zA-ZñÑáéíóúÁÉÍÓÚäëïöüÄËÏÖÜßÇç´0-9\s+][a-zA-ZñÑáéíóúÁÉÍÓÚßÇç´0-9\s+]{0,59})$/
#            message: 'Casa/Edificio debe tener menos de 60 caracteres'
#      'operadora_transporte_p[local]':
#        validators:
#          regexp:
#            regexp: /^([a-zA-ZñÑáéíóúÁÉÍÓÚäëïöüÄËÏÖÜßÇç´0-9\s+][a-zA-ZñÑáéíóúÁÉÍÓÚßÇç´0-9\s+]{0,59})$/
#            message: 'La Urbanización debe tener menos de 60 caracteres'
##
#  $('#rif_imagen').tooltip
#    placement: 'left',
#    title: 'Pasos:
#             1. Si la imágen se encuentra en su equipo, haz click en el botón "Seleccionar Imágen" y explora tu equipo para seleccionar la imagen a subir.
#             2. Haz click en Aceptar y automaticamente el sistema agregará la imagén.
#             3. Si desea modificar la imágen, vuelva a hacer click en el botón "Selecciona Imágen" y repitá el paso 1.'
#
#  #validaciones
  $("#operadora_transporte_p_telefono").inputmask "(999)-9999999",
    placeholder: ""

  $("#operadora_transporte_p_movil").inputmask "9999999",
    placeholder: ""

  $("#operadora_transporte_p_fax").inputmask "(999)-9999999",
    placeholder: ""

  $("#operadora_transporte_p_rif").inputmask "Regex", {
    regex: "[VEJPGvejpg][-][0-9]{9}"
  }


  $('#estado_id_operadora').change ->
    $.ajax
      type: "POST"
      url: "/operadora/ciudad_municipio"
      dataType: "JSON"
      data:
        estado_id: $('#estado_id_operadora').val()
      success: (data) ->
        html_ciudad_select = $('#operadora_transporte_p_ciudad_id')
        html_municipio_select = $('#municipio_id_nombre')
        html_ciudad_select.empty()
        html_municipio_select.empty()
        $.each data[1], (i, data_each) ->
          html_ciudad_select.append new Option(data_each.label, data_each.id)
        $.each data[0], (i, data_each) ->
          html_municipio_select.append new Option(data_each.label, data_each.id)


  $("#rif_imagen").popover
    trigger: "hover"
    html: true

    # titulo del popover
    title: ->
      $(this).parent().find(".po-title").html()

    content: ->
      $(this).parent().find(".po-body").html()

    container: "body"
    placement: "bottom"

  #esto es para que salga el input de sociedad mercantil
  $('#operadora_transporte_p_sociedad_mercantil_id').change ->
    valor = $('#operadora_transporte_p_sociedad_mercantil_id option:selected').html()
    if valor == 'Otros'
      $('#sociedad').show()
      $('#operadora_t_id_form').bootstrapValidator 'addField', 'operadora_transporte_p[sociedad_mercantil_otros]',
        validators:
          notEmpty:
            message: 'Es necesario indicar la informacion de la sociedad mercantil'
    else
      $('#formCreate').bootstrapValidator('removeField', 'operadora_transporte_p[sociedad_mercantil_otros]')
      $('#sociedad').hide()
#    return

  $('#showPdfModal').on 'show.bs.modal', (event) ->
    button = $(event.relatedTarget)
    recipient = button.data('document')
    modal = $(this)
    modal.find('.modal-body object').attr('data', recipient)
  #  return
  #    return
