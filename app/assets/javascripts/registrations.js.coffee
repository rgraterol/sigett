$('#registro-usuario-form').bootstrapValidator
  feedbackIcons:
    valid: 'fa fa-check',
    invalid: 'fa fa-times',
    validating: 'fa fa-refresh'
  live: 'enabled'
  fields:
    'session_user_usuario_sput_attributes_usuario_attributes_rif':
      validators:
        notEmpty:
          message: 'Es necesario el número del rif'
    'session_user_email':
      validators:
        notEmpty:
          message: 'Es necesario el correo electrónico'
        regexp:
          regexp: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/
          message: 'El formato del email no está permitido, formatos para email: ejemplo@ejemplo.com '
    'session_user_email_confirmation':
      validators:
        notEmpty:
          message: 'Es necesario confirmar el correo electrónico'
    'session_user_usuario_sput_attributes_movil':
          validators:
            notEmpty:
              message: 'Es necesario el teléfono móvil'
    'session_user[usuario_sput_attributes][usuario_attributes][direccion]':
      validators:
        notEmpty:
          message: 'Es necesario la dirección'
