jQuery(document).ready ($) ->

  $('#buscar_vehiculo').click ->
    $('#div_buscar_vehiculo').empty()
    $('#loading_buscar_vehiculo').show()
    $.ajax
      type: "POST"
      url: "/vehiculos/buscar_vehiculo"
      dataType: "html"
      data:
        placa: $('#placa_vehiculo').val()
        s_carroceria: $('#s_carroceria_vehiculo').val()
        flota_id: $('#flota_vehiculos_hi').val()
      success: (data) ->
        html = $('#div_buscar_vehiculo')
        html.empty()
        html.append(data)
      complete: ->
        $('#loading_buscar_vehiculo').hide()



  $('#posee_arrendatario').click ->
    if $('#posee_arrendatario').is(':checked')
      $('#contenedor-arrendatario').show()
      $('#contenedor-arrendatario :input').prop 'disabled', false
      $('#contenedor-arrendatario').find('select').prop 'disabled', false
    else
      $('#contenedor-arrendatario').hide()
      $('#contenedor-arrendatario :input').prop 'disabled', true
      $('#contenedor-arrendatario').find('select').prop 'disabled', true
    return
  $('#contrato_arrendamiento_rif_arrendatario').inputmask 'Regex', regex: '[VEJPGvejpg][-][0-9]{9}'

  $('#contrato_arrendamiento_duracion').inputmask 'Regex', regex: '[1-9]{1}'

  $('#contrato_arrendamiento_tomo').inputmask 'Regex', regex: '[0-9]{3}'

  $('#contrato_arrendamiento_folio').inputmask 'Regex', regex: '[0-9]{3}'

  $('#edit_vehiculo').bootstrapValidator
    feedbackIcons:
      valid: 'fa fa-check'
      invalid: 'fa fa-times'
      validating: 'fa fa-refresh'
    live: 'enabled'
    fields:
      'vehiculo[seguro_vehiculo][num_poliza]':
        validators: notEmpty: message: 'Debe indicar el número de póliza.'
      'vehiculo[seguro_vehiculo][aseguradora_id]':
        validators: notEmpty: message: 'Debe seleccionar una compañia aseguradora.'
      'contrato_arrendamiento[dracion]':
        validators:
          notEmpty:
            message: 'Debe indicar los años de duración del contrato'
      'contrato_arrendamiento[tomo]':
        group: '.group'
        validators:
          notEmpty:
            message: 'Debe indicar el numero de Tomo'
          regexp:
            regexp: /^\d*([1-9]\d*)$/
            message: 'No puede ser solo 0'
      'contrato_arrendamiento[folio]':
        group: '.group'
        validators:
          notEmpty:
            message: 'Debe indicar el numero de Folio'
          regexp:
            regexp: /^\d*([1-9]\d*)$/
            message: 'No puede ser solo 0'
      'contrato_arrendamiento[notaria]': validators: notEmpty: message: 'Debe indicar el numero la Notaria'
      'contrato_arrendamiento[nombre_arrendatario]':
        group: '.group'
        validators: notEmpty: message: 'Debe indicar el Nombre y Apellido/Razón Social'
      'contrato_arrendamiento[rif_arrendatario]':
        group: '.group'
        validators:
          notEmpty: message: 'Debe indicar la C.I/RIF'

