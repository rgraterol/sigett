# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

jQuery(document).ready ($) ->
  $('#estadistica-igual-parada').bootstrapValidator
    feedbackIcons:
      valid: 'fa fa-check',
      invalid: 'fa fa-times',
      validating: 'fa fa-refresh'
    live: 'enabled'
    fields:
      'value[id]':
        validators:
          notEmpty:
            message: 'Es necesario seleccionar un estado.'
      'value[tipo_parada]':
        validators:
          notEmpty:
            message: 'Es necesario seleccionar el tipo de parada.'
      'parada_igual':
        validators:
          notEmpty:
            message: 'Es necesario seleccionar una parada.'

  $('#estado_to_stad').change ->
    $.ajax
      type: "POST"
      url: "/operadora/ciudad_municipio"
      dataType: "JSON"
      data:
        estado_id: $(this).val()
      success: (data) ->
        html_municipio_select = $('#municipio')
        html_municipio_select.empty()
        $.each data[0], (i, data_each) ->
          html_municipio_select.append new Option(data_each.label, data_each.id)


  $('#tipo_parada_igual').change ->
    $.ajax
      type: 'POST'
      url: "/dynamic_select/form_igual_parada/estado"
      dataType: "JSON"
      data:
        estado_id: $('#estado_igual_parada').val()
        toque_tipo: $(this).val()
      success: (data) ->
        paradas = $('#parada_igual')
        paradas.html('')
        cargar_parada_select(data,paradas)
        return


cargar_parada_select = (data,select) ->
  $.each data, (i, data_each) ->
    select.append new Option(data_each.nombre, data_each.id)
    return

#
##Flot Bar Chart
#$ ->
#  var_a = $('.data-to-get')
#  barOptions =
#    series:
#      bars:
#        show: true
#        barWidth: 0.6
#        fill: true
#        fillColor:
#          colors: [
#            {
#              opacity: 0.8
#            }
#            {
#              opacity: 0.8
#            }
#          ]
#
#    xaxis:
#      tickDecimals: 0
#
#    colors: ["#245F93"]
#    grid:
#      color: "#999999"
#      hoverable: true
#      clickable: true
#      tickColor: "#D4D4D4"
#      borderWidth: 0
#
#    legend:
#      show: false
#
#    tooltip: true
#    tooltipOpts:
#      content: "x: %x, y: %y"
##  i = 0
##  m = []
##  console.log(var_a.size())
##  console.log(var_a[0])
##  while i < var_a.size()
##    console.log(i)
##    console.log(var_a[i])
##    console.log($(var_a[i]))
##
##    m[i] =
##      [
##        (i + 1)
##        parseInt($(var_a[i]).attr("data-to-get"))
##      ]
##    i++
#  barData =
#    label: "bar"
#    data: [
#          [
#            1
#            16
#          ]
#          [
#            2
#            25
#          ]
#          [
#            3
#            19
#          ]
#          [
#            4
#            34
#          ]
#          [
#            5
#            32
#          ]
#          [
#            6
#            44
#          ]
#          [
#            7
#            44
#          ]
#          [
#            8
#            44
#          ]
#          [
#            9
#            98
#          ]
#          ]
#
#  $.plot $("#flot-bar-chart"), [barData], barOptions
#  return
