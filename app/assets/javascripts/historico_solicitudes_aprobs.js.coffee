# require bootstrap-datepicker/core
# require bootstrap-datepicker/locales/bootstrap-datepicker.es

jQuery(document).ready ($) ->
#  $('.input-group.fecha_expedicion.date').datepicker({
#    keyboardNavigation: false
#    forceParse: false
#    calendarWeeks: true
#    format: 'dd/mm/yyyy'
#    language: 'es'
#    clearBtn: true
#    autoclose: true
#  })

#  $('select[data-dynamic-selectable-url][data-dynamic-selectable-target]').dynamicSelectable()
  $('#historico_t_id_form').bootstrapValidator
    feedbackIcons:
      valid: 'fa fa-check',
      invalid: 'fa fa-times',
      validating: 'fa fa-refresh'
    live: 'enabled'
    fields:
      'historico_solicitudes_aprob[tipo_certificacion]':
        validators:
          notEmpty:
            message: 'Es necesario seleccionar una opción en tipo de certificación'
      'historico_solicitudes_aprob[fecha_expedicion]':
        validators:
          notEmpty:
            message: 'Debe seleccionar una fecha'
          regexp:
            regexp: /^\d{1,2}\/\d{1,2}\/\d{2,4}$/
            message: 'El formato de la fecha no está permitido, formatos para fecha:  dd/mm/aaaa '

#      'historico_solicitudes_aprob[documento_historico]':
#        validators:
#          notEmpty:
#            message: 'Debe introducir un archivo u imágen'
#          regexp:
#            regexp: /.(gif|jpeg|jpg|png|pdf)$/
#            message: 'El formato del archivo subido no está permitido, formatos para imágen permitdos:  .jpg, .jpeg, .gif, .png, .pdf '
      'historico_solicitudes_aprob[descripcion]':
        validators:
          notEmpty:
            message: 'Es necesario una breve descripción'
          stringLength:
            max: 60
            message: 'Descripción debe tener un maximo de 60 caracteres'

  $("#historico_solicitudes_aprob_fecha_expedicion").inputmask "99/99/9999",
    clearIncomplete: true
#
#  $("#historico_solicitudes_aprob_descripcion").inputmask "Regex",
#    {regex: "([\x0-\x7F]){1,60}"}
#    placeholder: ""
#    regexp:
#      regexp: /^([[:ascii:]]){1,60}$/

  $('#link-show-historico-list').click ->
    $('#button-index-historico').hide('slow')
    $('#content-index-historico').show('slow')