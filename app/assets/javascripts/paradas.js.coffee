jQuery(document).ready ($) ->
  #CODIGO JUAN GOMEZ *******************************************************************************
  #cargo las primeras paradas al entrar al formulario
  if ($('#toque-modalidad').val() == 'sub_urb_p_puesto' || $('#toque-modalidad').val() == 'sub_urb_colectivo')
    cargar_paradas()

#  $('#toque_t_id_form').bootstrapValidator
#    feedbackIcons:
#      valid: 'fa fa-check',
#      invalid: 'fa fa-times',
#      validating: 'fa fa-refresh'
#    live: 'enabled'
#    fields:
#      'toque[tipo_parada]':
#        validators:
#          notEmpty:
#            message: 'Es necesario seleccionar tipo de parada'
#      'toque[parada_id]':
#        validators:
#          notEmpty:
#            message: 'Es necesario seleccionar la descripción de la parada'
#      'toque[tipo_parada]':
#        validators:
#          notEmpty:
#            message: 'Es necesario seleccionar tipo de parada'
#      'toque[tipo]':
#        validators:
#          notEmpty:
#            message: 'Es necesario seleccionar tipo de terminal'

    $('#estado-adm-id').change ->
      $('#parada-adm-estado-id').val($('#estado-adm-id').val())
      $.ajax
        type: "POST"
        url: "/operadora/ciudad_municipio"
        dataType: "JSON"
        data:
          estado_id: $('#estado-adm-id').val()
        success: (data) ->
          html_ciudad_select = $('#toque_ciudad_id')
          html_municipio_select = $('#municipio_id_nombre')
          html_ciudad_select.empty()
          html_municipio_select.empty()
          $.each data[1], (i, data_each) ->
            html_ciudad_select.append new Option(data_each.label, data_each.id)
          $.each data[0], (i, data_each) ->
            html_municipio_select.append new Option(data_each.label, data_each.id)

    $('#ubicacion_parada_t_adm').bootstrapValidator
      feedbackIcons:
        valid: 'fa fa-check',
        invalid: 'fa fa-times',
        validating: 'fa fa-refresh'
      live: 'enabled'
      fields:
        'estado_id[nombre]':
          validators:
            notEmpty:
              message: 'Es necesario seleccionar el estado de ubicación'
        'parada[ciudad_id]':
          validators:
            notEmpty:
              message: 'Es necesario seleccionar la ciudad'
        'municipio_id[nombre]':
          validators:
            notEmpty:
              message: 'Es necesario seleccionar el municipio'
        'parada[parroquia_v_id]':
          validators:
            notEmpty:
              message: 'Es necesario seleccionar la parroquia'
        'parada[tipo_parada]':
          validators:
            notEmpty:
              message: 'Es necesario el tipo de parada'
        'parada[nombre]':
          validators:
            notEmpty:
              message: 'Es necesario el nombre de la parada'

    $('#toque-estado-id').change ->
      $.ajax
        type: "POST"
        url: "/dynamic_select/paradas/estado"
        dataType: "JSON"
        data:
          estado_id: $('#toque-estado-id').val()
          tipo_parada: $('input[type="radio"]:checked').val()
        beforeSend: ->
          $('#carga-parada-toque').html('<i class="fa fa-refresh fa-spin"></i>')
        success: (data) ->
          html_parada_select = $('#parada-toque-id')
          html_parada_select.html('')
          $.each data, (i, data_each) ->
            html_parada_select.append new Option(data_each.label, data_each.id)
            return
          $('#carga-parada-toque').html('')
          return

      $('#link-nueva-ruta').attr('href',Routes.paradas_toque_nueva_parada_path() + '?estado_id=' + $('#toque-estado-id').val() + '&tipo=' + $('input[type="radio"]:checked').val()+ '&ruta_id=' + $('#ruta-origen-id').val())
      return




#  $('#ubicacion_parada_t_id_form').bootstrapValidator
#    feedbackIcons:
#      valid: 'fa fa-check',
#      invalid: 'fa fa-times',
#      validating: 'fa fa-refresh'
#    live: 'enabled'
#    fields:
#      'estado_id[nombre]':
#        validators:
#          notEmpty:
#            message: 'Es necesario seleccionar el estado de ubicación de la próxima parada'
#      'parada[ciudad_id]':
#        validators:
#          notEmpty:
#            message: 'Es necesario seleccionar la ciudad'
#      'municipio_id[nombre]':
#        validators:
#          notEmpty:
#            message: 'Es necesario seleccionar el municipio'
#      'parada[parroquia_v_id]':
#        validators:
#          notEmpty:
#            message: 'Es necesario seleccionar la parroquia'
#      'parada[nombre]':
#        validators:
#          notEmpty:
#            message: 'Es necesario el nombre de la parada'
#          regexp:
#            regexp: /^([a-zA-ZñÑáéíóúÁÉÍÓÚäëïöüÄËÏÖÜßÇç´0-9\s+][a-zA-ZñÑáéíóúÁÉÍÓÚßÇç´0-9\s+]{0,59})$/
#            message: 'La Razón social debe tener menos de 60 caracteres'
#      'parada[avenida]':
#        validators:
#          regexp:
#            regexp: /^([a-zA-ZñÑáéíóúÁÉÍÓÚäëïöüÄËÏÖÜßÇç´0-9\s+][a-zA-ZñÑáéíóúÁÉÍÓÚßÇç´0-9\s+]{0,29})$/
#            message: 'La Razón social debe tener menos de 60 caracteres'
#      'parada[calle]':
#        validators:
#          regexp:
#            regexp: /^([a-zA-ZñÑáéíóúÁÉÍÓÚäëïöüÄËÏÖÜßÇç´0-9\s+][a-zA-ZñÑáéíóúÁÉÍÓÚßÇç´0-9\s+]{0,29})$/
#            message: 'La Razón social debe tener menos de 60 caracteres'
#      'parada[sector]':
#        validators:
#          regexp:
#            regexp: /^([a-zA-ZñÑáéíóúÁÉÍÓÚäëïöüÄËÏÖÜßÇç´0-9\s+][a-zA-ZñÑáéíóúÁÉÍÓÚßÇç´0-9\s+]{0,29})$/
#            message: 'La Razón social debe tener menos de 60 caracteres'








#  $('#estado_id_nombre').change ->
#    $('#body').html('')
#    $.ajax
#      type: "POST"
#      url: "/dynamic_select/paradas/estado"
#      dataType: "JSON"
#      data:
#        estado_id: $('#estado_id_nombre').val()
#        tipo_parada: $('#toque_tipo').val()
#      success: (data) ->
#        html_parada_select = $('#toque_parada_id')
#        html_parada_select.html('')
#        $.each data, (i, data_each) ->
#          html_parada_select.append new Option(data_each.label, data_each.id)
#          return
#        return

#  $('#parroquia_v_id_nombre').change ->
#    $.ajax
#      type: "POST"
#      url: "/dynamic_select/paradas"
#      dataType: "JSON"
#      data:
#        toque_tipo: $('#ruta_toques_attributes_0_tipo').val()
#        parroquia_id: $('#parroquia_v_id_nombre').val()
#      success: (data) ->
#        html_parada_select = $('#ruta_toques_attributes_0_parada_id')
#        html_parada_select.empty()
#        console.log(data)
#        $.each data, (i, data_each) ->
#          html_parada_select.append new Option(data_each.label, data_each.id)
#
#  $('#el_id_de_parroquia_chamo_ay').change ->
#    $.ajax
#      type: "POST"
#      url: "/dynamic_select/paradas"
#      dataType: "JSON"
#      data:
#        toque_tipo: $('#toque_tipo').val()
#        parroquia_id: $('#el_id_de_parroquia_chamo_ay').val()
#      success: (data) ->
#        html_parada_select = $('#toque_parada_id')
#        html_parada_select.empty()
#        $.each data, (i, data_each) ->
#          html_parada_select.append new Option(data_each.label, data_each.id)
#
#  $('#el_id_admin_parada_form_post').change ->
#    $.ajax
#      type: "POST"
#      url: "/dynamic_select/paradas"
#      dataType: "JSON"
#      data:
#        toque_tipo: $('#toque_tipo').val()
#        parroquia_id: $('#el_id_de_parroquia_chamo_ay').val()
#      success: (data) ->
#        html_parada_select = $('#toque_parada_id')
#        html_parada_select.empty()
#        $.each data, (i, data_each) ->
#          html_parada_select.append new Option(data_each.label, data_each.id)
#
#  $('#otro_id_form_ruta').change ->
#    $.ajax
#      type: "POST"
#      url: "/dynamic_select/paradas"
#      dataType: "JSON"
#      data:
#        toque_tipo: $('#ruta_toques_attributes_0_tipo').val()
#        parroquia_id: $('#otro_id_form_ruta').val()
#      success: (data) ->
#        html_parada_select = $('#ruta_toques_attributes_0_parada_id')
#        html_parada_select.empty()
#        $.each data, (i, data_each) ->
#          html_parada_select.append new Option(data_each.label, data_each.id)
#
#  $('#parada_municipio_id').change ->
#    if $(this).val() == ''
#      $('#body').html('')
#    else
#      $.ajax
#        type: "POST"
#        url: "/paradas/buscar/ubicacion/parada"
#        dataType: "html"
#        data:
#          municipio_id: $("#parada_municipio_id").val()
#        success: (data) ->
#          $('#body').html('').append(data)
#          return

  $('#to_save_on_cookie_b_id').click (e) ->
#    $(this).attr('disabled',true)
    if $('#ruta_toques_attributes_0_tipo_parada').size() == 0
      tipo_parada_ = $('#toque_tipo_parada').val()
      ciudad = $('#toque_ciudad_id').val()
      parroquia = $('#el_id_de_parroquia_chamo_ay').val()
    else
      ciudad = $('#ruta_toques_attributes_0_ciudad_id').val()
      tipo_parada_ = $('#ruta_toques_attributes_0_tipo_parada').val()
      parroquia = $('#otro_id_form_ruta').val()
    if $('#ruta_toques_attributes_0_tipo').size() == 0
      tipo_ = $('#toque_tipo').val()
    else
      tipo_ = $('#ruta_toques_attributes_0_tipo').val()
    e.preventDefault()
    $.ajax
      type: "POST"
      url: "/paradas/cookie/constructor/"
      dataType: "JSON"
      data:
        estado_id: $('#estado_id_nombre').val()
        municipio_id: $("#municipio_id_nombre").val()
        ciudad_id: ciudad
        parroquia_v_id: parroquia
        num_ruta: $('#ruta_num_ruta').val()
        ida_vuelta: $('#ruta_ida_vuelta_true').val()
        fecha_auto_dia: $('#ruta_fecha_autorizacion_3i').val()
        fecha_auto_mes: $('#ruta_fecha_autorizacion_2i').val()
        fecha_auto_ano: $('#ruta_fecha_autorizacion_1i').val()
        tipo_parada: tipo_parada_
        tipo: tipo_
        id_cookie: $('#id_to_cookie').val()
        create_band: $('#create_band').val()
      success: (data) ->
        estado_id = readCookie("estado_id")
        id_cookie = readCookie("id_cookie")
        municipio_id = readCookie("municipio_id")
        parroquia_id = readCookie("parroquia_v_id")
        ciudad_id = readCookie("ciudad_id")
        create_band = readCookie("create_band")
        tipo = readCookie("tipo")
        document.location.href = '/paradas/new/ubicacion/parada/?id='+ id_cookie + '&create=' + create_band + '&estado=' + estado_id + '&municipio=' + municipio_id + '&parroquia=' + parroquia_id + '&ciudad=' + ciudad_id + '&tipo=' + tipo
        return

readCookie = (name) ->
  nameEQ = name + "="
  cookie = document.cookie.split(";")
  i = 0
  while i < cookie.length
    c = cookie[i]
    c = c.substring(1, c.length)  while c.charAt(0) is " "
    return c.substring(nameEQ.length, c.length)  if c.indexOf(nameEQ) is 0
    i++
  null



#CODIGO JUAN GOMEZ ************************************************************************************
$(document).on 'change', 'form#toque_t_id_form input:radio', ->
  est = $('#toque-estado-id').val()
  unless(!est)
    cargar_paradas()
    $('#link-nueva-ruta').attr('href',Routes.paradas_toque_nueva_parada_path() + '?estado_id=' + est + '&tipo=' + $('input[type="radio"]:checked').val()+ '&ruta_id=' + $('#ruta-origen-id').val())
    val = $('input[type="radio"]:checked').val()
    mod = $('#toque-modalidad').val()
    if val == 'terminal_privado' || val == 'terminal_publico'
      if !(mod == 'sub_urb_p_puesto' || mod == 'sub_urb_colectivo')
        $('#toque-estado-id').val('')
      $('#boton-nueva-parada').hide()
      $('#grupo-municipio').hide()
    else if val == 'zona'
      if !(mod == 'sub_urb_p_puesto' || mod == 'sub_urb_colectivo')
        $('#toque-estado-id').val('')
      $('#boton-nueva-parada').show()
      $('#parada-toque-id').html('').append new Option('Seleccione una Parada', '')
  return

$(document).on 'change', '#estado-toque-parada-id', ->
  #cargar municipio
  estado_id = $('#estado-toque-parada-id').val()
  select_elem_municipio = $('#municipio-toque-parada-id')
  loader_mun = $('carga-municipio-toque-parada')
  url_municipio = Routes.localizacion_select_estado_municipios_path()
  carga_select_localizacion(estado_id, select_elem_municipio, url_municipio, loader_mun)
  #cargar ciudad
  select_elem_ciudad = $('#ciudad-toque-parada-id')
  loader_ciud = $('#carga-ciudad-toque-parada')
  url_ciudad = Routes.localizacion_select_estado_ciudades_path()
  carga_select_localizacion(estado_id, select_elem_ciudad, url_ciudad, loader_ciud)

$(document).on 'change', '#municipio-toque-parada-id', ->
  #Cargar parroquias
  municipio_id = $('#municipio-toque-parada-id').val()
  select_elem_parroquia = $('#parroquia-toque-parada-id')
  loader_parr = $('#carga-parroquia-toque-parada')
  url_parroquia = Routes.localizacion_select_municipio_parroquias_path()
  carga_select_localizacion(municipio_id, select_elem_parroquia, url_parroquia, loader_parr)

cargar_paradas = ->
  $.ajax
    type: "POST"
    url: "/dynamic_select/paradas/estado"
    dataType: "JSON"
    beforeSend: (xhr) ->
      $('#carga-parada-toque').html('<i class="fa fa-refresh fa-spin"></i>')
      xhr.setRequestHeader 'X-CSRF-Token', $('meta[name="csrf-token"]').attr('content')
      return
    data:
      estado_id: $('#toque-estado-id').val()
      tipo_parada: $('input[type="radio"]:checked').val()
    success: (data) ->
      html_parada_select = $('#parada-toque-id')
      html_parada_select.html('')
      $.each data, (i, data_each) ->
        html_parada_select.append new Option(data_each.label, data_each.id)
        return
      $('#carga-parada-toque').html('')
      return
  return

carga_select_localizacion = (elemFiltro, elemHtml, ruta, loader) ->
  $.ajax
    type: "POST"
    url: ruta
    dataType: "JSON"
    beforeSend: (xhr) ->
      loader.html('<i class="fa fa-refresh fa-spin"></i>')
      xhr.setRequestHeader 'X-CSRF-Token', $('meta[name="csrf-token"]').attr('content')
      return
    data:
      filtro_id: elemFiltro
    success: (data) ->
      elemHtml.empty()
      elemHtml.append new Option('Seleccione...', '')
      $.each data, (i, data_each) ->
        elemHtml.append new Option(data_each.nombre, data_each.id)
        return
      loader.html('')
      return