# require bootstrap-datepicker/core
# require bootstrap-datepicker/locales/bootstrap-datepicker.es

jQuery(document).ready ($) ->
  $('#aseguradora_id_rif').change ->
    $.ajax
      type: "POST"
      url: "/asegurador/rif"
      dataType: "JSON"
      data:
        id: $('#aseguradora_id_rif').val()
      success: (data) ->
        $('#seguro_vehiculo_rif').val(data[0])


  $('select[data-dynamic-selectable-url][data-dynamic-selectable-target]').dynamicSelectable()
  $('#registro-seguro-form').bootstrapValidator
    feedbackIcons:
      valid: 'fa fa-check',
      invalid: 'fa fa-times',
      validating: 'fa fa-refresh'
    live: 'enabled'
    fields:
      'seguro_vehiculo[aseguradora_id]':
        validators:
          notEmpty:
            message: 'Aseguradora es Obligatorio'
      'seguro_vehiculo[num_poliza]':
        validators:
          notEmpty:
            message: 'Número de Póliza es Obligatorio'

  #logica de pantalla Seguro RCV
  $("#seguro_vehiculo_num_poliza").inputmask "Regex", {
    regex: "([a-zA-Z0-9_.#\-]{1,30})"
  }

  $("#radio-RCV-individual").click ->
    $("#btn-continuar-seguro").show()
    $(".continuar-seguro-pregunta").show()
    $("#registro-seguro-form").hide()
    $(".ibox-content.caja-RCV-flota").hide()
    return

  $("#radio-RCV-flota").click ->
    $("#btn-continuar-seguro").hide()
    $(".continuar-seguro-pregunta").hide()
    $("#registro-seguro-form").show()
    $(".ibox-content.caja-RCV-flota").show()

    return