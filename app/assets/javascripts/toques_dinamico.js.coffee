jQuery(document).ready ($) ->
  #elementos ocultos en el form al entrar a nueva ruta
  $('#boton-nueva-parada').hide();
  #validar el formulario
  $('#toque_t_id_form').bootstrapValidator
    feedbackIcons:
      valid: 'fa fa-check',
      invalid: 'fa fa-times',
      validating: 'fa fa-refresh'
    live: 'submitted'
    fields:
      'toque[tipo_parada]':
        validators:
          notEmpty:
            message: 'Es necesario seleccionar tipo de parada'
      'toque[tipo]':
        validators:
          notEmpty:
            message: 'Es necesario seleccionar tipo del terminal'
      'toque[nombre]':
        validators:
          notEmpty:
            message: 'Es necesario seleccionar el estado'
      'toque[parada_id]':
        validators:
          notEmpty:
            message: 'Es necesario seleccionar un terminal'
  .on 'success.form.bv', (e) ->
    e.preventDefault()
  return

$(document).on 'change', '#toque_tipo', ->
#  est = $('#toque-estado-id').val()
#  unless(!est)
#    $('#parada-toque-id').html('').append new Option('Seleccione una Parada', '')
#    cargar_paradas()
#  $('#link-nueva-ruta').attr('href',Routes.paradas_toque_nueva_parada_path() + '?estado_id=' + est + '&tipo=' + $(this).val() + '&ruta_id=' + $('#ruta-origen-id').val())
  $('#boton-nueva-parada').hide()
  $('#toque-estado-id').val('')
  $('#parada-toque-id').html('').append new Option('Seleccione una Parada', '')
  return

$(document).on 'change', '#toque-estado-id', ->
  toque_tipo = $('#toque_tipo').val()
  est = $('#toque-estado-id').val()
  unless (toque_tipo == "")
    if  (est == "")
      $('#parada-toque-id').html('').append new Option('Seleccione una Parada', '')
    else
      cargar_paradas()
      if toque_tipo == 'terminal_privado' || toque_tipo == 'terminal_publico'
        $('#boton-nueva-parada').hide()
      else if toque_tipo == 'zona'
        $('#boton-nueva-parada').show()
        $('#link-nueva-ruta').attr('href',Routes.paradas_toque_nueva_parada_path() + '?estado_id=' + est + '&tipo=' + toque_tipo + '&ruta_id=' + $('#ruta-origen-id').val())

#  unless ((toque_tipo == "")  && (est == ""))
#    cargar_paradas()
#    if toque_tipo == 'terminal_privado' || toque_tipo == 'terminal_publico'
#      $('#boton-nueva-parada').hide()
#    else if toque_tipo == 'zona'
#      $('#boton-nueva-parada').show()
#      $('#link-nueva-ruta').attr('href',Routes.paradas_toque_nueva_parada_path() + '?estado_id=' + est + '&tipo=' + toque_tipo + '&ruta_id=' + $('#ruta-origen-id').val())
  return

cargar_paradas = ->
  $.ajax
    type: "POST"
    url: "/dynamic_select/paradas/estado"
    dataType: "JSON"
    beforeSend: (xhr) ->
      $('#boton-toque-submit').html('<i class="fa fa-refresh fa-spin"></i>').prop('disabled', true)
      xhr.setRequestHeader 'X-CSRF-Token', $('meta[name="csrf-token"]').attr('content')
      return
    data:
      estado_id: $('#toque-estado-id').val()
      tipo_parada: $('#toque_tipo').val()
    success: (data) ->
      html_parada_select = $('#parada-toque-id')
      html_parada_select.html('')
      $.each data, (i, data_each) ->
        html_parada_select.append new Option(data_each.label, data_each.id)
        return
      $('#boton-toque-submit').html('<i class="fa fa-plus"></i>Agregar Toque').prop('disabled', false)
      return
  return

carga_select_localizacion = (elemFiltro, elemHtml, ruta, loader) ->
  $.ajax
    type: "POST"
    url: ruta
    dataType: "JSON"
    beforeSend: (xhr) ->
      loader.html('<i class="fa fa-refresh fa-spin"></i>')
      xhr.setRequestHeader 'X-CSRF-Token', $('meta[name="csrf-token"]').attr('content')
      return
    data:
      filtro_id: elemFiltro
    success: (data) ->
      elemHtml.empty()
      elemHtml.append new Option('Seleccione...', '')
      $.each data, (i, data_each) ->
        elemHtml.append new Option(data_each.nombre, data_each.id)
        return
      loader.html('')
      return
  return