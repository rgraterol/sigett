(function() {
  var NodeTypes, ParameterMissing, Utils, createGlobalJsRoutesObject, defaults, root,
    __hasProp = {}.hasOwnProperty;

  root = typeof exports !== "undefined" && exports !== null ? exports : this;

  ParameterMissing = function(message) {
    this.message = message;
  };

  ParameterMissing.prototype = new Error();

  defaults = {
    prefix: "",
    default_url_options: {}
  };

  NodeTypes = {"GROUP":1,"CAT":2,"SYMBOL":3,"OR":4,"STAR":5,"LITERAL":6,"SLASH":7,"DOT":8};

  Utils = {
    serialize: function(object, prefix) {
      var element, i, key, prop, result, s, _i, _len;

      if (prefix == null) {
        prefix = null;
      }
      if (!object) {
        return "";
      }
      if (!prefix && !(this.get_object_type(object) === "object")) {
        throw new Error("Url parameters should be a javascript hash");
      }
      if (root.jQuery) {
        result = root.jQuery.param(object);
        return (!result ? "" : result);
      }
      s = [];
      switch (this.get_object_type(object)) {
        case "array":
          for (i = _i = 0, _len = object.length; _i < _len; i = ++_i) {
            element = object[i];
            s.push(this.serialize(element, prefix + "[]"));
          }
          break;
        case "object":
          for (key in object) {
            if (!__hasProp.call(object, key)) continue;
            prop = object[key];
            if (!(prop != null)) {
              continue;
            }
            if (prefix != null) {
              key = "" + prefix + "[" + key + "]";
            }
            s.push(this.serialize(prop, key));
          }
          break;
        default:
          if (object) {
            s.push("" + (encodeURIComponent(prefix.toString())) + "=" + (encodeURIComponent(object.toString())));
          }
      }
      if (!s.length) {
        return "";
      }
      return s.join("&");
    },
    clean_path: function(path) {
      var last_index;

      path = path.split("://");
      last_index = path.length - 1;
      path[last_index] = path[last_index].replace(/\/+/g, "/");
      return path.join("://");
    },
    set_default_url_options: function(optional_parts, options) {
      var i, part, _i, _len, _results;

      _results = [];
      for (i = _i = 0, _len = optional_parts.length; _i < _len; i = ++_i) {
        part = optional_parts[i];
        if (!options.hasOwnProperty(part) && defaults.default_url_options.hasOwnProperty(part)) {
          _results.push(options[part] = defaults.default_url_options[part]);
        }
      }
      return _results;
    },
    extract_anchor: function(options) {
      var anchor;

      anchor = "";
      if (options.hasOwnProperty("anchor")) {
        anchor = "#" + options.anchor;
        delete options.anchor;
      }
      return anchor;
    },
    extract_trailing_slash: function(options) {
      var trailing_slash;

      trailing_slash = false;
      if (defaults.default_url_options.hasOwnProperty("trailing_slash")) {
        trailing_slash = defaults.default_url_options.trailing_slash;
      }
      if (options.hasOwnProperty("trailing_slash")) {
        trailing_slash = options.trailing_slash;
        delete options.trailing_slash;
      }
      return trailing_slash;
    },
    extract_options: function(number_of_params, args) {
      var last_el;

      last_el = args[args.length - 1];
      if (args.length > number_of_params || ((last_el != null) && "object" === this.get_object_type(last_el) && !this.look_like_serialized_model(last_el))) {
        return args.pop();
      } else {
        return {};
      }
    },
    look_like_serialized_model: function(object) {
      return "id" in object || "to_param" in object;
    },
    path_identifier: function(object) {
      var property;

      if (object === 0) {
        return "0";
      }
      if (!object) {
        return "";
      }
      property = object;
      if (this.get_object_type(object) === "object") {
        if ("to_param" in object) {
          property = object.to_param;
        } else if ("id" in object) {
          property = object.id;
        } else {
          property = object;
        }
        if (this.get_object_type(property) === "function") {
          property = property.call(object);
        }
      }
      return property.toString();
    },
    clone: function(obj) {
      var attr, copy, key;

      if ((obj == null) || "object" !== this.get_object_type(obj)) {
        return obj;
      }
      copy = obj.constructor();
      for (key in obj) {
        if (!__hasProp.call(obj, key)) continue;
        attr = obj[key];
        copy[key] = attr;
      }
      return copy;
    },
    prepare_parameters: function(required_parameters, actual_parameters, options) {
      var i, result, val, _i, _len;

      result = this.clone(options) || {};
      for (i = _i = 0, _len = required_parameters.length; _i < _len; i = ++_i) {
        val = required_parameters[i];
        if (i < actual_parameters.length) {
          result[val] = actual_parameters[i];
        }
      }
      return result;
    },
    build_path: function(required_parameters, optional_parts, route, args) {
      var anchor, opts, parameters, result, trailing_slash, url, url_params;

      args = Array.prototype.slice.call(args);
      opts = this.extract_options(required_parameters.length, args);
      if (args.length > required_parameters.length) {
        throw new Error("Too many parameters provided for path");
      }
      parameters = this.prepare_parameters(required_parameters, args, opts);
      this.set_default_url_options(optional_parts, parameters);
      anchor = this.extract_anchor(parameters);
      trailing_slash = this.extract_trailing_slash(parameters);
      result = "" + (this.get_prefix()) + (this.visit(route, parameters));
      url = Utils.clean_path("" + result);
      if (trailing_slash === true) {
        url = url.replace(/(.*?)[\/]?$/, "$1/");
      }
      if ((url_params = this.serialize(parameters)).length) {
        url += "?" + url_params;
      }
      url += anchor;
      return url;
    },
    visit: function(route, parameters, optional) {
      var left, left_part, right, right_part, type, value;

      if (optional == null) {
        optional = false;
      }
      type = route[0], left = route[1], right = route[2];
      switch (type) {
        case NodeTypes.GROUP:
          return this.visit(left, parameters, true);
        case NodeTypes.STAR:
          return this.visit_globbing(left, parameters, true);
        case NodeTypes.LITERAL:
        case NodeTypes.SLASH:
        case NodeTypes.DOT:
          return left;
        case NodeTypes.CAT:
          left_part = this.visit(left, parameters, optional);
          right_part = this.visit(right, parameters, optional);
          if (optional && !(left_part && right_part)) {
            return "";
          }
          return "" + left_part + right_part;
        case NodeTypes.SYMBOL:
          value = parameters[left];
          if (value != null) {
            delete parameters[left];
            return this.path_identifier(value);
          }
          if (optional) {
            return "";
          } else {
            throw new ParameterMissing("Route parameter missing: " + left);
          }
          break;
        default:
          throw new Error("Unknown Rails node type");
      }
    },
    build_path_spec: function(route, wildcard) {
      var left, right, type;

      if (wildcard == null) {
        wildcard = false;
      }
      type = route[0], left = route[1], right = route[2];
      switch (type) {
        case NodeTypes.GROUP:
          return "(" + (this.build_path_spec(left)) + ")";
        case NodeTypes.CAT:
          return "" + (this.build_path_spec(left)) + (this.build_path_spec(right));
        case NodeTypes.STAR:
          return this.build_path_spec(left, true);
        case NodeTypes.SYMBOL:
          if (wildcard === true) {
            return "" + (left[0] === '*' ? '' : '*') + left;
          } else {
            return ":" + left;
          }
          break;
        case NodeTypes.SLASH:
        case NodeTypes.DOT:
        case NodeTypes.LITERAL:
          return left;
        default:
          throw new Error("Unknown Rails node type");
      }
    },
    visit_globbing: function(route, parameters, optional) {
      var left, right, type, value;

      type = route[0], left = route[1], right = route[2];
      if (left.replace(/^\*/i, "") !== left) {
        route[1] = left = left.replace(/^\*/i, "");
      }
      value = parameters[left];
      if (value == null) {
        return this.visit(route, parameters, optional);
      }
      parameters[left] = (function() {
        switch (this.get_object_type(value)) {
          case "array":
            return value.join("/");
          default:
            return value;
        }
      }).call(this);
      return this.visit(route, parameters, optional);
    },
    get_prefix: function() {
      var prefix;

      prefix = defaults.prefix;
      if (prefix !== "") {
        prefix = (prefix.match("/$") ? prefix : "" + prefix + "/");
      }
      return prefix;
    },
    route: function(required_parts, optional_parts, route_spec) {
      var path_fn;

      path_fn = function() {
        return Utils.build_path(required_parts, optional_parts, route_spec, arguments);
      };
      path_fn.required_params = required_parts;
      path_fn.toString = function() {
        return Utils.build_path_spec(route_spec);
      };
      return path_fn;
    },
    _classToTypeCache: null,
    _classToType: function() {
      var name, _i, _len, _ref;

      if (this._classToTypeCache != null) {
        return this._classToTypeCache;
      }
      this._classToTypeCache = {};
      _ref = "Boolean Number String Function Array Date RegExp Object Error".split(" ");
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        name = _ref[_i];
        this._classToTypeCache["[object " + name + "]"] = name.toLowerCase();
      }
      return this._classToTypeCache;
    },
    get_object_type: function(obj) {
      if (root.jQuery && (root.jQuery.type != null)) {
        return root.jQuery.type(obj);
      }
      if (obj == null) {
        return "" + obj;
      }
      if (typeof obj === "object" || typeof obj === "function") {
        return this._classToType()[Object.prototype.toString.call(obj)] || "object";
      } else {
        return typeof obj;
      }
    }
  };

  createGlobalJsRoutesObject = function() {
    var namespace;

    namespace = function(mainRoot, namespaceString) {
      var current, parts;

      parts = (namespaceString ? namespaceString.split(".") : []);
      if (!parts.length) {
        return;
      }
      current = parts.shift();
      mainRoot[current] = mainRoot[current] || {};
      return namespace(mainRoot[current], parts.join("."));
    };
    namespace(root, "Routes");
    root.Routes = {
// actualizar_contrasena => /actualizar_contrasena(.:format)
  // function(options)
  actualizar_contrasena_path: Utils.route([], ["format"], [2,[2,[7,"/",false],[6,"actualizar_contrasena",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// asegurador_create => /asegurador/create(.:format)
  // function(options)
  asegurador_create_path: Utils.route([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"asegurador",false]],[7,"/",false]],[6,"create",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// asegurador_destroy => /asegurador/destroy/:id(.:format)
  // function(id, options)
  asegurador_destroy_path: Utils.route(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"asegurador",false]],[7,"/",false]],[6,"destroy",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// asegurador_edit => /asegurador/edit/:id(.:format)
  // function(id, options)
  asegurador_edit_path: Utils.route(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"asegurador",false]],[7,"/",false]],[6,"edit",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// asegurador_index => /asegurador/index(.:format)
  // function(options)
  asegurador_index_path: Utils.route([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"asegurador",false]],[7,"/",false]],[6,"index",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// asegurador_rif_a => /asegurador/rif(.:format)
  // function(options)
  asegurador_rif_a_path: Utils.route([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"asegurador",false]],[7,"/",false]],[6,"rif",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// asegurador_update => /asegurador/update/:id(.:format)
  // function(id, options)
  asegurador_update_path: Utils.route(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"asegurador",false]],[7,"/",false]],[6,"update",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// cambio_contrasena => /cambio_contrasena(.:format)
  // function(options)
  cambio_contrasena_path: Utils.route([], ["format"], [2,[2,[7,"/",false],[6,"cambio_contrasena",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// cancel_session_user_registration => /session_users/cancel(.:format)
  // function(options)
  cancel_session_user_registration_path: Utils.route([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"session_users",false]],[7,"/",false]],[6,"cancel",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// cerrar_sesion => /cerrar_sesion(.:format)
  // function(options)
  cerrar_sesion_path: Utils.route([], ["format"], [2,[2,[7,"/",false],[6,"cerrar_sesion",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// creador_rutas => /creador/rutas/buscar(.:format)
  // function(options)
  creador_rutas_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"creador",false]],[7,"/",false]],[6,"rutas",false]],[7,"/",false]],[6,"buscar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// destroy_session_user_session => /session_users/sign_out(.:format)
  // function(options)
  destroy_session_user_session_path: Utils.route([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"session_users",false]],[7,"/",false]],[6,"sign_out",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// dynamic_select_ciudades => /dynamic_select/:estado_id/ciudades(.:format)
  // function(estado_id, options)
  dynamic_select_ciudades_path: Utils.route(["estado_id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"dynamic_select",false]],[7,"/",false]],[3,"estado_id",false]],[7,"/",false]],[6,"ciudades",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// dynamic_select_form_igual_parada_estado => /dynamic_select/form_igual_parada/estado(.:format)
  // function(options)
  dynamic_select_form_igual_parada_estado_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"dynamic_select",false]],[7,"/",false]],[6,"form_igual_parada",false]],[7,"/",false]],[6,"estado",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// dynamic_select_municipios => /dynamic_select/:estado_id/municipios(.:format)
  // function(estado_id, options)
  dynamic_select_municipios_path: Utils.route(["estado_id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"dynamic_select",false]],[7,"/",false]],[3,"estado_id",false]],[7,"/",false]],[6,"municipios",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// dynamic_select_paradas => /dynamic_select/:municipio_id/paradas(.:format)
  // function(municipio_id, options)
  dynamic_select_paradas_path: Utils.route(["municipio_id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"dynamic_select",false]],[7,"/",false]],[3,"municipio_id",false]],[7,"/",false]],[6,"paradas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// dynamic_select_paradas_parroquia => /dynamic_select/:parroquia_id/paradas/p(.:format)
  // function(parroquia_id, options)
  dynamic_select_paradas_parroquia_path: Utils.route(["parroquia_id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"dynamic_select",false]],[7,"/",false]],[3,"parroquia_id",false]],[7,"/",false]],[6,"paradas",false]],[7,"/",false]],[6,"p",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// dynamic_select_parroquia_estado_index => /dynamic_select/paradas/estado(.:format)
  // function(options)
  dynamic_select_parroquia_estado_index_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"dynamic_select",false]],[7,"/",false]],[6,"paradas",false]],[7,"/",false]],[6,"estado",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// dynamic_select_parroquia_index => /dynamic_select/paradas(.:format)
  // function(options)
  dynamic_select_parroquia_index_path: Utils.route([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"dynamic_select",false]],[7,"/",false]],[6,"paradas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// dynamic_select_parroquia_vs => /dynamic_select/:municipio_id/parroquias(.:format)
  // function(municipio_id, options)
  dynamic_select_parroquia_vs_path: Utils.route(["municipio_id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"dynamic_select",false]],[7,"/",false]],[3,"municipio_id",false]],[7,"/",false]],[6,"parroquias",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// edit_flota => /flotas/:id/edit(.:format)
  // function(id, options)
  edit_flota_path: Utils.route(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"flotas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"edit",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// edit_flota_vehiculo => /flotas/:flota_id/vehiculos/:id/edit(.:format)
  // function(flota_id, id, options)
  edit_flota_vehiculo_path: Utils.route(["flota_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"flotas",false]],[7,"/",false]],[3,"flota_id",false]],[7,"/",false]],[6,"vehiculos",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"edit",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// edit_paradas_adm_parada => /paradas/adm_paradas/:id/edit(.:format)
  // function(id, options)
  edit_paradas_adm_parada_path: Utils.route(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"paradas",false]],[7,"/",false]],[6,"adm_paradas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"edit",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// edit_session_user_password => /session_users/password/edit(.:format)
  // function(options)
  edit_session_user_password_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"session_users",false]],[7,"/",false]],[6,"password",false]],[7,"/",false]],[6,"edit",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// edit_session_user_registration => /session_users/edit(.:format)
  // function(options)
  edit_session_user_registration_path: Utils.route([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"session_users",false]],[7,"/",false]],[6,"edit",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// estadistica_igual_parada => /resultado/igual/parada(.:format)
  // function(options)
  estadistica_igual_parada_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"resultado",false]],[7,"/",false]],[6,"igual",false]],[7,"/",false]],[6,"parada",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// estadistica_module_estadistica_igual_parada => /estadistica_module/estadistica/igual/parada(.:format)
  // function(options)
  estadistica_module_estadistica_igual_parada_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"estadistica_module",false]],[7,"/",false]],[6,"estadistica",false]],[7,"/",false]],[6,"igual",false]],[7,"/",false]],[6,"parada",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// estadistica_module_form_igual_parada => /estadistica_module/form/igual/parada(.:format)
  // function(options)
  estadistica_module_form_igual_parada_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"estadistica_module",false]],[7,"/",false]],[6,"form",false]],[7,"/",false]],[6,"igual",false]],[7,"/",false]],[6,"parada",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// estadistica_module_modalidad_ciudad => /estadistica_module/modalidad/ciudad(.:format)
  // function(options)
  estadistica_module_modalidad_ciudad_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"estadistica_module",false]],[7,"/",false]],[6,"modalidad",false]],[7,"/",false]],[6,"ciudad",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// estadistica_module_modalidad_cupo_autorizado => /estadistica_module/modalidad/cupo/estados(.:format)
  // function(options)
  estadistica_module_modalidad_cupo_autorizado_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"estadistica_module",false]],[7,"/",false]],[6,"modalidad",false]],[7,"/",false]],[6,"cupo",false]],[7,"/",false]],[6,"estados",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// estadistica_module_modalidad_estado => /estadistica_module/modalidad/estado(.:format)
  // function(options)
  estadistica_module_modalidad_estado_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"estadistica_module",false]],[7,"/",false]],[6,"modalidad",false]],[7,"/",false]],[6,"estado",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// estadistica_module_modalidad_estado_all => /estadistica_module/modalidad/todos/estados(.:format)
  // function(options)
  estadistica_module_modalidad_estado_all_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"estadistica_module",false]],[7,"/",false]],[6,"modalidad",false]],[7,"/",false]],[6,"todos",false]],[7,"/",false]],[6,"estados",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// estadistica_module_modalidad_parroquias => /estadistica_module/modalidad/parroquias(.:format)
  // function(options)
  estadistica_module_modalidad_parroquias_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"estadistica_module",false]],[7,"/",false]],[6,"modalidad",false]],[7,"/",false]],[6,"parroquias",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// estadistica_module_modalidad_tipo_unidad => /estadistica_module/modalidad/tipounidad(.:format)
  // function(options)
  estadistica_module_modalidad_tipo_unidad_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"estadistica_module",false]],[7,"/",false]],[6,"modalidad",false]],[7,"/",false]],[6,"tipounidad",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// estadistica_module_proximas_vencer_flota => /estadistica_module/proximas/vencer/flota(.:format)
  // function(options)
  estadistica_module_proximas_vencer_flota_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"estadistica_module",false]],[7,"/",false]],[6,"proximas",false]],[7,"/",false]],[6,"vencer",false]],[7,"/",false]],[6,"flota",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// estadistica_module_selector_ano_modalidad => /estadistica_module/modalidad/estado/seleccion(.:format)
  // function(options)
  estadistica_module_selector_ano_modalidad_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"estadistica_module",false]],[7,"/",false]],[6,"modalidad",false]],[7,"/",false]],[6,"estado",false]],[7,"/",false]],[6,"seleccion",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// estadistica_module_selector_ano_vencimiento_cps => /estadistica_module/vencimiento/ano/seleccion/cps(.:format)
  // function(options)
  estadistica_module_selector_ano_vencimiento_cps_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"estadistica_module",false]],[7,"/",false]],[6,"vencimiento",false]],[7,"/",false]],[6,"ano",false]],[7,"/",false]],[6,"seleccion",false]],[7,"/",false]],[6,"cps",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// estadistica_module_selector_ano_vencimiento_flota => /estadistica_module/vencimiento/ano/seleccion/flota(.:format)
  // function(options)
  estadistica_module_selector_ano_vencimiento_flota_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"estadistica_module",false]],[7,"/",false]],[6,"vencimiento",false]],[7,"/",false]],[6,"ano",false]],[7,"/",false]],[6,"seleccion",false]],[7,"/",false]],[6,"flota",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// estadistica_module_selector_ciudad => /estadistica_module/modalidad/ciudad/seleccion(.:format)
  // function(options)
  estadistica_module_selector_ciudad_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"estadistica_module",false]],[7,"/",false]],[6,"modalidad",false]],[7,"/",false]],[6,"ciudad",false]],[7,"/",false]],[6,"seleccion",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// estadistica_module_selector_estados_tipo => /estadistica_module/modalidad/estados/tipounidad(.:format)
  // function(options)
  estadistica_module_selector_estados_tipo_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"estadistica_module",false]],[7,"/",false]],[6,"modalidad",false]],[7,"/",false]],[6,"estados",false]],[7,"/",false]],[6,"tipounidad",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// estadistica_module_selector_meses_historicos => /estadistica_module/meses/seleccion/historicos(.:format)
  // function(options)
  estadistica_module_selector_meses_historicos_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"estadistica_module",false]],[7,"/",false]],[6,"meses",false]],[7,"/",false]],[6,"seleccion",false]],[7,"/",false]],[6,"historicos",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// estadistica_module_selector_parroquias => /estadistica_module/modalidad/parroquias/seleccion(.:format)
  // function(options)
  estadistica_module_selector_parroquias_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"estadistica_module",false]],[7,"/",false]],[6,"modalidad",false]],[7,"/",false]],[6,"parroquias",false]],[7,"/",false]],[6,"seleccion",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// estadistica_module_selector_proximas_vencer_flota => /estadistica_module/selector/proximas/vencer/flota(.:format)
  // function(options)
  estadistica_module_selector_proximas_vencer_flota_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"estadistica_module",false]],[7,"/",false]],[6,"selector",false]],[7,"/",false]],[6,"proximas",false]],[7,"/",false]],[6,"vencer",false]],[7,"/",false]],[6,"flota",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// estadistica_module_tramites_historicos => /estadistica_module/tramites/historicos(.:format)
  // function(options)
  estadistica_module_tramites_historicos_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"estadistica_module",false]],[7,"/",false]],[6,"tramites",false]],[7,"/",false]],[6,"historicos",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// estadistica_module_vencimiento_cps => /estadistica_module/vencimiento/ano/cps(.:format)
  // function(options)
  estadistica_module_vencimiento_cps_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"estadistica_module",false]],[7,"/",false]],[6,"vencimiento",false]],[7,"/",false]],[6,"ano",false]],[7,"/",false]],[6,"cps",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// estadistica_module_vencimiento_flota => /estadistica_module/unidades/todos/estado(.:format)
  // function(options)
  estadistica_module_vencimiento_flota_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"estadistica_module",false]],[7,"/",false]],[6,"unidades",false]],[7,"/",false]],[6,"todos",false]],[7,"/",false]],[6,"estado",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// estadistica_module_vistas_ciudad => /estadistica_module/vistas/ciudad(.:format)
  // function(options)
  estadistica_module_vistas_ciudad_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"estadistica_module",false]],[7,"/",false]],[6,"vistas",false]],[7,"/",false]],[6,"ciudad",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// estadistica_module_vistas_estados => /estadistica_module/vistas/estados/modalidad(.:format)
  // function(options)
  estadistica_module_vistas_estados_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"estadistica_module",false]],[7,"/",false]],[6,"vistas",false]],[7,"/",false]],[6,"estados",false]],[7,"/",false]],[6,"modalidad",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// estadistica_module_vistas_parroquias => /estadistica_module/vistas/parroquias(.:format)
  // function(options)
  estadistica_module_vistas_parroquias_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"estadistica_module",false]],[7,"/",false]],[6,"vistas",false]],[7,"/",false]],[6,"parroquias",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// flota => /flota(.:format)
  // function(options)
  flota_path: Utils.route([], ["format"], [2,[2,[7,"/",false],[6,"flota",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// flota_d_create => /flota_d/create(.:format)
  // function(options)
  flota_d_create_path: Utils.route([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"flota_d",false]],[7,"/",false]],[6,"create",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// flota_d_destroy => /flota_d/destroy/:id(.:format)
  // function(id, options)
  flota_d_destroy_path: Utils.route(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"flota_d",false]],[7,"/",false]],[6,"destroy",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// flota_d_edit => /flota_d/edit/:id(.:format)
  // function(id, options)
  flota_d_edit_path: Utils.route(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"flota_d",false]],[7,"/",false]],[6,"edit",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// flota_d_index => /flota_d/index(.:format)
  // function(options)
  flota_d_index_path: Utils.route([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"flota_d",false]],[7,"/",false]],[6,"index",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// flota_d_new => /flota_d/new(.:format)
  // function(options)
  flota_d_new_path: Utils.route([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"flota_d",false]],[7,"/",false]],[6,"new",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// flota_d_show => /flota_d/show/:id(.:format)
  // function(id, options)
  flota_d_show_path: Utils.route(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"flota_d",false]],[7,"/",false]],[6,"show",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// flota_d_update => /flota_d/update/:id(.:format)
  // function(id, options)
  flota_d_update_path: Utils.route(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"flota_d",false]],[7,"/",false]],[6,"update",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// flota_vehiculo => /flotas/:flota_id/vehiculos/:id(.:format)
  // function(flota_id, id, options)
  flota_vehiculo_path: Utils.route(["flota_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"flotas",false]],[7,"/",false]],[3,"flota_id",false]],[7,"/",false]],[6,"vehiculos",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// flota_vehiculo_arrendatario => /flotas/:flota_id/vehiculo/arrendatario(.:format)
  // function(flota_id, options)
  flota_vehiculo_arrendatario_path: Utils.route(["flota_id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"flotas",false]],[7,"/",false]],[3,"flota_id",false]],[7,"/",false]],[6,"vehiculo",false]],[7,"/",false]],[6,"arrendatario",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// flota_vehiculos => /flotas/:flota_id/vehiculos(.:format)
  // function(flota_id, options)
  flota_vehiculos_path: Utils.route(["flota_id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"flotas",false]],[7,"/",false]],[3,"flota_id",false]],[7,"/",false]],[6,"vehiculos",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// flotas => /flotas(.:format)
  // function(options)
  flotas_path: Utils.route([], ["format"], [2,[2,[7,"/",false],[6,"flotas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// flotum_seguro_vehiculos => /flota/:flotum_id/seguro_vehiculos(.:format)
  // function(flotum_id, options)
  flotum_seguro_vehiculos_path: Utils.route(["flotum_id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"flota",false]],[7,"/",false]],[3,"flotum_id",false]],[7,"/",false]],[6,"seguro_vehiculos",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// form_igual_parada => /operadora/igual/parada(.:format)
  // function(options)
  form_igual_parada_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"operadora",false]],[7,"/",false]],[6,"igual",false]],[7,"/",false]],[6,"parada",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// historico_create => /historico/create(.:format)
  // function(options)
  historico_create_path: Utils.route([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"historico",false]],[7,"/",false]],[6,"create",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// historico_destroy => /historico/destroy/:id(.:format)
  // function(id, options)
  historico_destroy_path: Utils.route(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"historico",false]],[7,"/",false]],[6,"destroy",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// historico_edit => /historico/edit/:id(.:format)
  // function(id, options)
  historico_edit_path: Utils.route(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"historico",false]],[7,"/",false]],[6,"edit",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// historico_index => /historico/index(.:format)
  // function(options)
  historico_index_path: Utils.route([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"historico",false]],[7,"/",false]],[6,"index",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// historico_show => /historico/show/:id(.:format)
  // function(id, options)
  historico_show_path: Utils.route(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"historico",false]],[7,"/",false]],[6,"show",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// historico_update => /historico/update/:id(.:format)
  // function(id, options)
  historico_update_path: Utils.route(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"historico",false]],[7,"/",false]],[6,"update",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// horario_d_create => /horario_d/create(.:format)
  // function(options)
  horario_d_create_path: Utils.route([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"horario_d",false]],[7,"/",false]],[6,"create",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// horario_d_destroy => /horario_d/destroy/:id(.:format)
  // function(id, options)
  horario_d_destroy_path: Utils.route(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"horario_d",false]],[7,"/",false]],[6,"destroy",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// horario_d_edit => /horario_d/edit/:id(.:format)
  // function(id, options)
  horario_d_edit_path: Utils.route(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"horario_d",false]],[7,"/",false]],[6,"edit",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// horario_d_index => /horario_d/index(.:format)
  // function(options)
  horario_d_index_path: Utils.route([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"horario_d",false]],[7,"/",false]],[6,"index",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// horario_d_new => /horario_d/new(.:format)
  // function(options)
  horario_d_new_path: Utils.route([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"horario_d",false]],[7,"/",false]],[6,"new",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// horario_d_show => /horario_d/show/:id(.:format)
  // function(id, options)
  horario_d_show_path: Utils.route(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"horario_d",false]],[7,"/",false]],[6,"show",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// horario_d_update => /horario_d/update/:id(.:format)
  // function(id, options)
  horario_d_update_path: Utils.route(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"horario_d",false]],[7,"/",false]],[6,"update",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// index_est_mod => /operadoras/est/mod(.:format)
  // function(options)
  index_est_mod_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"operadoras",false]],[7,"/",false]],[6,"est",false]],[7,"/",false]],[6,"mod",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// index_op_fl_vencer => /operadoras/flota/vencer(.:format)
  // function(options)
  index_op_fl_vencer_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"operadoras",false]],[7,"/",false]],[6,"flota",false]],[7,"/",false]],[6,"vencer",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// index_op_igual_parada => /operadoras/igual/parada(.:format)
  // function(options)
  index_op_igual_parada_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"operadoras",false]],[7,"/",false]],[6,"igual",false]],[7,"/",false]],[6,"parada",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// index_op_mes_cps => /operadoras/vencimiento/mes/cps(.:format)
  // function(options)
  index_op_mes_cps_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"operadoras",false]],[7,"/",false]],[6,"vencimiento",false]],[7,"/",false]],[6,"mes",false]],[7,"/",false]],[6,"cps",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// index_op_mes_venc => /operadoras/vencimiento/mes(.:format)
  // function(options)
  index_op_mes_venc_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"operadoras",false]],[7,"/",false]],[6,"vencimiento",false]],[7,"/",false]],[6,"mes",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// index_operadora => /operadoras/index(.:format)
  // function(options)
  index_operadora_path: Utils.route([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"operadoras",false]],[7,"/",false]],[6,"index",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// index_tipo_unidad => /operadoras/tipounidadvehicular(.:format)
  // function(options)
  index_tipo_unidad_path: Utils.route([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"operadoras",false]],[7,"/",false]],[6,"tipounidadvehicular",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// iniciar_sesion => /iniciar_sesion(.:format)
  // function(options)
  iniciar_sesion_path: Utils.route([], ["format"], [2,[2,[7,"/",false],[6,"iniciar_sesion",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// listar_por_rutas => /operadoras/listadas/rutas(.:format)
  // function(options)
  listar_por_rutas_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"operadoras",false]],[7,"/",false]],[6,"listadas",false]],[7,"/",false]],[6,"rutas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// localizacion_select_estado_ciudades => /localizacion_select/lista/ciudades(.:format)
  // function(options)
  localizacion_select_estado_ciudades_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"localizacion_select",false]],[7,"/",false]],[6,"lista",false]],[7,"/",false]],[6,"ciudades",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// localizacion_select_estado_municipios => /localizacion_select/lista/municipios(.:format)
  // function(options)
  localizacion_select_estado_municipios_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"localizacion_select",false]],[7,"/",false]],[6,"lista",false]],[7,"/",false]],[6,"municipios",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// localizacion_select_estado_tipo_paradas => /localizacion_select/lista/estado/paradas(.:format)
  // function(options)
  localizacion_select_estado_tipo_paradas_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"localizacion_select",false]],[7,"/",false]],[6,"lista",false]],[7,"/",false]],[6,"estado",false]],[7,"/",false]],[6,"paradas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// localizacion_select_municipio_parroquias => /localizacion_select/lista/parroquias(.:format)
  // function(options)
  localizacion_select_municipio_parroquias_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"localizacion_select",false]],[7,"/",false]],[6,"lista",false]],[7,"/",false]],[6,"parroquias",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// localizacion_select_parroquia_paradas => /localizacion_select/lista/paradas(.:format)
  // function(options)
  localizacion_select_parroquia_paradas_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"localizacion_select",false]],[7,"/",false]],[6,"lista",false]],[7,"/",false]],[6,"paradas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// new_flota => /flotas/new(.:format)
  // function(options)
  new_flota_path: Utils.route([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"flotas",false]],[7,"/",false]],[6,"new",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// new_flota_vehiculo => /flotas/:flota_id/vehiculos/new(.:format)
  // function(flota_id, options)
  new_flota_vehiculo_path: Utils.route(["flota_id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"flotas",false]],[7,"/",false]],[3,"flota_id",false]],[7,"/",false]],[6,"vehiculos",false]],[7,"/",false]],[6,"new",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// new_paradas_adm_parada => /paradas/adm_paradas/new(.:format)
  // function(options)
  new_paradas_adm_parada_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"paradas",false]],[7,"/",false]],[6,"adm_paradas",false]],[7,"/",false]],[6,"new",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// new_session_user_confirmation => /session_users/confirmation/new(.:format)
  // function(options)
  new_session_user_confirmation_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"session_users",false]],[7,"/",false]],[6,"confirmation",false]],[7,"/",false]],[6,"new",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// new_session_user_password => /session_users/password/new(.:format)
  // function(options)
  new_session_user_password_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"session_users",false]],[7,"/",false]],[6,"password",false]],[7,"/",false]],[6,"new",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// new_session_user_registration => /session_users/sign_up(.:format)
  // function(options)
  new_session_user_registration_path: Utils.route([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"session_users",false]],[7,"/",false]],[6,"sign_up",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// new_session_user_session => /session_users/sign_in(.:format)
  // function(options)
  new_session_user_session_path: Utils.route([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"session_users",false]],[7,"/",false]],[6,"sign_in",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// operadora_ciudad_municipio => /operadora/ciudad_municipio(.:format)
  // function(options)
  operadora_ciudad_municipio_path: Utils.route([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"operadora",false]],[7,"/",false]],[6,"ciudad_municipio",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// operadora_consultar => /operadora/consultar(.:format)
  // function(options)
  operadora_consultar_path: Utils.route([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"operadora",false]],[7,"/",false]],[6,"consultar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// operadora_create => /operadora/create(.:format)
  // function(options)
  operadora_create_path: Utils.route([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"operadora",false]],[7,"/",false]],[6,"create",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// operadora_destroy => /operadora/destroy/:id(.:format)
  // function(id, options)
  operadora_destroy_path: Utils.route(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"operadora",false]],[7,"/",false]],[6,"destroy",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// operadora_edit => /operadora/edit/:id(.:format)
  // function(id, options)
  operadora_edit_path: Utils.route(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"operadora",false]],[7,"/",false]],[6,"edit",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// operadora_index => /operadora/index(.:format)
  // function(options)
  operadora_index_path: Utils.route([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"operadora",false]],[7,"/",false]],[6,"index",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// operadora_new => /operadora/new(.:format)
  // function(options)
  operadora_new_path: Utils.route([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"operadora",false]],[7,"/",false]],[6,"new",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// operadora_show => /operadora/show/:id(.:format)
  // function(id, options)
  operadora_show_path: Utils.route(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"operadora",false]],[7,"/",false]],[6,"show",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// operadora_update => /operadora/update/:id(.:format)
  // function(id, options)
  operadora_update_path: Utils.route(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"operadora",false]],[7,"/",false]],[6,"update",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// paradas_adm_parada => /paradas/adm_paradas/:id(.:format)
  // function(id, options)
  paradas_adm_parada_path: Utils.route(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"paradas",false]],[7,"/",false]],[6,"adm_paradas",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// paradas_adm_paradas => /paradas/adm_paradas(.:format)
  // function(options)
  paradas_adm_paradas_path: Utils.route([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"paradas",false]],[7,"/",false]],[6,"adm_paradas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// paradas_buscar_ubicacion => /paradas/buscar/ubicacion/parada(.:format)
  // function(options)
  paradas_buscar_ubicacion_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"paradas",false]],[7,"/",false]],[6,"buscar",false]],[7,"/",false]],[6,"ubicacion",false]],[7,"/",false]],[6,"parada",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// paradas_cookie_constructor => /paradas/cookie/constructor(.:format)
  // function(options)
  paradas_cookie_constructor_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"paradas",false]],[7,"/",false]],[6,"cookie",false]],[7,"/",false]],[6,"constructor",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// paradas_create => /paradas/create(.:format)
  // function(options)
  paradas_create_path: Utils.route([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"paradas",false]],[7,"/",false]],[6,"create",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// paradas_create_ubicacion => /paradas/create/ubicacion/parada(.:format)
  // function(options)
  paradas_create_ubicacion_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"paradas",false]],[7,"/",false]],[6,"create",false]],[7,"/",false]],[6,"ubicacion",false]],[7,"/",false]],[6,"parada",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// paradas_create_ubicacion_admin => /paradas/create/ubicacion/parada(.:format)
  // function(options)
  paradas_create_ubicacion_admin_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"paradas",false]],[7,"/",false]],[6,"create",false]],[7,"/",false]],[6,"ubicacion",false]],[7,"/",false]],[6,"parada",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// paradas_destroy => /paradas/destroy(.:format)
  // function(options)
  paradas_destroy_path: Utils.route([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"paradas",false]],[7,"/",false]],[6,"destroy",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// paradas_destroy_ubicacion => /paradas/destroy/:id/ubicacion/parada(.:format)
  // function(id, options)
  paradas_destroy_ubicacion_path: Utils.route(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"paradas",false]],[7,"/",false]],[6,"destroy",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"ubicacion",false]],[7,"/",false]],[6,"parada",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// paradas_edit => /paradas/edit/:id(.:format)
  // function(id, options)
  paradas_edit_path: Utils.route(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"paradas",false]],[7,"/",false]],[6,"edit",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// paradas_edit_ubicacion => /paradas/edit/:id/ubicacion/parada(.:format)
  // function(id, options)
  paradas_edit_ubicacion_path: Utils.route(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"paradas",false]],[7,"/",false]],[6,"edit",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"ubicacion",false]],[7,"/",false]],[6,"parada",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// paradas_error_parada_ubicacion => /paradas/error/parada(.:format)
  // function(options)
  paradas_error_parada_ubicacion_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"paradas",false]],[7,"/",false]],[6,"error",false]],[7,"/",false]],[6,"parada",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// paradas_index => /paradas/index(.:format)
  // function(options)
  paradas_index_path: Utils.route([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"paradas",false]],[7,"/",false]],[6,"index",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// paradas_index_ubicacion => /paradas/index/ubicacion/parada(.:format)
  // function(options)
  paradas_index_ubicacion_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"paradas",false]],[7,"/",false]],[6,"index",false]],[7,"/",false]],[6,"ubicacion",false]],[7,"/",false]],[6,"parada",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// paradas_new => /paradas/new(.:format)
  // function(options)
  paradas_new_path: Utils.route([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"paradas",false]],[7,"/",false]],[6,"new",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// paradas_new_by_post_ubicacion => /paradas/new_by_post/ubicacion/parada(.:format)
  // function(options)
  paradas_new_by_post_ubicacion_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"paradas",false]],[7,"/",false]],[6,"new_by_post",false]],[7,"/",false]],[6,"ubicacion",false]],[7,"/",false]],[6,"parada",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// paradas_new_ubicacion => /paradas/new/ubicacion/parada(.:format)
  // function(options)
  paradas_new_ubicacion_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"paradas",false]],[7,"/",false]],[6,"new",false]],[7,"/",false]],[6,"ubicacion",false]],[7,"/",false]],[6,"parada",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// paradas_redirect_parada => /paradas/redirect(.:format)
  // function(options)
  paradas_redirect_parada_path: Utils.route([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"paradas",false]],[7,"/",false]],[6,"redirect",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// paradas_ruta_flota_redirect_new => /paradas/ruta_flota_new_path/full(.:format)
  // function(options)
  paradas_ruta_flota_redirect_new_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"paradas",false]],[7,"/",false]],[6,"ruta_flota_new_path",false]],[7,"/",false]],[6,"full",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// paradas_show => /paradas/show/:id(.:format)
  // function(id, options)
  paradas_show_path: Utils.route(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"paradas",false]],[7,"/",false]],[6,"show",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// paradas_show_ubicacion => /paradas/show/:id/ubicacion/parada(.:format)
  // function(id, options)
  paradas_show_ubicacion_path: Utils.route(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"paradas",false]],[7,"/",false]],[6,"show",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"ubicacion",false]],[7,"/",false]],[6,"parada",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// paradas_toque_guardar_parada => /paradas/guardar/parada(.:format)
  // function(options)
  paradas_toque_guardar_parada_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"paradas",false]],[7,"/",false]],[6,"guardar",false]],[7,"/",false]],[6,"parada",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// paradas_toque_nueva_parada => /paradas/nueva/parada(.:format)
  // function(options)
  paradas_toque_nueva_parada_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"paradas",false]],[7,"/",false]],[6,"nueva",false]],[7,"/",false]],[6,"parada",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// paradas_update => /paradas/update/:id(.:format)
  // function(id, options)
  paradas_update_path: Utils.route(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"paradas",false]],[7,"/",false]],[6,"update",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// paradas_update_ubicacion => /paradas/update/:id/ubicacion/parada(.:format)
  // function(id, options)
  paradas_update_ubicacion_path: Utils.route(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"paradas",false]],[7,"/",false]],[6,"update",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"ubicacion",false]],[7,"/",false]],[6,"parada",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// rails_info => /rails/info(.:format)
  // function(options)
  rails_info_path: Utils.route([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"rails",false]],[7,"/",false]],[6,"info",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// rails_info_properties => /rails/info/properties(.:format)
  // function(options)
  rails_info_properties_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"rails",false]],[7,"/",false]],[6,"info",false]],[7,"/",false]],[6,"properties",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// rails_info_routes => /rails/info/routes(.:format)
  // function(options)
  rails_info_routes_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"rails",false]],[7,"/",false]],[6,"info",false]],[7,"/",false]],[6,"routes",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// rails_mailers => /rails/mailers(.:format)
  // function(options)
  rails_mailers_path: Utils.route([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"rails",false]],[7,"/",false]],[6,"mailers",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// recuperar_contrasena => /recuperar_contrasena(.:format)
  // function(options)
  recuperar_contrasena_path: Utils.route([], ["format"], [2,[2,[7,"/",false],[6,"recuperar_contrasena",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// reestablecer_password => /reestablecer_password(.:format)
  // function(options)
  reestablecer_password_path: Utils.route([], ["format"], [2,[2,[7,"/",false],[6,"reestablecer_password",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// registrado => /registrado(.:format)
  // function(options)
  registrado_path: Utils.route([], ["format"], [2,[2,[7,"/",false],[6,"registrado",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// registro => /registro(.:format)
  // function(options)
  registro_path: Utils.route([], ["format"], [2,[2,[7,"/",false],[6,"registro",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// root => /
  // function(options)
  root_path: Utils.route([], [], [7,"/",false], arguments),
// ruta_flota_create => /ruta_flota/create(.:format)
  // function(options)
  ruta_flota_create_path: Utils.route([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"ruta_flota",false]],[7,"/",false]],[6,"create",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// ruta_flota_create_taxi => /ruta_flota/create/taxi(.:format)
  // function(options)
  ruta_flota_create_taxi_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"ruta_flota",false]],[7,"/",false]],[6,"create",false]],[7,"/",false]],[6,"taxi",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// ruta_flota_destroy => /ruta_flota/destroy/:id(.:format)
  // function(id, options)
  ruta_flota_destroy_path: Utils.route(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"ruta_flota",false]],[7,"/",false]],[6,"destroy",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// ruta_flota_edit => /ruta_flota/edit/:id(.:format)
  // function(id, options)
  ruta_flota_edit_path: Utils.route(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"ruta_flota",false]],[7,"/",false]],[6,"edit",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// ruta_flota_edit_taxi => /ruta_flota/edit_taxi/:id(.:format)
  // function(id, options)
  ruta_flota_edit_taxi_path: Utils.route(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"ruta_flota",false]],[7,"/",false]],[6,"edit_taxi",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// ruta_flota_guardar_parada => /ruta_flota/guardar/parada(.:format)
  // function(options)
  ruta_flota_guardar_parada_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"ruta_flota",false]],[7,"/",false]],[6,"guardar",false]],[7,"/",false]],[6,"parada",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// ruta_flota_index => /ruta_flota/index(.:format)
  // function(options)
  ruta_flota_index_path: Utils.route([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"ruta_flota",false]],[7,"/",false]],[6,"index",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// ruta_flota_new => /ruta_flota/new(.:format)
  // function(options)
  ruta_flota_new_path: Utils.route([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"ruta_flota",false]],[7,"/",false]],[6,"new",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// ruta_flota_new_taxi => /ruta_flota/new_taxi(.:format)
  // function(options)
  ruta_flota_new_taxi_path: Utils.route([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"ruta_flota",false]],[7,"/",false]],[6,"new_taxi",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// ruta_flota_nueva_parada => /ruta_flota/nueva/parada(.:format)
  // function(options)
  ruta_flota_nueva_parada_path: Utils.route([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"ruta_flota",false]],[7,"/",false]],[6,"nueva",false]],[7,"/",false]],[6,"parada",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// ruta_flota_show => /ruta_flota/show/:id(.:format)
  // function(id, options)
  ruta_flota_show_path: Utils.route(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"ruta_flota",false]],[7,"/",false]],[6,"show",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// ruta_flota_update => /ruta_flota/update/:id(.:format)
  // function(id, options)
  ruta_flota_update_path: Utils.route(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"ruta_flota",false]],[7,"/",false]],[6,"update",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// ruta_flota_update_taxi => /ruta_flota/update_taxi/:id(.:format)
  // function(id, options)
  ruta_flota_update_taxi_path: Utils.route(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"ruta_flota",false]],[7,"/",false]],[6,"update_taxi",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// seguro_create => /seguro/create(.:format)
  // function(options)
  seguro_create_path: Utils.route([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"seguro",false]],[7,"/",false]],[6,"create",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// seguro_destroy => /seguro/destroy/:id(.:format)
  // function(id, options)
  seguro_destroy_path: Utils.route(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"seguro",false]],[7,"/",false]],[6,"destroy",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// seguro_edit => /seguro/edit/:id(.:format)
  // function(id, options)
  seguro_edit_path: Utils.route(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"seguro",false]],[7,"/",false]],[6,"edit",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// seguro_index => /seguro/index(.:format)
  // function(options)
  seguro_index_path: Utils.route([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"seguro",false]],[7,"/",false]],[6,"index",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// seguro_new => /seguro/new(.:format)
  // function(options)
  seguro_new_path: Utils.route([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"seguro",false]],[7,"/",false]],[6,"new",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// seguro_show => /seguro/show/:id(.:format)
  // function(id, options)
  seguro_show_path: Utils.route(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"seguro",false]],[7,"/",false]],[6,"show",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// seguro_update => /seguro/update/:id(.:format)
  // function(id, options)
  seguro_update_path: Utils.route(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"seguro",false]],[7,"/",false]],[6,"update",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// servicio_create => /servicio/create(.:format)
  // function(options)
  servicio_create_path: Utils.route([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"servicio",false]],[7,"/",false]],[6,"create",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// servicio_destroy => /servicio/destroy/:id(.:format)
  // function(id, options)
  servicio_destroy_path: Utils.route(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"servicio",false]],[7,"/",false]],[6,"destroy",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// servicio_edit => /servicio/edit/:id(.:format)
  // function(id, options)
  servicio_edit_path: Utils.route(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"servicio",false]],[7,"/",false]],[6,"edit",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// servicio_end_carga => /servicio/end(.:format)
  // function(options)
  servicio_end_carga_path: Utils.route([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"servicio",false]],[7,"/",false]],[6,"end",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// servicio_index => /servicio/index(.:format)
  // function(options)
  servicio_index_path: Utils.route([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"servicio",false]],[7,"/",false]],[6,"index",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// servicio_new => /servicio/new(.:format)
  // function(options)
  servicio_new_path: Utils.route([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"servicio",false]],[7,"/",false]],[6,"new",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// servicio_show => /servicio/show/:id(.:format)
  // function(id, options)
  servicio_show_path: Utils.route(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"servicio",false]],[7,"/",false]],[6,"show",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// servicio_update => /servicio/update/:id(.:format)
  // function(id, options)
  servicio_update_path: Utils.route(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"servicio",false]],[7,"/",false]],[6,"update",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// session_user_confirmation => /session_users/confirmation(.:format)
  // function(options)
  session_user_confirmation_path: Utils.route([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"session_users",false]],[7,"/",false]],[6,"confirmation",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// session_user_password => /session_users/password(.:format)
  // function(options)
  session_user_password_path: Utils.route([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"session_users",false]],[7,"/",false]],[6,"password",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// session_user_registration => /session_users(.:format)
  // function(options)
  session_user_registration_path: Utils.route([], ["format"], [2,[2,[7,"/",false],[6,"session_users",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments),
// session_user_session => /session_users/sign_in(.:format)
  // function(options)
  session_user_session_path: Utils.route([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"session_users",false]],[7,"/",false]],[6,"sign_in",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments)}
;
    root.Routes.options = defaults;
    return root.Routes;
  };

  if (typeof define === "function" && define.amd) {
    define([], function() {
      return createGlobalJsRoutesObject();
    });
  } else {
    createGlobalJsRoutesObject();
  }

}).call(this);
