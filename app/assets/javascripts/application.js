// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//= require jquery
//= require jquery.turbolinks
//= require jquery_ujs
//= require bootstrap
//= require extras/bootstrapValidator
//= require lightbox/js/lightbox.min.js
//= require plugins/jasny/jasny-bootstrap
//= require input-mask/jquery.inputmask.js
//= require input-mask/jquery.inputmask.regex.extensions
//= require plugins/datapicker/bootstrap-datepicker
//= require jquery-dynamic-selectable
//= require turbolinks
//= require_tree .
jQuery(document).ready(function($) {

    $("a[rel=tooltip]").tooltip();
});

$('form[data-remote="true"]')
.on('ajax:before', function(e) {
    var bv = $(e.target).data('bootstrapValidator');
    return (typeof bv == 'undefined') || bv.isValid();
}).on('success.form.bv error.form.bv', function(e) {
    e.preventDefault();
});



