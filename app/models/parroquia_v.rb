# == Schema Information
#
# Table name: parroquia_vs
#
#  id           :integer          not null, primary key
#  nombre       :string(255)
#  municipio_id :integer
#  created_at   :datetime
#  updated_at   :datetime
#

class ParroquiaV < ActiveRecord::Base
  belongs_to :municipio
  has_many :operadora_transporte_ps
  has_many :paradas
end
