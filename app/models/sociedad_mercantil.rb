# == Schema Information
#
# Table name: sociedad_mercantils
#
#  id         :integer          not null, primary key
#  codigo     :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class SociedadMercantil < ActiveRecord::Base
  has_many :operadora_transporte_ps

  validates :codigo, presence: { message: 'El código de la sociedad mercantil es requerido'}

  def ca?
    codigo == 'C.A.'
  end

  def srl?
    codigo == 'S.R.L.'
  end

  def sa?
    codigo == 'S.A.'
  end

  def puede_arrendar?
    ca? || srl?|| sa?
  end
end


