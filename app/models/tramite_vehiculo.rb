# == Schema Information
#
# Table name: tramite_vehiculos
#
#  id                         :integer          not null, primary key
#  NRO_TRAMITE                :string
#  PLACA                      :string
#  SERIAL_CARROCERIA          :string
#  ID_COLOR1                  :string
#  ID_COLOR2                  :string
#  ID_USO                     :string
#  ID_TIPO_VEHICULO           :string
#  ID_CATEGORIA               :string
#  ID_CLASE                   :string
#  SERIAL_MOTOR               :string
#  SERIAL_CHASSIS             :string
#  PLACA_ANTERIOR             :string
#  NRO_EJES                   :integer
#  NRO_PUESTOS                :integer
#  NRO_IDENTIFICACION_RESERVA :string
#  MODELO                     :string
#  ANO                        :integer
#  CAPACIDAD                  :integer
#  NRO_PROPIETARIO            :string
#  marca_id                   :integer
#  uso_id                     :integer
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#

class TramiteVehiculo < ActiveRecord::Base
  self.establish_connection :inttt_database
  #self.table_name = "TRAMITE_VEHICULO"

  def self.find_by_placa_carroceria_ano(placa, s_carroceria)
    vehiculo = TramiteVehiculo.find_by(PLACA: placa.upcase, SERIAL_CARROCERIA: s_carroceria.upcase)
    # self.connection.disconnect!
    return vehiculo
  end
end
