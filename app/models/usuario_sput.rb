# -*- encoding : utf-8 -*-
# == Schema Information
#
# Table name: usuario_sputs
#
#  id               :integer          not null, primary key
#  nombre           :string(60)       not null
#  apellido         :string(50)       not null
#  nacionalidad     :string(1)        not null
#  numero_doc       :string(8)        not null
#  telefono_local   :string(10)
#  movil            :string(10)       not null
#  correo_alterno   :string(50)
#  pregunta_secreta :string(30)       not null
#  respuesta        :string(15)       not null
#  created_at       :datetime
#  updated_at       :datetime
#

class UsuarioSput < ActiveRecord::Base
  include RegexHelper
  # establish_connection :sput_development

  #asociaciones
  has_one :session_user
  has_one :usuario
  accepts_nested_attributes_for :session_user
  accepts_nested_attributes_for :usuario

  #validates
  before_validation do
    self.telefono_local =
        telefono_local.gsub(/[\(\)-]/, "") if attribute_present? ("telefono_local")
  end
  validates :nombre , presence: { message: 'Nombre es requerido'},
            format: {with: SOLO_CARACTERES_REGEX,
                     message: 'Nombre no cumple con Formato' },
            :length => {  maximum:  60,
                          message:
                              'Nombre debe contener máximo 60 caracteres'
            }
  validates :apellido ,  presence: { message: 'Apellido es requerido'},
            format: {with: SOLO_CARACTERES_REGEX,
                     message: 'Apellido no cumple con Formato' },
            :length => {  maximum: 50,
                          message:
                              'Apellido debe contener máximo 50 caracteres'
            }
  validates  :nacionalidad , presence: {message: 'Nacionalidad requerida'},
             format: {with: NACIONALIDAD_REGEX,
                      message: 'Nacionalidad no cumple con el formato (V ó E)' }

  validates :numero_doc , presence: {message: 'Número Cédula es Requerida'},
            format: {with: CEDULA_REGEX,
                     message: 'Número Cédula no cumple con el formato'},
            :length => {  maximum: 8,
                          message:
                              'Apellido debe contener máximo 50 caracteres'
            }
  validates :telefono_local ,
            format: {with: TELEFONO_LOCAL_REGEX,
                     message: 'Teléfono local incorrecto',
                     :allow_blank => true
            }
  validates :movil,
            format: {with: MOVIL_REGEX,
                     message: 'Teléfono Móvil incorrecto'},
            presence: {message: 'Teléfono Móvil es Requerido'},
            :length => {  is: 10,
                          message:
                              'Debe introducir 7 dígitos en Teléfono Móvil  '
            }
  validates :correo_alterno,
            format: {with: VALID_EMAIL_REGEX,
                     message: 'Correo Alterno no cumple con formato de correo'}, :allow_blank => true,
            :length => {  maximum: 30,
                          message:
                              'Correo Alterno debe contener máximo 30 caracteres'
            }
  validates :pregunta_secreta,
            format: {with: ALFANUMERICO_REGEX,
                     message: 'Pregunta secreta no cumple con formato alfanumérico'},
            presence: {message: 'Pregunta secreta es Requerida'},
            :length => {  maximum: 30,
                          message:
                              'Pregunta secreta debe contener máximo 28 carácteres'
            }
  validates :respuesta,
            format: {with: ALFANUMERICO_REGEX,
                     message: 'Respuesta secreta no cumple con formato alfanumérico'},
            presence: {message: 'Respuesta secreta es Requerida'},
            :length => {  maximum: 15,
                          message:
                              'Respuesta secreta debe contener máximo 15 carácteres'
            }

end

