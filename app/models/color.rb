# == Schema Information
#
# Table name: colors
#
#  id           :integer          not null, primary key
#  ID_USO       :string
#  NOMBRE_COLOR :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class Color < ActiveRecord::Base
  self.establish_connection :inttt_database
  #self.table_name = "COLOR"

  def self.color(id_color)
    color = self.find_by(ID_COLOR: id_color)
    return color.nil? ? 'NULL': color.NOMBRE_COLOR
  end

end
