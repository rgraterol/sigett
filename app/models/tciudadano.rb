# -*- encoding : utf-8 -*-
# == Schema Information
#
# Table name: tciudadano
#
#  dnacionalidad :string(1)        not null
#  ccedula       :integer          not null, primary key
#  dnombre_1     :string(30)
#  dnombre_2     :string(30)
#  dapellido_1   :string(25)
#  dapellido_2   :string(25)
#  ffecha_nac    :datetime
#  dsexo         :string(1)
#

class Tciudadano < ActiveRecord::Base
  def self.find_cedula(cedula)
    self.establish_connection :saime
    self.pluralize_table_names = false
    persona = self.find_by(ccedula: cedula)
    self.connection.disconnect!
    return persona
  end

end

