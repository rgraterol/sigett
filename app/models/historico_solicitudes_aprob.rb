# -*- encoding : utf-8 -*-
# == Schema Information
#
# Table name: historico_solicitudes_aprobs
#
#  id                      :integer          not null, primary key
#  tipo_certificacion      :string(255)
#  descripcion             :string(255)
#  fecha_expedicion        :date
#  documento_historico     :string(255)
#  estatus_historico       :string(255)      default("Aprobadas")
#  servicio_operadora_c_id :integer
#  created_at              :datetime
#  updated_at              :datetime
#

class HistoricoSolicitudesAprob < ActiveRecord::Base
  include RegexHelper
  belongs_to :servicio_operadora_c
  enum tipo_certificacion: {
                            renovacion_de_cps: "Renovación de la CPS",
                            aumento_de_cupo: "Aumento de Cupo",
                            cambio_de_turnos_horas: "Cambio de Turnos Horas",
                            cambio_de_rutas: "Cambio de Rutas",
                            fusion_de_empresas: "Fusión de Empresas",
                            cambio_de_razon_social: "Cambio de Razón Social"
                            }
  enum estatus_historico: {
            aprobadas: "Aprobadas",
            tramitadas: "Tramitadas",
            rechazadas: "Rechazadas"
  }
  mount_uploader :documento_historico, DocumentoHistoricoUploader

  validates :tipo_certificacion, presence: { message: 'Tipo de certificación es requerido'}
  validates :fecha_expedicion, presence: { message: 'La fecha de expedición es requerido'}
  # validates :fecha_expedicion, format: {with: FECHA_REGEX, message: 'Formato de fecha no válido, debe ingresar un fecha válida, Ej. dd/mm/aaaa'}
  validates :documento_historico, presence: { message: 'Archivo de imágen o documento es requerido'}
  validates :documento_historico, format: {with: DOC_REGEX, message: 'El formato del archivo subido no está permitido, formatos permitidos:  .jpg, .jpeg, .gif, .png y .pdf'}
  validates :descripcion, presence: { message: 'La Descripción es requerido'}
  validates :descripcion, length: { in: 1..60, message: 'Descripción debe tener un maximo de 60 caracteres'}
  validate  :hasta_dia_actual?


  def tipo_certificacion_val
    if self.tipo_certificacion.nil?
    else
      HistoricoSolicitudesAprob.tipo_certificacions[self.tipo_certificacion]
    end
  end


  def hasta_dia_actual?
    if self.fecha_expedicion >= Date.today
      errors.add(:fecha_expedicion, 'La Fecha de expedición debe ser anterior a la fecha actual')
    end
  end

end

