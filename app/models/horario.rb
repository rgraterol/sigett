# -*- encoding : utf-8 -*-
# == Schema Information
#
# Table name: horarios
#
#  id          :integer          not null, primary key
#  hora        :time
#  ida_regreso :boolean
#  tipo        :string(255)
#  dia         :string(255)
#  ruta_id     :integer
#  created_at  :datetime
#  updated_at  :datetime
#

class Horario < ActiveRecord::Base
  belongs_to :ruta
  enum dia: {lunes: "Lunes",
             martes: "Martes",
             miercoles: "Miercoles",
             jueves: "Jueves",
             viernes: "Viernes",
             sabado: "Sabado",
             domingo: "Domingo",
             todos_los_dias: "Todos los Dias",
             fin_semana: "Fines de Semana"}
  # enum ida: {ida: 0, regreso: 1}


end
