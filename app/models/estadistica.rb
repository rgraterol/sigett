class Estadistica < ActiveRecord::Base
  include VistaBd

  def self.modalidad_estado(estado=nil)
    # begin
    #   nil.any?
      if estado.nil? || estado == ""
        if estado == ""
          return "", nil, nil
        else
          estados = Array.new
          valor, total = self.calculo_estadistica_modalidad(estado)
          Estado.all.each do |a|
            estados << a.nombre
          end
        end
      else
        estados = Array.new
        valor, total = self.calculo_estadistica_modalidad(estado)
        Municipio.where(estado_id: estado).each do |a|
          estados << a.nombre
        end
      end
    # rescue ActiveRecord::RecordNotFound
    #   return "true", nil, nil
    # rescue ActiveRecord::ActiveRecordError
    #   return "false", nil, nil
    # rescue Exception
    #   #Only comes in here if nothing else catches the error
    #   return nil, nil, nil
    # end
    return valor, total, estados
  end

  def self.modalidad_cupo_autorizado(estado=nil)
    if estado.nil?
      estadisticas = Array.new(Estado.all.count)
      return self.calculo_est_cupo_autorizado(estadisticas)
    else #AUN NO FUNCIONA MUNICIPIO
      estadisticas = Array.new(Municipio.where(estado_id: estado).count)
      return self.calculo_est_cupo_autorizado(estadisticas, estado)
    end
  end

  def self.calculo_est_cupo_autorizado(estadisticas, estado=nil)
    if estado.nil?
      total = Array.new(ServicioOperadoraC.modalidad_servicios.count)
      modalidad = Array.new(ServicioOperadoraC.modalidad_servicios.count)
      opera = OperadoraTransporteP.all
      return self.cal_cupo_autorizado_estado(opera, modalidad, estadisticas, total)
    else
      total = Array.new(ServicioOperadoraC.modalidad_servicios.count)
      modalidad = Array.new(ServicioOperadoraC.modalidad_servicios.count)
      parroquia = ParroquiaV.joins(:municipio).where(municipios: {estado_id: estado})
      opera = estado.nil? ? OperadoraTransporteP.all : OperadoraTransporteP.where(parroquia_v_id: parroquia)
      estadisticas = opera.any? ? estadisticas : ""
      return self.cal_opt_municipio(opera, modalidad, estadisticas, total)
    end
  end

  def self.cal_cupo_autorizado_estado(opera, modalidad, estadisticas, total)
    total_ = Array.new(total)
    tupla = Array.new(2,0)
    counter = 0
    opera.each do |opt|
      if opt.servicio_operadora_cs.any?
        opt.servicio_operadora_cs.each do |serv|
          indice = self.pos_on_modalidad(serv.modalidad_servicio.to_s)
          unless indice.nil?
            tupla[0] = serv.flota.cupo_asignado
            tupla[1] = serv.flota.vehiculos.count
            suma = modalidad[self.pos_on_modalidad(serv.modalidad_servicio)].nil? ? tupla : self.sum_array(modalidad[self.pos_on_modalidad(serv.modalidad_servicio)], tupla)
            modalidad[self.pos_on_modalidad(serv.modalidad_servicio)] = suma
            counter += 1
            tupla = Array.new(2,0)
          end
        end

        suma = estadisticas[opt.parroquia_v.municipio.estado.id.to_i - 1].nil? ? modalidad : self.sum_array_array(estadisticas[opt.parroquia_v.municipio.estado.id.to_i - 1], modalidad)
        estadisticas[opt.parroquia_v.municipio.estado.id.to_i - 1] = suma
        suma = nil
        total_ = self.sum_array_array(total_, modalidad)
        modalidad = Array.new(ServicioOperadoraC.modalidad_servicios.count)
      else
      end
    end
    estados = Array.new
    Estado.all.each do |a|
      estados << a.nombre
    end
    return estadisticas, total_, estados, estados
  end

  def self.sum_array_array(array1_=[], array2_=[])
    if array1_.length == array2_.length
      array1 = []
      array2 = []
      array1 = array1_
      array2 = array2_
      arra = Array.new(array1.length)
      (0..array1.length - 1).each do |i|
        sum = array1[i].nil? ? (array2[i].nil? ? nil : array2[i]) : (array2[i].nil? ? array1[i] : self.sum_array(array1[i], array2[i]))
        arra[i] = sum
      end
      return arra
    else
      return nil
    end
  end

  def self.calculo_estadistica_modalidad(estado=nil)
    if estado.nil?
      estadisticas = Array.new(Estado.all.count)
      return self.calculo_est_operadora(estadisticas)
    else
      estadisticas = Array.new(Municipio.where(estado_id: estado).count)
      return self.calculo_est_operadora(estadisticas, estado)
    end
  end

  def self.pos_on_modalidad(modalidad=nil)
    lista = {}
    counter = 0
    ServicioOperadoraC.modalidad_servicios.each do |sym_, val|
      lista[sym_] = counter
      counter += 1
    end
    modalidad.nil? ? nil : lista[modalidad]
  end

  def self.pos_on_modalidad_val(modalidad=nil)
    lista = {}
    counter = 0
    ServicioOperadoraC.modalidad_servicios.each do |sym_, val|
      lista[val] = counter
      counter += 1
    end
    modalidad.nil? ? nil : lista[modalidad]
  end

  def self.pos_on_tipounidad_val(tipo=nil)
    lista = {}
    counter = 0
    Vehiculo.tipo_unidads.each do |sym_, val|
      lista[val] = counter
      counter += 1
    end
    tipo.nil? ? nil : lista[tipo]
  end

  def self.pos_on_tipo_certificacion(cert=nil)
    lista = {}
    counter = 0
    HistoricoSolicitudesAprob.tipo_certificacions.each do |sym_, val|
      lista[sym_] = counter
      counter += 1
    end
    cert.nil? ? nil : lista[cert]
  end

  def self.pos_on_estatus_historico(est=nil)
    lista = {}
    counter = 0
    HistoricoSolicitudesAprob.estatus_historicos.each do |sym_, val|
      lista[sym_] = counter
      counter += 1
    end
    est.nil? ? nil : lista[est]
  end


  def self.sum_array(array1 = [], array2 = [])
    array1_ = []
    array2_ = []
    array1_ = array1
    array2_ = array2
    if array1_.length == array2_.length
      suma = Array.new(array1_.length)
      (0..array1_.length - 1).each do |i|
        suma[i] = array1_[i] + array2_[i]
      end
      return suma
    else
      return nil
    end
  end

  def self.calculo_est_operadora(estadisticas, estado=nil)
    total = Array.new(ServicioOperadoraC.modalidad_servicios.count, 0)
    modalidad = Array.new(ServicioOperadoraC.modalidad_servicios.count, 0)
    if estado.nil?
      opera = OperadoraTransporteP.all
      estadisticas = estadisticas
      return self.cal_opt_estado(opera, modalidad, estadisticas, total)
    else
      parroquia = ParroquiaV.joins(:municipio).where(municipios: {estado_id: estado})
      opera = estado.nil? ? OperadoraTransporteP.all : OperadoraTransporteP.where(parroquia_v_id: parroquia)
      estadisticas = opera.any? ? estadisticas : ""
      return self.cal_opt_municipio(opera, modalidad, estadisticas, total)
    end
  end

  def self.cal_opt_estado(opera, modalidad, estadisticas, total)
    counter = 0
    opera.each do |opt|
      if opt.servicio_operadora_cs.any?
        opt.servicio_operadora_cs.each do |serv|
          indice = self.pos_on_modalidad(serv.modalidad_servicio.to_s)
          unless indice.nil?
            modalidad[self.pos_on_modalidad(serv.modalidad_servicio)] += 1
            counter += 1
          end
        end
        suma = estadisticas[opt.parroquia_v.municipio.estado.id.to_i - 1].nil? ? modalidad : self.sum_array(estadisticas[opt.parroquia_v.municipio.estado.id.to_i - 1], modalidad)
        estadisticas[opt.parroquia_v.municipio.estado.id.to_i - 1] = suma
        total = self.sum_array(total, modalidad)
        modalidad = Array.new(ServicioOperadoraC.modalidad_servicios.count, 0)
      else
      end
    end
    return estadisticas, total
  end

  def self.cal_opt_municipio(opera, modalidad, estadisticas, total)
    counter = 0
    opera.each do |opt|
      if opt.servicio_operadora_cs.any?
        opt.servicio_operadora_cs.each do |serv|
          indice = self.pos_on_modalidad(serv.modalidad_servicio.to_s)
          unless indice.nil?
            modalidad[self.pos_on_modalidad(serv.modalidad_servicio)] += 1
            counter += 1
          end
        end
        suma = estadisticas[opt.parroquia_v.municipio.id.to_i - 1].nil? ? modalidad : self.sum_array(estadisticas[opt.parroquia_v.municipio.id.to_i - 1], modalidad)
        count_h = estadisticas.length
        estadisticas[(opt.parroquia_v.municipio.id.to_i % count_h).to_i - 1] = suma
        total = self.sum_array(total, modalidad)
        modalidad = Array.new(ServicioOperadoraC.modalidad_servicios.count, 0)
      else
      end
    end
    return estadisticas, total
  end

  def self.operadoras_estado(estado=nil)

  end

  def self.vencimiento_flota(fecha=nil)
    if fecha.nil?
      return "", nil, nil, nil
    else
      return self.calc_est_vencimiento_flota(fecha)
    end
  end

  def self.vencimiento_cps(fecha=nil)
    if fecha.nil?
      return "", nil, nil, nil
    else
      return self.calc_est_vencimiento_cps(fecha)
    end
  end

  def self.calc_est_vencimiento_flota(fecha)
    estadisticas = Array.new(Estado.all.count)
    mode = Array.new(12,0)
    total = Array.new(12,0)
    OperadoraTransporteP.all.each do |opt|
      opt.servicio_operadora_cs.each do |serv|
        if serv.flota.fecha_vence_autorizacion.year.to_i == fecha.to_i
          mode[serv.flota.fecha_vence_autorizacion.month.to_i - 1] += 1
        end
      end
      if verificar_estadisticas_cero(mode)
        sum = estadisticas[opt.parroquia_v.municipio.estado.id.to_i - 1].nil? ? mode : self.sum_array(estadisticas[opt.parroquia_v.municipio.estado.id.to_i - 1], mode)
        estadisticas[opt.parroquia_v.municipio.estado.id.to_i - 1] = sum
      end
      total = self.sum_array(total, mode)
      mode = Array.new(12,0)
    end
    estados = Array.new
    Estado.all.each do |a|
      estados << a.nombre
    end
    return self.verificar_estadisticas_nil(estadisticas), total, self.crear_array_meses, estados
  end

  def self.calc_est_vencimiento_cps(fecha)
    estadisticas = Array.new(Estado.all.count)
    mode = Array.new(12,0)
    total = Array.new(12,0)
    OperadoraTransporteP.all.each do |opt|
      opt.servicio_operadora_cs.each do |serv|
        if serv.fecha_vencimiento.year.to_i == fecha.to_i
          mode[serv.fecha_vencimiento.month.to_i - 1] += 1
        end
      end
      if verificar_estadisticas_cero(mode)
        sum = estadisticas[opt.parroquia_v.municipio.estado.id.to_i - 1].nil? ? mode : self.sum_array(estadisticas[opt.parroquia_v.municipio.estado.id.to_i - 1], mode)
        estadisticas[opt.parroquia_v.municipio.estado.id.to_i - 1] = sum
      end
      total = self.sum_array(total, mode)
      mode = Array.new(12,0)
    end
    estados = Array.new
    Estado.all.each do |a|
      estados << a.nombre
    end
    return self.verificar_estadisticas_nil(estadisticas), total, self.crear_array_meses, estados
  end

  def self.crear_array_meses(start_=1,end_=12)
    meses = Array.new
    meses << "Enero" << "Febrero" << "Marzo" <<
        "Abril" << "Mayo" << "Junio" << "Julio" <<
        "Agosto" << "Septiembre" << "Octubre" << "Noviembre" << "Diciembre"
    return meses[(start_ - 1)..(end_ - 1)]
  end

  def self.crear_array_meses_start(start_=1, meses=5)
    array = []
    date = Date.new(2014,start_,1)
    (0..(meses-1)).each do |a|
      array << self.crear_array_meses((date + a.months).month.to_i,(date + a.months).month.to_i)[0]
    end
    array
  end

  def self.verificar_estadisticas_nil(estadisticas)
    counter = 0
    estadisticas.each do |a|
      if a.nil?
        counter += 1
      end
    end
    return counter == estadisticas.length ? "" : estadisticas
  end

  def self.verificar_estadisticas_cero(estadisticas)
    counter = 0
    estadisticas.each do |a|
      if a == 0
        counter += 1
      end
    end
    return counter == estadisticas.length ? false : true
  end

  def self.verificar_estadisticas_bidimensional_cero(estadisticas)
    counter = 0
    puts estadisticas.size.inspect
    dimension = estadisticas.size * estadisticas[0].size
    estadisticas.each {
         |a| a.each {
           |y| if y == 0
                 counter +=1
               end
                 }
    }
    return counter == dimension ? true : false
  end


  def self.proximas_vencer_flota(meses=5)
    estadisticas = Array.new(Estado.all.count)
    mode = Array.new(meses,0)
    total = Array.new(meses,0)

    OperadoraTransporteP.all.each do |opt|
      opt.servicio_operadora_cs.each do |serv|
        fecha = serv.flota.fecha_vence_autorizacion
        if (fecha >= Date.today) && (fecha <= Date.today + (meses - 1).months)
          mode[self.month_pos_cinco(serv.flota.fecha_vence_autorizacion, meses)] += 1
        end
      end
      if verificar_estadisticas_cero(mode)
        puts meses
        puts mode.inspect
        puts estadisticas.inspect
        sum = estadisticas[opt.parroquia_v.municipio.estado.id.to_i - 1].nil? ? mode : self.sum_array(estadisticas[opt.parroquia_v.municipio.estado.id.to_i - 1], mode)
        estadisticas[opt.parroquia_v.municipio.estado.id.to_i - 1] = sum
      end
      total = self.sum_array(total, mode)
      mode = Array.new(meses,0)
    end
    estados = Array.new
    Estado.all.each do |a|
      estados << a.nombre
    end
    return self.verificar_estadisticas_nil(estadisticas), total, self.crear_array_meses_start(Date.today.month.to_i, meses), estados
  end
  # Estadistica.month_pos_cinco
  def self.month_pos_cinco(fecha, meses=4)
    date = Date.today
    (0..meses).each do |m|
      if fecha >= (date + m.months) && fecha < (date + (m + 1).month)
        return m
      end
    end
  end

  def self.calc_vistas_estados
    estadisticas = Array.new(Estado.all.count) { Array.new(ServicioOperadoraC.modalidad_servicios.count, 0)}
    totales = Array.new(ServicioOperadoraC.modalidad_servicios.count, 0)
    VistaEstadosModalidad.all.each do |e|
      estadisticas[e.estado_id.to_i - 1][self.pos_on_modalidad_val(e.modalidad_servicio)] = estadisticas[e.estado_id.to_i - 1][self.pos_on_modalidad_val(e.modalidad_servicio)] += 1
      totales[self.pos_on_modalidad_val(e.modalidad_servicio)] += 1
    end
    # imprimir estados
    estados = Array.new
    Estado.all.each do |a|
      estados << a.nombre
    end
    # imprimir modalidad
    modalidad = Array.new
    ServicioOperadoraC.modalidad_servicios.each do |sym_, val|
      modalidad << val
    end
    puts modalidad.inspect
    return  estados, modalidad, estadisticas, totales
  end

  def self.calc_vistas_tipo_unidad(estado_id)
    estadisticas = Array.new(Vehiculo.tipo_unidads.count) { Array.new(ServicioOperadoraC.modalidad_servicios.count, 0)}
    VistaModalidadTipoUnidad.all.each do |e|
      if Estado.find_by(id: estado_id).id == e.estado_id
        estadisticas[self.pos_on_tipounidad_val(e.tipo_unidad)][self.pos_on_modalidad_val(e.modalidad_servicio)] = estadisticas[self.pos_on_tipounidad_val(e.tipo_unidad)][self.pos_on_modalidad_val(e.modalidad_servicio)] += 1
      end
    end
    # imprimir tipo unidad
    tipo = Array.new
    Vehiculo.tipo_unidads.each do |s, v|
      tipo << v
    end
    # imprimir modalidad
    modalidad = Array.new
    ServicioOperadoraC.modalidad_servicios.each do |sym_, val|
      modalidad << val
    end
    return  tipo, modalidad, self.verificar_estadisticas_bidimensional_cero(estadisticas) ? "" : estadisticas
  end

  def self.calc_tramites_historicos(mes)
    estadisticas = Array.new(HistoricoSolicitudesAprob.tipo_certificacions.count) { Array.new(HistoricoSolicitudesAprob.estatus_historicos.count, 0)}
    HistoricoSolicitudesAprob.all.each do |h|
      m = h.fecha_expedicion.strftime('%m')
      if mes.to_i == m.to_i
        estadisticas[self.pos_on_tipo_certificacion(h.tipo_certificacion)][self.pos_on_estatus_historico(h.estatus_historico)] = estadisticas[self.pos_on_tipo_certificacion(h.tipo_certificacion)][self.pos_on_estatus_historico(h.estatus_historico)] += 1
        puts estadisticas.inspect
      end
    end
    # puts estadisticas.inspect
    # imprimir tipo de certificacion
    certificacion = Array.new
    HistoricoSolicitudesAprob.tipo_certificacions.each do |sym_, val|
      certificacion << val
    end
    # imprimir estatus
    estatus = Array.new
    HistoricoSolicitudesAprob.estatus_historicos.each do |s, v|
      estatus << v
    end
    return certificacion, estatus, self.verificar_estadisticas_bidimensional_cero(estadisticas) ? "" : estadisticas
  end

  def self.calc_vistas_mod_parroquias( municipio_id)
    parr = ParroquiaV.where(municipio_id: municipio_id)
    dimension = parr.count
    estadisticas = Array.new(dimension,0) { Array.new(ServicioOperadoraC.modalidad_servicios.count, 0)}
    totales = Array.new(ServicioOperadoraC.modalidad_servicios.count, 0)
    h = {}
    parr.each_with_index {
        |p, index|  h[p.id] = index
    }
    VistaOperadorasEstado.where(municipio_id: municipio_id).each do |e|
      if dimension > 0
       estadisticas[h[e.parroquia_id].to_i][self.pos_on_modalidad_val(e.modalidad_servicio)] = estadisticas[h[e.parroquia_id].to_i][self.pos_on_modalidad_val(e.modalidad_servicio)] += 1
       totales[self.pos_on_modalidad_val(e.modalidad_servicio).to_i] += 1
      end
    end
    # imprimir parroquias
    parroquias = Array.new
    ParroquiaV.where(municipio_id: municipio_id).each do |p|
      parroquias << p.nombre
    end
    # imprimir modalidad
    modalidad = Array.new
    ServicioOperadoraC.modalidad_servicios.each do |sym_, val|
      modalidad << val
    end
    return parroquias, modalidad, totales, estadisticas == [] ? estadisticas = "" : self.verificar_estadisticas_bidimensional_cero(estadisticas) ? "" : estadisticas
  end

  def self.vistas_mod_ciudad( estado_id)
    ciud = Ciudad.where(estado_id: estado_id)
    dimension = ciud.count
    estadisticas = Array.new(dimension) { Array.new(ServicioOperadoraC.modalidad_servicios.count, 0)}
    totales = Array.new(ServicioOperadoraC.modalidad_servicios.count, 0)
    h = {}
    ciud.each_with_index {
        |p, index|  puts h[p.id] = index
    }
    VistaModalidadCiudad.where(estado_id: estado_id).each do |e|
      estadisticas[h[e.ciudad_id].to_i][self.pos_on_modalidad_val(e.modalidad_servicio)] = estadisticas[h[e.ciudad_id].to_i][self.pos_on_modalidad_val(e.modalidad_servicio)] += 1
      totales[self.pos_on_modalidad_val(e.modalidad_servicio).to_i] += 1
    end
    # imprimir ciudad
    ciudad = Array.new
    Ciudad.where(estado_id: estado_id).each do |c|
      ciudad << c.nombre
    end
    # imprimir modalidad
    modalidad = Array.new
    ServicioOperadoraC.modalidad_servicios.each do |sym_, val|
      modalidad << val
    end
    return ciudad, modalidad, totales, self.verificar_estadisticas_bidimensional_cero(estadisticas) ? "" : estadisticas
  end

end
