# -*- encoding : utf-8 -*-
# == Schema Information
#
# Table name: usuarios
#
#  id                   :integer          not null, primary key
#  estado_representante :integer          default(0), not null
#  rif                  :string(10)       not null
#  cedula_imagen        :string(255)      not null
#  session_user_id      :integer
#  usuario_sput_id      :integer          not null
#

class Usuario < ActiveRecord::Base
  include RegexHelper
  enum estado_representante: [:temporal,:en_proceso,:autorizado]
  mount_uploader :cedula_imagen, CedulaImagenUploader


  #asociaciones
  has_many :operadora_transporte_ps
  belongs_to :usuario_sput
  belongs_to :session_user
  accepts_nested_attributes_for :usuario_sput
  accepts_nested_attributes_for :session_user

  #validates
  validates :session_user_id, :uniqueness => true
  validates :usuario_sput_id, :uniqueness => true
  validates :rif, presence: { message: 'Rif es requerido'},
            format: {with:RIF_REGEX,
                     message: 'Rif no cumple con formato' },
            :length => {  maximum: 10,
                          message:
                              'Rif debe contener máximo 10 caracteres'
            }
  validates :cedula_imagen, presence: { message: 'Imagen de cédula es requerida'}
  validates :usuario_sput_id,uniqueness: {message: 'Error del sistema no duplicados de registro'}
  validates :direccion, presence: {message: 'La dirección es requerida'}

end

