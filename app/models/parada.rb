# -*- encoding : utf-8 -*-
# == Schema Information
#
# Table name: paradas
#
#  id             :integer          not null, primary key
#  nombre         :string(255)
#  coordenada_utm :string(255)
#  avenida        :string(255)
#  calle          :string(255)
#  sector         :string(255)
#  tipo_parada    :string(255)
#  parroquia_v_id :integer
#  ciudad_id      :integer
#  created_at     :datetime
#  updated_at     :datetime
#  estado_id      :integer
#

class Parada < ActiveRecord::Base
  has_many :toques
  belongs_to :parroquia_v
  belongs_to :ciudad
  belongs_to :estado
  before_save :nombre_mayuscula
  before_update :nombre_mayuscula

  enum tipo_parada: {terminal_publico: "Terminal Publico",
                     terminal_privado: "Terminal Privado",
                     zona: "Zona o Parada Terminal"}
  validates :nombre, presence: { message: 'El nombre es requerido'}
  validates :nombre, length: { in: 4..80, message: 'El nombre debe tener entre 4 a 80 caracteres' }
  validates :ciudad_id, presence: { message: 'La ciudad es requerida'}
  validates :estado_id, presence: { message: 'El estado es requerido'}
  validates :parroquia_v_id, presence: { message: 'La parroquia es requerida'}
  # validates :nombre, length: { in: 1..60, message: 'Nombre de la Parada debe tener entre 1 a 60 caracteres'}
  # validates :avenida, length: { in: 1..30, message: 'Avenida debe tener entre 1 a 30 caracteres'}, allow_blank: true
  # validates :calle, length: { in: 1..30, message: 'Calle debe tener entre 1 a 30 caracteres'}, allow_blank: true
  # validates :sector, length: { in: 1..30, message: 'Sector debe tener entre 1 a 30 caracteres'}, allow_blank: true
  def nombre_mayuscula
    self.nombre = self.nombre.split.collect(&:upcase).join(' ') if self.nombre && !self.nombre.blank?
  end
end

