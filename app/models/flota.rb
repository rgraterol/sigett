# -*- encoding : utf-8 -*-
# == Schema Information
#
# Table name: flota
#
#  id                       :integer          not null, primary key
#  cupo_asignado            :integer
#  fecha_autorizacion       :date
#  fecha_vence_autorizacion :date
#  fecha_ultima_renovacion  :date
#  tiene_ruta               :boolean          default(FALSE)
#  servicio_operadora_c_id  :integer
#  created_at               :datetime
#  updated_at               :datetime
#

class Flota < ActiveRecord::Base
  belongs_to :servicio_operadora_c
  has_many :rutas
  has_many :vehiculos
  has_one :seguro_vehiculo
  accepts_nested_attributes_for :rutas

  validates :cupo_asignado, presence: {message: "Cupo autorizado es requerido"}, numericality: true
  validate :fecha_autorizacion_dia_actual?

  before_save :actualizar_fecha_vence_dt
  def actualizar_fecha_vence_dt
    self.fecha_vence_autorizacion = self.fecha_autorizacion + 1.year
  end


  def fecha_autorizacion_dia_actual?
    if self.fecha_autorizacion >= Date.today
      errors.add(:fecha_expedicion, 'La Fecha última de la Autorización de la flota vehicular (DT/9 o DT/10) debe ser anterior a la fecha actual')
    end
  end
end

