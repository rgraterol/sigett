# -*- encoding : utf-8 -*-
# == Schema Information
#
# Table name: toques
#
#  id          :integer          not null, primary key
#  tipo_parada :string(255)
#  tipo        :string(255)
#  ruta_id     :integer
#  next_id     :integer
#  prev_id     :integer
#  parada_id   :integer
#  created_at  :datetime
#  updated_at  :datetime
#

class Toque < ActiveRecord::Base
  belongs_to :ruta
  belongs_to :parada
  # belongs_to :ciudad
  has_one :next, class_name: "Toque", foreign_key: :next_id
  belongs_to :next, class_name: "Toque"
  accepts_nested_attributes_for :parada

  has_one :prev, class_name: "Toque", foreign_key: :prev_id
  belongs_to :prev, class_name: "Toque"

  enum tipo_parada: {origen: "Origen",
                     destino: "Destino",
                     intermedio: "Intermedio"}

  validates :tipo_parada, presence: { message: 'Es necesario insertar el tipo de parada'}
  validates :tipo, presence: { message: 'El Tipo es necesario'}
  validates :parada_id, presence: { message: 'Es necesario seleccionar una parada'}
  validate :parada_id_not_cero

  def parada_id_not_cero
    if self.parada_id == 0
      errors.add(:parada_id, "Es necesario seleccionar una parada" )
    end
  end

  def self.set_new_toque(ruta, params)
    prev = ruta.toques.where(next_id: nil).where.not(id: nil).last
    toque = ruta.toques.build
    toque.prev = prev
    toque.next = nil
    toque.tipo_parada = params[:tipo_parada]
    toque.tipo = params[:tipo]
    toque.parada_id = params[:parada_id]
    return toque
  end

  def self.actualizar_prev_next(toque, valid=true)
    estado_previo = toque.prev
    asd = toque.prev.prev_id
    if toque.save(validate: valid)
      prev = estado_previo
      prev.next_id = toque.id
      prev.prev_id = asd
      prev.save(validate: valid)
      return true
    else
      return false
    end
  end

  def self.delete_toque(toque)
    toque_ant = self.find(toque.prev_id)
    toque_ant.update(next_id: toque.next_id)
    unless toque.next.nil?
      toque_sig = self.find(toque.next_id)
      toque_sig.update(prev_id: toque.prev_id)
    end
    toque.destroy
  end

  def tipo_val
    self.tipo
    # if self.terminal?
    #   return "Terminal"
    # else
    #   if self.ubicacion?
    #     return "Ubicacion"
    #   else
    #     return ""
    #   end
    # end
  end


  def tipo_parada_val
    if self.origen?
      return "Origen"
    else
      if self.destino?
        return "Destino"
      else
        if self.intermedio?
          return "Intermedio"
        else
          return ""
        end
      end
    end
  end

end

