# -*- encoding : utf-8 -*-
# == Schema Information
#
# Table name: operadora_transporte_ps
#
#  id                       :integer          not null, primary key
#  razon_social             :string(255)
#  rif                      :string(255)
#  email                    :string(255)
#  telefono                 :string(255)
#  movil                    :string(255)
#  fax                      :string(255)
#  observacion              :string(255)
#  avenida                  :string(255)
#  calle                    :string(255)
#  urbanizacion             :string(255)
#  edificio_cc              :string(255)
#  local                    :string(255)
#  piso                     :string(255)
#  sociedad_mercantil_otros :string(255)
#  rif_imagen               :string(255)
#  documento_acreditacion   :string(255)
#  ciudad_id                :integer
#  parroquia_v_id           :integer
#  usuario_id               :integer
#  sociedad_mercantil_id    :integer
#  created_at               :datetime
#  updated_at               :datetime
#

class OperadoraTransporteP < ActiveRecord::Base
  before_validation :colocar_mayuscula
  include RegexHelper
  belongs_to :ciudad
  belongs_to :parroquia_v
  belongs_to :usuario
  has_many :servicio_operadora_cs
  belongs_to :sociedad_mercantil
  mount_uploader :rif_imagen, RifImagenUploader
  mount_uploader :documento_acreditacion, DocumentoAcreditacionUploader
  mount_uploader :acta_socios, RifImagenUploader
  mount_uploader :documento_cooperativa, DocumentoAcreditacionUploader

  validates :documento_acreditacion, presence: { message: 'Documentación de acreditación es requerido'}
  validates :acta_socios, presence: { message: 'Última acta de socios es requerido'}
  validates :razon_social, presence: { message: 'Razón social es requerido'}
  validates :razon_social, length: { in: 1..60, message: 'Razón Social debe tener entre 1 a 60 caracteres' }
  validates :razon_social, uniqueness: {message: "Razon Social ya está en uso", case_sensitive: false}
  validates :rif, presence: { message: 'Rif es requerido'}
  validates :rif, format: {with: RIFOPER_REGEX, message: 'Formato de rif no válido, debe ingresar un RIF válido: Ej. J-999999999'}
  validates :rif, uniqueness: {message: "El RIF ya está en uso", case_sensitive: false}
  validates :email, presence: {message: 'Código de operadora es requerido'}
  validates :email,  length: { in: 4..50, message: 'Email debe tener entre 4 a 50 caracteres'}
  validates :email, format: {with: EMAILOPER_REGEX, message: 'Formato de email no válido, debe ingresar un email válido'}
  validates :telefono, format: {with:  TELEFONOOPER_REGEX, message: 'Formato de Teléfono fijo no válido, debe ingresar un teléfono válido: Ej. (999)-9999999'}, allow_blank: true
  validates :fax, format: {with: TELEFONOOPER_REGEX, message: 'Formato de Teléfono fax no válido, debe ingresar un fax válido: Ej. (999)-9999999'}, allow_blank: true
  validates :movil,
            format: {with: MOVIL_REGEX,
                     message: 'Teléfono Móvil incorrecto'},
            presence: {message: 'Teléfono Móvil es Requerido'},
            :length => {  is: 10,
                          message:
                              'Debe introducir 7 dígitos en Teléfono Móvil  '}
  validates :rif_imagen, presence: { message: 'Imágen del RIF es requerido'}
  validates :rif_imagen, format: {with: DOC_REGEX, message: 'El formato del archivo subido no está permitido, formatos para imágen permitidos:  .jpg, .jpeg, .gif, .png pdf'}
  validates :observacion, length: { in: 1..60, message: 'Observación debe tener una longitud maximo de 60 caracteres'},  allow_blank: true
  validates :avenida, length: { in: 1..60, message: 'Avenida debe tener un maximo de 60 caracteres'},  allow_blank: true
  validates :calle, length: { in: 1..60, message: 'Calle debe tener un maximo de 60 caracteres'},  allow_blank: true
  validates :urbanizacion, length: { in: 1..60, message: 'Sector/Urbanización debe tener un maximo de 60 caracteres'},  allow_blank: true
  validates :edificio_cc, length: { in: 1..60, message: 'Edificio debe tener un maximo de 60 caracteres'},  allow_blank: true
  validates :local, length: { in: 1..60, message: 'Local debe tener un maximo de 60 caracteres'},  allow_blank: true
  validates :piso, length: { in: 1..10, message: 'Piso debe tener un maximo de 10 caracteres'},  allow_blank: true

  validate :vacios_ubicacion
  validate :parroquia_id_not_cero

  def parroquia_id_not_cero
    if self.parroquia_v_id == "0" || self.parroquia_v_id.nil? || self.parroquia_v_id == 0
      errors.add(:parroquia_v_id, "Es necesario seleccionar la ubicación completa" )
    end
  end

  def vacios_ubicacion
    if self.calle.blank? and self.avenida.blank? and self.urbanizacion.blank?
      errors.add(:avenida, "Ingrese al menos una avenida, calle o sector/urbanización" )
    end
  end

  def documento_acreditacion_pdf?
    return true if self.documento_acreditacion.to_s.split('.').last == 'pdf'
    return false
  end

  def rif_imagen_pdf?
    return true if self.rif_imagen.to_s.split('.').last == 'pdf'
    return false
  end

  def colocar_mayuscula
    self.rif.upcase!
    self.razon_social.upcase!
  end


end

