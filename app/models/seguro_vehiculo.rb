# -*- encoding : utf-8 -*-
# == Schema Information
#
# Table name: seguro_vehiculos
#
#  id                :integer          not null, primary key
#  rif               :string(255)
#  num_poliza        :string(255)
#  documento_poliza  :string(255)      not null
#  fecha_vencimiento :date
#  aseguradora_id    :integer
#  vehiculo_id       :integer
#  flota_id          :integer
#  created_at        :datetime
#  updated_at        :datetime
#

class SeguroVehiculo < ActiveRecord::Base
  include RegexHelper
  #carga de documentos e imagenes
  mount_uploader :documento_poliza, DocumentoUploader

  #asociaciones
  belongs_to :vehiculo
  belongs_to :flota
  belongs_to :aseguradora

  #validaciones
  validates :aseguradora, presence: {message: 'Debe seleccionar una compañia aseguradora'}
  validates :num_poliza, presence: { message: 'Número de póliza es requerido'}
  validates :num_poliza, length: { in: 1..30, message: 'Número de la póliza debe tener entre 1 a 30 caracteres'}
  validates :rif,
            format: {with: RIFOPER_REGEX,
                     message: 'Formato de rif no válido, debe ingresar un RIF válido: Ej. J-999999999'},
            allow_blank: true
  validates :fecha_vencimiento,
            presence: { message: 'Fecha de vencimiento es requerida'}
  validates :documento_poliza, presence: { message: 'El archivo de la póliza en digital es requerido'}
  # validates :documento_poliza,
  #           allow_blank: true,
  #           format:
  #               {
  #                 with: POLIZA_REGEX,
  #                 message: 'El formato del archivo subido no está permitido, formato permitido: .pdf'
  #               }


end

