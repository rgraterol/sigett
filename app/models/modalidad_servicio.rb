class ModalidadServicio < ActiveRecord::Base
  has_many :servicio_operadora_cs

  has_and_belongs_to_many :tipologias

end
