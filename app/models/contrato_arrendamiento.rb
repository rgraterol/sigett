# -*- encoding : utf-8 -*-
# == Schema Information
#
# Table name: contrato_arrendamientos
#
#  id                  :integer          not null, primary key
#  nombre_arrendatario :string(255)
#  rif_arrendatario    :string(255)
#  fecha               :date
#  duracion            :integer
#  tomo                :integer
#  folio               :integer
#  notaria             :string(255)
#  vehiculo_id         :integer
#  created_at          :datetime
#  updated_at          :datetime
#

class ContratoArrendamiento < ActiveRecord::Base
  belongs_to :vehiculo
  include RegexHelper

  validates :duracion, length: {is: 1, message: 'Duración del contrato debe tener sólo 1 carácter'}
  validates :tomo, length: {in: 1..3, message: 'Tomo del contrato debe tener entre 1 a 3 caracteres'}
  validates :folio, length: {in: 1..3 , message: 'Folio del contrato debe tener entre 1 a 3 caracteres'}
  validates :notaria, length: { in: 1..20, message: 'Notaria debe tener entre 1  a 20 caracteres'}
  validates :duracion, presence: {message: 'Nombre de arrendatario es requerido'}
  validates :notaria, presence: {message: 'Notaria es requerido'}
  validates :nombre_arrendatario, length: { in: 1..60, message: 'Nombre del arrendatario debe tener entre 1 a 60 caracteres'}
  validates :rif_arrendatario,
            format: {with: RIFOPER_REGEX, message: 'Formato de rif no válido, debe ingresar un CI/RIF válido: Ej. J-123456789'}
  validates :fecha,
            presence: { message: 'Fecha de vencimiento es requerida en formato válido'}
end

