# -*- encoding : utf-8 -*-
# == Schema Information
#
# Table name: ciudads
#
#  id         :integer          not null, primary key
#  nombre     :string(255)
#  estado_id  :integer
#  created_at :datetime
#  updated_at :datetime
#

class Ciudad < ActiveRecord::Base
  # has_many :toques
  belongs_to :estado
  has_many :paradas
  has_many :operadora_transporte_ps
end

