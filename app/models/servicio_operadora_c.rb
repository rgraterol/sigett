# -*- encoding : utf-8 -*-
# == Schema Information
#
# Table name: servicio_operadora_cs
#
#  id                        :integer          not null, primary key
#  codigo_rotp               :string(255)
#  fecha_expedicion          :date
#  fecha_vencimiento         :date
#  modalidad_servicio        :string(255)
#  completado                :boolean          default(FALSE)
#  operadora_transporte_p_id :integer
#  created_at                :datetime
#  updated_at                :datetime
#

class ServicioOperadoraC < ActiveRecord::Base
  has_many :historico_solicitudes_aprobs
  belongs_to :operadora_transporte_p
  belongs_to :modalidad_servicio
  has_one :flota
  accepts_nested_attributes_for :flota

  mount_uploader :dt, DtUploader
  mount_uploader :cps, CpsUploader
  validates_uniqueness_of :modalidad_servicio_id,
                          scope: :operadora_transporte_p_id, message: "Esta operadora ya tiene este tipo de modalidad registrada"
  validates :cps, presence: { message: 'Documentación de la CPS es requerido'}
  validates :dt, presence: { message: 'Documentación DT/9 ó DT/10 es requerido'}

  # @@list = {
  #     sub_urb_p_puesto: "PS",
  #     sub_urb_colectivo: "CS",
  #     int_urb_p_puesto: "PI",
  #     int_urb_colectivo: "CI",
  #     taxi: "TX",
  #     moto_taxi: "MT",
  #     turismo: "TT",
  #     periferico_i: "PE",
  #     periferico_s: "PE",
  #     escolar: "TE",
  #     alquiler: "TA" ,
  #     ninguno: nil}
  # enum modalidad_servicio: {sub_urb_p_puesto: "Por Puesto SubUrbano",
  #                           sub_urb_colectivo: "Colectivo SubUrbano",
  #                           int_urb_p_puesto: "Por Puesto InterUrbano",
  #                           int_urb_colectivo: "Colectivo InterUrbano",
  #                           taxi: "Taxi",
  #                           moto_taxi: "Moto Taxi",
  #                           turismo: "Turismo",
  #                           periferico_i: "Periferico InterUrbano",
  #                           periferico_s: "Periferico SubUrbano",
  #                           escolar: "Escolar",
  #                           alquiler: "Alquiler de Vehiculo con o sin chofer"}
  #validaciones
  validates :codigo_rotp, presence: {message: 'Código ROTP es requerido'}
  validates :codigo_rotp, uniqueness: {message: 'El Código ROTP ya existe en la base de datos'}
  validates :codigo_rotp, length: { is: 8, message: 'Código ROTP debe tener sólo 8 caracteres'}
  validates :modalidad_servicio_id, presence: { message: 'Modalidad servicio es requerido'}
  validates :fecha_expedicion, presence: { message: 'Fecha de expedición es requerido'}
  validate :fecha_mayor
  validate :codigo_rot
  validate :fecha_expedicion_dia_actual?

  def fecha_mayor
    if self.fecha_vencimiento.nil?
        errors.add(:fecha_vencimiento, "Fecha de vencimiento es requerido")
      else
        if self.fecha_vencimiento != (self.fecha_expedicion + 10.years)
          errors.add(:fecha_vencimiento, "Fecha de vencimiento debe corresponder a diez años de la fecha de expedición")
        end
    end
  end

  def fecha_expedicion_dia_actual?
    if self.fecha_expedicion >= Date.today
      errors.add(:fecha_expedicion, 'La Fecha de expedición debe ser anterior a la fecha actual')
    end
  end



  def sub_urb_p_puesto?
    self.modalidad_servicio.abreviacion == 'sub_urb_p_puesto'
  end

  def sub_urb_colectivo?
    self.modalidad_servicio.abreviacion == 'sub_urb_colectivo'
  end

  def int_urb_p_puesto?
    self.modalidad_servicio.abreviacion == 'int_urb_p_puesto'
  end

  def int_urb_colectivo?
    self.modalidad_servicio.abreviacion == 'int_urb_colectivo'
  end

  def taxi?
    self.modalidad_servicio.abreviacion == 'taxi'
  end

  def moto_taxi?
    self.modalidad_servicio.abreviacion == 'moto_taxi'
  end

  def turismo?
    self.modalidad_servicio.abreviacion == 'turismo'
  end

  def periferico_i?
    self.modalidad_servicio.abreviacion == 'periferico_i'
  end

  def periferico_s?
    self.modalidad_servicio.abreviacion == 'periferico_s'
  end

  def escolar?
    self.modalidad_servicio.abreviacion == 'escolar'
  end

  def alquiler?
    self.modalidad_servicio.abreviacion == 'alquiler'
  end

  def codigo_rot
    if self.modalidad_servicio.nil?
    else
      sub_a = self.operadora_transporte_p.parroquia_v.municipio.estado.codificacion
      sub_b = self.modalidad_servicio.rotp
      codigo = self.codigo_rotp[0,4].upcase
      unless codigo == (sub_b + sub_a)
        errors.add(:codigo_rotp, "El codigo ROTP debe estar relacionado con el estado de ubicación de la operadora y la modalidad del servicio, para este servicio con esta modalidad el codigo debe iniciar con: #{sub_b + sub_a}")
      end
      # puts ServicioOperadoraC.codigo_modalidad(self.modalidad_servicio)
    end
  end

  def modalidad_servicio_val
    self.modalidad_servicio.nombre
  end

  def corresponde_modalidad_tipo?(vehiculo_intt)
    self.modalidad_servicio.tipologias.each do |tipologia|
      if vehiculo_intt.ID_CLASE == tipologia.id_clase
        return true
      end
    end
    return false
  end

  def codigo_modalidad
    self.modalidad_servicio_val + ' - ' + self.codigo_rotp
  end

  # def self.codigo_modalidad(interno="ninguno")
  #   return self.list[interno]
  # end

  # def self.list
  #   @@list
  # end
end

