# -*- encoding : utf-8 -*-
# == Schema Information
#
# Table name: session_users
#
#  id                     :integer          not null, primary key
#  email                  :string(50)       default(""), not null
#  encrypted_password     :string(255)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  confirmation_token     :string(255)
#  confirmed_at           :datetime
#  confirmation_sent_at   :datetime
#  created_at             :datetime
#  updated_at             :datetime
#  usuario_sput_id        :integer          not null
#

class SessionUser < ActiveRecord::Base
  include RegexHelper
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  has_one :usuario
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,:confirmable

  #asociaciones
  belongs_to :usuario_sput
  accepts_nested_attributes_for :usuario_sput

  #validates
  validates :usuario_sput_id, :uniqueness => true
  validates :email,:length => {  maximum: 50,
                                 message:
                                     'Email debe contener máximo 50 caracteres'
                  }
  validates_presence_of :email_confirmation , on: :create
  validates_format_of :email, with: VALID_EMAIL_REGEX
  validates_confirmation_of :email,
                            message: 'Correo y su confirmación no coincide'
  validates :usuario_sput_id,
            uniqueness: {message: 'Error del sistema, no duplicados de registro'}
  validate :correos_diferentes

  def correos_diferentes
    if self.usuario_sput.correo_alterno == self.email
      errors.add(:email, "Debe usar un correo alterno diferente al email principal")
    end
  end

  #metodos consulta
  def operadoras
    self.usuario.operadora_transporte_ps
  end

  def servicios #no funciona aun
    self.operadoras.servicio_operadora_cs
  end

  def servicio_flota(servicio)
    self.operadoras.servicio_operadora_cs.where(id: servicio).last  end


end

