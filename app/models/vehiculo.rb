# -*- encoding : utf-8 -*-
# == Schema Information
#
# Table name: vehiculos
#
#  id                         :integer          not null, primary key
#  placa                      :string(255)
#  s_carroceria               :string(255)
#  ano                        :string(255)
#  marca                      :string(255)
#  modelo                     :string(255)
#  color                      :string(255)
#  capacidad                  :integer
#  puesto                     :integer          default(0), not null
#  serial_motor               :string(255)
#  tipo_vehiculo              :string(255)
#  propietario                :string(255)
#  identificacion_propietario :string(255)
#  uso                        :string(255)      not null
#  tipo_unidad                :string(255)
#  status                     :boolean
#  flota_id                   :integer
#  created_at                 :datetime
#  updated_at                 :datetime
#

class Vehiculo < ActiveRecord::Base
  belongs_to :flota
  has_one :seguro_vehiculo, dependent: :destroy
  has_one :contrato_arrendamiento, dependent: :destroy
  accepts_nested_attributes_for :seguro_vehiculo
  accepts_nested_attributes_for :contrato_arrendamiento
  validates :placa, uniqueness: { case_sensitive: false,
                                 message:'El Vehículo ya pertenece a una flota' }
  validates :placa, length: { in: 6..7, message: 'La placa debe tener mínimo 6 y máximo 7 caracteres.' }

  enum tipo_unidad: {  minibus: "Minibus",
                       autobus: "Autobus",
                       automovil: "Automovil",
                       rustico: "Rustico",
                       moto: "Moto",
                       microbus: "Microbus"
                            }

  def contrato_arrendamiento_update_or_create(attributes)
    self.contrato_arrendamiento || self.build_contrato_arrendamiento
    self.contrato_arrendamiento.update(attributes)
  end

  def self.build_vehiculo_intt(vehiculo_intt, contrato_arrendamiento_params, tramite_vehiculo_params, flota)
    propietario = TramitePropietario.find_by(NRO_TRAMITE: vehiculo_intt.NRO_TRAMITE) || TramitePropietario.new
    uso = Uso.find_by(ID_USO: vehiculo_intt.ID_USO) || Uso.new
    tipo_vehiculo = TipoVehiculo.find_by(ID_TIPO_VEHICULO: vehiculo_intt.ID_TIPO_VEHICULO) || TipoVehiculo.new
    marca = Marca.find_by(ID_MARCA: vehiculo_intt.ID_MARCA) || Marca.new
    vehiculo = Vehiculo.new(placa: vehiculo_intt.PLACA,
                             s_carroceria: vehiculo_intt.SERIAL_CARROCERIA,
                             ano: vehiculo_intt.ANO,
                             marca: marca.NOMBRE_MARCA,
                             modelo: vehiculo_intt.MODELO,
                             color: Color.color(vehiculo_intt.ID_COLOR1),
                             serial_motor: vehiculo_intt.SERIAL_MOTOR,
                             capacidad: vehiculo_intt.CAPACIDAD,
                             propietario: [propietario.PRIMER_NOMBRE,
                                           propietario.SEGUNDO_NOMBRE, propietario.PRIMER_APELLIDO, propietario.SEGUNDO_APELLIDO, propietario.NOMBRE_EMPRESA].reject(&:nil?).join(' '),
                             identificacion_propietario: [propietario.ID_TIPO_IDENTIFICACION,
                                                          propietario.NRO_IDENTIFICACION .to_s, propietario.DIGITO_RIF.to_s].reject(&:nil?).join(' '),
                             puesto: vehiculo_intt.NRO_PUESTOS,
                             uso: (uso.NOMBRE_USO || ''),
                             tipo_vehiculo: (tipo_vehiculo.NOMBRE_TIPO || ''  ),
                             flota: flota)
    vehiculo.build_contrato_arrendamiento(contrato_arrendamiento_params) unless contrato_arrendamiento_params.nil?

    vehiculo.build_seguro_vehiculo(tramite_vehiculo_params[:seguro_vehiculo]) if flota.seguro_vehiculo.nil?

    return vehiculo
  end

  # enum uso: { porpuesto: "Por Puesto",
  #             colectivo: "Colectivo",
  #             taxi: "Taxi",
  #             monotaxi: "Moto Taxi",
  #             escolar:"Escolar",
  #             periferico: "Periferico"
  # }
  #
end
