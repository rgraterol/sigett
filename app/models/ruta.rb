# -*- encoding : utf-8 -*-
# == Schema Information
#
# Table name: ruta
#
#  id                 :integer          not null, primary key
#  num_ruta           :string(255)
#  fecha_autorizacion :date
#  tipo               :boolean          default(FALSE)
#  ida_vuelta         :boolean          default(FALSE)
#  flota_id           :integer
#  created_at         :datetime
#  updated_at         :datetime
#

class Ruta < ActiveRecord::Base
  include RegexHelper
  has_many :toques
  belongs_to :flota
  has_many :horarios
  # belongs_to :municipio
  accepts_nested_attributes_for :toques
  before_create :set_time

  MODO_RUTA = %w(Ida IdaVuelta)

  validates :num_ruta, presence: { message: 'Número de ruta es requerido'}
  validates :num_ruta, length: { in: 1..4, message: 'Número de la ruta debe tener entre 1 a 64 caracteres'}
  # validates :fecha_autorizacion, presence: { message: 'Fecha de autorización es requerido'}
  # validates :fecha_autorizacion, format: {with: FECHA_REGEX, message: 'Formato de fecha no válido, debe ingresar un fecha válida, Ej. dd/mm/aaaa'}
  # validates :ida_vuelta, presence: { message: 'Ida y vuelta es requerido'}



  def set_time
    self.fecha_autorizacion = Time.now
  end

end

