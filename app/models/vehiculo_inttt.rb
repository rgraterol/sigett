# -*- encoding : utf-8 -*-
# == Schema Information
#
# Table name: vehiculo_inttts
#
#  id                 :integer          not null, primary key
#  placa              :string(255)      not null
#  anio               :date             not null
#  s_carroceria       :string(255)      not null
#  color              :string(255)      not null
#  marca              :string(255)      not null
#  modelo             :string(255)      not null
#  puestos            :integer          default(0), not null
#  nombre_propietario :string(255)      not null
#  uso                :string(255)      default(""), not null
#  tipo_unidad        :string(255)      default(""), not null
#  created_at         :datetime
#  updated_at         :datetime
#

class VehiculoInttt < ActiveRecord::Base
 # establish_connection :inttt_development

  #asociaciones


  #validates

end

