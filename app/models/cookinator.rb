class Cookinator < ActiveRecord::Base
  def self.create_cookie(nombre={}, valor=nil)
    cookies.permanent[nombre] = valor
  end

  def self.show_cookie(nombre = {})
    cookies[nombre]
  end

  def self.create_cookie_opt(val=nil)
    self.create_cookie(:opt,val)
  end

  def self.show_cookie_opt(nombre = {})
    self.show_cookie(nombre)
  end


end
