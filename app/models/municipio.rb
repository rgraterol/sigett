# -*- encoding : utf-8 -*-
# == Schema Information
#
# Table name: municipios
#
#  id         :integer          not null, primary key
#  nombre     :string(255)
#  estado_id  :integer
#  created_at :datetime
#  updated_at :datetime
#

class Municipio < ActiveRecord::Base
  belongs_to :estado
  # has_many :rutas
  has_many :parroquia_vs

end

