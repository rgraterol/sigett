json.array!(@entregables) do |entregable|
  json.extract! entregable, :id, :costoEstimado, :comunicacion, :producto_id
  json.url entregable_url(entregable, format: :json)
end
