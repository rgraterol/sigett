json.array!(@paradas_adm_paradas) do |paradas_adm_parada|
  json.extract! paradas_adm_parada, :id
  json.url paradas_adm_parada_url(paradas_adm_parada, format: :json)
end
