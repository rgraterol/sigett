json.array!(@toques) do |toque|
  json.extract! toque, :id
  json.url toque_url(toque, format: :json)
end
