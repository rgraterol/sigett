json.array!(@admin_modalidad_servicios) do |admin_modalidad_servicio|
  json.extract! admin_modalidad_servicio, :id
  json.url admin_modalidad_servicio_url(admin_modalidad_servicio, format: :json)
end
