json.array!(@seguro_vehiculos) do |seguro_vehiculo|
  json.extract! seguro_vehiculo, :id
  json.url seguro_vehiculo_url(seguro_vehiculo, format: :json)
end
