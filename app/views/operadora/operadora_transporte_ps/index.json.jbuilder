json.array!(@operadora_transporte_ps) do |operadora_transporte_p|
  json.extract! operadora_transporte_p, :id
  json.url operadora_transporte_p_url(operadora_transporte_p, format: :json)
end
