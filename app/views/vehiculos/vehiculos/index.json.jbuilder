json.array!(@vehiculos) do |vehiculo|
  json.extract! vehiculo, :id
  json.url vehiculo_url(vehiculo, format: :json)
end
