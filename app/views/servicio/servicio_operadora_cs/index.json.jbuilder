json.array!(@servicio_operadora_cs) do |servicio_operadora_c|
  json.extract! servicio_operadora_c, :id
  json.url servicio_operadora_c_url(servicio_operadora_c, format: :json)
end
