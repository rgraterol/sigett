json.array!(@aseguradoras) do |aseguradora|
  json.extract! aseguradora, :id
  json.url aseguradora_url(aseguradora, format: :json)
end
