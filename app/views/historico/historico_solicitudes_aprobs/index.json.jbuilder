json.array!(@historico_solicitudes_aprobs) do |historico_solicitudes_aprob|
  json.extract! historico_solicitudes_aprob, :id
  json.url historico_solicitudes_aprob_url(historico_solicitudes_aprob, format: :json)
end
