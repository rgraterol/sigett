class EstadisticasSql
  def initialize
    #@start_time = start_time
    #@range = range
  end

  def igual_parada(estado, tipoP, parada)
    <<SQL
      SELECT
        servicio_operadora_cs.modalidad_servicio as servicio,
        count(*) as total
      FROM
        estados,
        municipios,
        parroquia_vs,
        operadora_transporte_ps,
        servicio_operadora_cs,
        flota,
        ruta,
        toques,
        paradas
      WHERE
        estados.id = municipios.estado_id AND
        municipios.id = parroquia_vs.municipio_id AND
        parroquia_vs.id = operadora_transporte_ps.parroquia_v_id AND
        operadora_transporte_ps.id = servicio_operadora_cs.operadora_transporte_p_id AND
        servicio_operadora_cs.id = flota.servicio_operadora_c_id AND
        flota.id = ruta.flota_id AND
        ruta.id = toques.ruta_id AND
        paradas.id = toques.parada_id AND
        paradas.tipo_parada = #{tipoP} AND
        paradas.id = #{parada} AND
        estados.id = #{estado}
      GROUP BY
        servicio_operadora_cs.modalidad_servicio
SQL
  end
end