class ListadoSql
  def initialize

  end

  def op_prox_flotas_venc(est,f_ini,f_fin)
    <<SQL
    SELECT
      estados.nombre,
      operadora_transporte_ps.razon_social,
      operadora_transporte_ps.rif,
      operadora_transporte_ps.movil,
      operadora_transporte_ps.email
    FROM
      public.estados,
      public.municipios,
      public.parroquia_vs,
      public.operadora_transporte_ps,
      public.servicio_operadora_cs,
      public.flota
    WHERE
      estados.id = municipios.estado_id AND
      municipios.id = parroquia_vs.municipio_id AND
      parroquia_vs.id = operadora_transporte_ps.parroquia_v_id AND
      operadora_transporte_ps.id = servicio_operadora_cs.operadora_transporte_p_id AND
      servicio_operadora_cs.id = flota.servicio_operadora_c_id AND
      estados.id = #{est} AND
      flota.fecha_vence_autorizacion BETWEEN #{f_ini} AND #{f_fin};
SQL
  end

  def op_igual_parada(est, tipoP, parada)
    <<SQL
      SELECT
        estados.nombre,
        operadora_transporte_ps.razon_social,
        operadora_transporte_ps.rif,
        operadora_transporte_ps.movil,
        operadora_transporte_ps.email
      FROM
        estados,
        municipios,
        parroquia_vs,
        operadora_transporte_ps,
        servicio_operadora_cs,
        flota,
        ruta,
        toques,
        paradas
      WHERE
        estados.id = municipios.estado_id AND
        municipios.id = parroquia_vs.municipio_id AND
        parroquia_vs.id = operadora_transporte_ps.parroquia_v_id AND
        operadora_transporte_ps.id = servicio_operadora_cs.operadora_transporte_p_id AND
        servicio_operadora_cs.id = flota.servicio_operadora_c_id AND
        flota.id = ruta.flota_id AND
        ruta.id = toques.ruta_id AND
        paradas.id = toques.parada_id AND
        paradas.tipo_parada = #{tipoP} AND
        paradas.id = #{parada} AND
        estados.id = #{est};
SQL
  end

  def op_mes_cps(f_ini,f_fin,est)
    <<SQL
      SELECT
        estados.nombre,
        operadora_transporte_ps.razon_social,
        operadora_transporte_ps.rif,
        operadora_transporte_ps.movil,
        operadora_transporte_ps.email
      FROM
        estados,
        municipios,
        parroquia_vs,
        operadora_transporte_ps,
        servicio_operadora_cs
      WHERE
        estados.id = municipios.estado_id AND
        municipios.id = parroquia_vs.municipio_id AND
        parroquia_vs.id = operadora_transporte_ps.parroquia_v_id AND
        operadora_transporte_ps.id = servicio_operadora_cs.operadora_transporte_p_id AND
        servicio_operadora_cs.fecha_vencimiento BETWEEN #{f_ini} AND #{f_fin} AND
        estados.id = #{est};
SQL
  end

  def op_mes_vence(f_ini,f_fin,est)
    <<SQL
      SELECT
        estados.nombre,
        operadora_transporte_ps.razon_social,
        operadora_transporte_ps.rif,
        operadora_transporte_ps.movil,
        operadora_transporte_ps.email
      FROM
        operadora_transporte_ps,
        servicio_operadora_cs,
        flota,
        estados,
        municipios,
        parroquia_vs
      WHERE
        operadora_transporte_ps.id = servicio_operadora_cs.operadora_transporte_p_id AND
        servicio_operadora_cs.id = flota.servicio_operadora_c_id AND
        estados.id = municipios.estado_id AND
        municipios.id = parroquia_vs.municipio_id AND
        parroquia_vs.id = operadora_transporte_ps.parroquia_v_id AND
        flota.fecha_vence_autorizacion BETWEEN #{f_ini} AND #{f_fin} AND
        estados.id = #{est};
SQL
  end

  def self.todas_operadoras_estado
    # VistaBd::VistaOperadorasConEstado.all
    <<SQL
        SELECT
          estados.nombre as nombre_estado,
          estados.id as id_estado,
          operadora_transporte_ps.id as id_operadora,
          operadora_transporte_ps.razon_social,
          operadora_transporte_ps.rif, operadora_transporte_ps.email,
          operadora_transporte_ps.movil
        FROM
          public.estados,
          public.municipios,
          public.parroquia_vs,
          public.operadora_transporte_ps
        WHERE
          estados.id = municipios.estado_id AND
          municipios.id = parroquia_vs.municipio_id AND
          parroquia_vs.id = operadora_transporte_ps.parroquia_v_id

        ORDER BY
          estados.nombre ASC;
SQL
  end

  def self.operadoras_estado_modalidad(modalidad="", estado="null")
    modalidad = ServicioOperadoraC.modalidad_servicios[modalidad]
    <<SQL
      SELECT
        estados.nombre as nombre_estado,
        estados.id as id_estado,
        operadora_transporte_ps.id as id_operadora,
        operadora_transporte_ps.razon_social,
        operadora_transporte_ps.rif, operadora_transporte_ps.email,
        operadora_transporte_ps.movil
      FROM
        public.estados,
        public.municipios,
        public.parroquia_vs,
        public.operadora_transporte_ps,
        public.servicio_operadora_cs
      WHERE
        estados.id = municipios.estado_id AND
        municipios.id = parroquia_vs.municipio_id AND
        parroquia_vs.id = operadora_transporte_ps.parroquia_v_id AND
        operadora_transporte_ps.id = servicio_operadora_cs.operadora_transporte_p_id AND
        servicio_operadora_cs.modalidad_servicio = '#{modalidad}' AND
        estados.id = #{estado};
SQL
  end

  def self.operadoras_por_rutas(paradas=[])
    # string = "[1,19,28]"
    string = paradas.to_s
    size = paradas.nil? ? 0 : paradas.size
    # size = 3
    <<SQL
      SELECT
        estados.nombre as nombre_estado,
        estados.id as id_estado,
        operadora_transporte_ps.id as id_operadora,
        operadora_transporte_ps.razon_social,
        operadora_transporte_ps.rif, operadora_transporte_ps.email,
        operadora_transporte_ps.movil,
        ruta.id as ruta_id
       FROM
        public.estados,
        public.municipios,
        public.parroquia_vs,
        public.operadora_transporte_ps,
        public.servicio_operadora_cs,
        public.flota,
        public.ruta
      WHERE
        estados.id = municipios.estado_id AND
        municipios.id = parroquia_vs.municipio_id AND
        parroquia_vs.id = operadora_transporte_ps.parroquia_v_id AND
        operadora_transporte_ps.id = servicio_operadora_cs.operadora_transporte_p_id AND
        servicio_operadora_cs.id = flota.servicio_operadora_c_id AND
        ruta.id IN

        (SELECT
          n1.ruta_id
        FROM (
          SELECT
            ruta.id as ruta_id
          FROM
            public.estados,
            public.municipios,
            public.parroquia_vs,
            public.operadora_transporte_ps,
            public.servicio_operadora_cs,
            public.ruta,
            public.flota,
            public.toques,
            public.paradas
          WHERE
            estados.id = municipios.estado_id AND
            municipios.id = parroquia_vs.municipio_id AND
            parroquia_vs.id = operadora_transporte_ps.parroquia_v_id AND
            operadora_transporte_ps.id = servicio_operadora_cs.operadora_transporte_p_id AND
            servicio_operadora_cs.id = flota.servicio_operadora_c_id AND
            ruta.id = toques.ruta_id AND
            flota.id = ruta.flota_id AND
            paradas.id = toques.parada_id AND
            paradas.id IN #{string.gsub(/[\x5b\x5d\x22]/, '[' => '(', ']' => ')', '\"' => '')}
          ORDER BY
          ruta.id
            )n1

        GROUP BY
        ruta_id
        HAVING
        COUNT (ruta_id) = #{size}
        ) AND
        flota.id = ruta.flota_id
SQL

  end

  def self.columna_to_integer(array=[], nombre="")
    if nombre.blank? || array.blank?
      return nil
    else
      return array.index(nombre)
    end
  end

  def self.operadoras_tipo_unidad(estado, tipo, modalidad)
    operadoras = {}
    servicio = ServicioOperadoraC.modalidad_servicios[modalidad.to_sym]
    vehiculo = Vehiculo.tipo_unidads[tipo.to_sym]
    operadoras = VistaModalidadTipoUnidad.where(nombre: estado, modalidad_servicio: servicio, tipo_unidad: vehiculo )
    return operadoras.empty? ? {} : operadoras
  end

  def self.operadoras_por_rutas_toques(paradas = [])
    a = ActiveRecord::Base.connection
    query = a.exec_query(Listado.operadoras_por_rutas(paradas))
    self.verificar_trayectoria_toques(query, paradas)
  end

  def self.verificar_trayectoria_toques(query, paradas)
    operadoras = []
    band = true
    query.each_with_index do |q, index_q|
      op = OperadoraTransporteP.find_by(id: q["id_operadora"])
      op.servicio_operadora_cs.each do |s|
        s.flota.rutas.each do |r|
          puts r.toques.inspect
          toque = r.toques.first
          paradas.each_with_index  do |p, index|
            if toque.parada.id != p.to_i
              band = false
            end
            toque = toque.next
          end
        end
      end
      if band
        operadoras << q
      else
        band = true
      end
    end
    operadoras
  end
end